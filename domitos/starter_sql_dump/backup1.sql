--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: blog_blogdetailpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_blogdetailpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200),
    category character varying(100) NOT NULL,
    date date,
    read_time character varying(100),
    requestcontent character varying(100),
    requestbutton character varying(100),
    content text,
    blog_detail_image_id integer,
    blog_list_image_id integer
);


ALTER TABLE public.blog_blogdetailpage OWNER TO postgres;

--
-- Name: blog_bloglistingpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_bloglistingpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200),
    category character varying(100) NOT NULL,
    requestcontent character varying(100),
    requestbutton character varying(100)
);


ALTER TABLE public.blog_bloglistingpage OWNER TO postgres;

--
-- Name: blog_cookiepolicy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_cookiepolicy (
    page_ptr_id integer NOT NULL,
    cookie_title character varying(200),
    cookie_content text
);


ALTER TABLE public.blog_cookiepolicy OWNER TO postgres;

--
-- Name: blog_gdpr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_gdpr (
    page_ptr_id integer NOT NULL,
    gdpr_title character varying(200),
    gdpr_content text
);


ALTER TABLE public.blog_gdpr OWNER TO postgres;

--
-- Name: blog_privacy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_privacy (
    page_ptr_id integer NOT NULL,
    privacy_title character varying(200),
    privacy_content text
);


ALTER TABLE public.blog_privacy OWNER TO postgres;

--
-- Name: blog_resources; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_resources (
    page_ptr_id integer NOT NULL
);


ALTER TABLE public.blog_resources OWNER TO postgres;

--
-- Name: blog_termsofservices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_termsofservices (
    page_ptr_id integer NOT NULL,
    terms_title character varying(200),
    terms_content text
);


ALTER TABLE public.blog_termsofservices OWNER TO postgres;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: home_homepage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.home_homepage (
    page_ptr_id integer NOT NULL,
    assests_content1 text,
    assests_content2 text,
    assests_title1 character varying(300),
    box_text1 character varying(300),
    box_text2 character varying(300),
    box_text3 character varying(300),
    box_title1 character varying(300),
    box_title2 character varying(300),
    box_title3 character varying(300),
    button1 character varying(100),
    button2 character varying(100),
    button3 character varying(100),
    button4 character varying(100),
    covid_content1 text,
    covid_content2 text,
    covid_title1 character varying(300),
    crm_content1 text,
    crm_content2 text,
    crm_title1 character varying(300),
    customer_content text,
    facility_content1 text,
    facility_content2 text,
    facility_title1 character varying(300),
    header_title character varying(200),
    manage_covid_content text,
    manage_covid_subtitle character varying(300),
    meta_content character varying(200),
    news_content text,
    page_title_content text,
    plan1 character varying(300),
    plan1_content text,
    plan2 character varying(300),
    plan2_point1 character varying(300),
    plan2_point2 character varying(300),
    plan2_point3 character varying(300),
    plan2_point4 character varying(300),
    plan2_point5 character varying(300),
    plan3 character varying(300),
    plan3_content text,
    plan_button character varying(300),
    project_unit character varying(100),
    quote_content text,
    requestbutton character varying(100),
    requestcontent character varying(100),
    snippet_content1 text,
    snippet_content2 text,
    snippet_content3 text,
    snippet_content4 text,
    snippet_content5 text,
    snippet_title character varying(300),
    snippet_title1 character varying(300),
    snippet_title2 character varying(300),
    snippet_title3 character varying(300),
    snippet_title4 character varying(300),
    snippet_title5 character varying(300),
    tenants_managed character varying(100),
    testimonials text,
    total_project character varying(100),
    view_button character varying(100)
);


ALTER TABLE public.home_homepage OWNER TO postgres;

--
-- Name: taggit_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taggit_tag (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(100) NOT NULL
);


ALTER TABLE public.taggit_tag OWNER TO postgres;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taggit_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_tag_id_seq OWNER TO postgres;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taggit_tag_id_seq OWNED BY public.taggit_tag.id;


--
-- Name: taggit_taggeditem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taggit_taggeditem (
    id integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.taggit_taggeditem OWNER TO postgres;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taggit_taggeditem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_taggeditem_id_seq OWNER TO postgres;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taggit_taggeditem_id_seq OWNED BY public.taggit_taggeditem.id;


--
-- Name: wagtail_localize_localesynchronization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_localesynchronization (
    id integer NOT NULL,
    locale_id integer NOT NULL,
    sync_from_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_localesynchronization OWNER TO postgres;

--
-- Name: wagtail_localize_localesynchronization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_localesynchronization_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_localesynchronization_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_localesynchronization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_localesynchronization_id_seq OWNED BY public.wagtail_localize_localesynchronization.id;


--
-- Name: wagtail_localize_overridablesegment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_overridablesegment (
    id integer NOT NULL,
    "order" integer NOT NULL,
    data_json text NOT NULL,
    context_id integer NOT NULL,
    source_id integer NOT NULL,
    CONSTRAINT wagtail_localize_overridablesegment_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.wagtail_localize_overridablesegment OWNER TO postgres;

--
-- Name: wagtail_localize_overridablesegment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_overridablesegment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_overridablesegment_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_overridablesegment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_overridablesegment_id_seq OWNED BY public.wagtail_localize_overridablesegment.id;


--
-- Name: wagtail_localize_relatedobjectsegment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_relatedobjectsegment (
    id integer NOT NULL,
    "order" integer NOT NULL,
    context_id integer NOT NULL,
    object_id uuid NOT NULL,
    source_id integer NOT NULL,
    CONSTRAINT wagtail_localize_relatedobjectsegment_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.wagtail_localize_relatedobjectsegment OWNER TO postgres;

--
-- Name: wagtail_localize_relatedobjectsegment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_relatedobjectsegment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_relatedobjectsegment_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_relatedobjectsegment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_relatedobjectsegment_id_seq OWNED BY public.wagtail_localize_relatedobjectsegment.id;


--
-- Name: wagtail_localize_segmentoverride; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_segmentoverride (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    data_json text NOT NULL,
    has_error boolean NOT NULL,
    field_error text NOT NULL,
    context_id integer,
    last_translated_by_id integer,
    locale_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_segmentoverride OWNER TO postgres;

--
-- Name: wagtail_localize_segmentoverride_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_segmentoverride_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_segmentoverride_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_segmentoverride_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_segmentoverride_id_seq OWNED BY public.wagtail_localize_segmentoverride.id;


--
-- Name: wagtail_localize_string; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_string (
    id integer NOT NULL,
    data_hash uuid NOT NULL,
    data text NOT NULL,
    locale_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_string OWNER TO postgres;

--
-- Name: wagtail_localize_string_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_string_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_string_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_string_id_seq OWNED BY public.wagtail_localize_string.id;


--
-- Name: wagtail_localize_stringsegment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_stringsegment (
    id integer NOT NULL,
    "order" integer NOT NULL,
    attrs text NOT NULL,
    context_id integer NOT NULL,
    source_id integer NOT NULL,
    string_id integer NOT NULL,
    CONSTRAINT wagtail_localize_stringsegment_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.wagtail_localize_stringsegment OWNER TO postgres;

--
-- Name: wagtail_localize_stringsegment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_stringsegment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_stringsegment_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_stringsegment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_stringsegment_id_seq OWNED BY public.wagtail_localize_stringsegment.id;


--
-- Name: wagtail_localize_stringtranslation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_stringtranslation (
    id integer NOT NULL,
    data text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    context_id integer,
    locale_id integer NOT NULL,
    translation_of_id integer NOT NULL,
    tool_name character varying(255) NOT NULL,
    translation_type character varying(20) NOT NULL,
    last_translated_by_id integer,
    field_error text NOT NULL,
    has_error boolean NOT NULL
);


ALTER TABLE public.wagtail_localize_stringtranslation OWNER TO postgres;

--
-- Name: wagtail_localize_stringtranslation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_stringtranslation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_stringtranslation_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_stringtranslation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_stringtranslation_id_seq OWNED BY public.wagtail_localize_stringtranslation.id;


--
-- Name: wagtail_localize_template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_template (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    template text NOT NULL,
    template_format character varying(100) NOT NULL,
    string_count integer NOT NULL,
    CONSTRAINT wagtail_localize_template_string_count_check CHECK ((string_count >= 0))
);


ALTER TABLE public.wagtail_localize_template OWNER TO postgres;

--
-- Name: wagtail_localize_template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_template_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_template_id_seq OWNED BY public.wagtail_localize_template.id;


--
-- Name: wagtail_localize_templatesegment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_templatesegment (
    id integer NOT NULL,
    "order" integer NOT NULL,
    context_id integer NOT NULL,
    source_id integer NOT NULL,
    template_id integer NOT NULL,
    CONSTRAINT wagtail_localize_templatesegment_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.wagtail_localize_templatesegment OWNER TO postgres;

--
-- Name: wagtail_localize_templatesegment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_templatesegment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_templatesegment_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_templatesegment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_templatesegment_id_seq OWNED BY public.wagtail_localize_templatesegment.id;


--
-- Name: wagtail_localize_translatableobject; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_translatableobject (
    translation_key uuid NOT NULL,
    content_type_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_translatableobject OWNER TO postgres;

--
-- Name: wagtail_localize_translation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_translation (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    source_last_updated_at timestamp with time zone NOT NULL,
    translations_last_updated_at timestamp with time zone,
    destination_last_updated_at timestamp with time zone,
    enabled boolean NOT NULL,
    source_id integer NOT NULL,
    target_locale_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_translation OWNER TO postgres;

--
-- Name: wagtail_localize_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_translation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_translation_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_translation_id_seq OWNED BY public.wagtail_localize_translation.id;


--
-- Name: wagtail_localize_translationcontext; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_translationcontext (
    id integer NOT NULL,
    path_id uuid NOT NULL,
    path text NOT NULL,
    object_id uuid NOT NULL
);


ALTER TABLE public.wagtail_localize_translationcontext OWNER TO postgres;

--
-- Name: wagtail_localize_translationcontext_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_translationcontext_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_translationcontext_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_translationcontext_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_translationcontext_id_seq OWNED BY public.wagtail_localize_translationcontext.id;


--
-- Name: wagtail_localize_translationlog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_translationlog (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    locale_id integer NOT NULL,
    page_revision_id integer,
    source_id integer NOT NULL
);


ALTER TABLE public.wagtail_localize_translationlog OWNER TO postgres;

--
-- Name: wagtail_localize_translationlog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_translationlog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_translationlog_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_translationlog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_translationlog_id_seq OWNED BY public.wagtail_localize_translationlog.id;


--
-- Name: wagtail_localize_translationsource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtail_localize_translationsource (
    id integer NOT NULL,
    object_repr text NOT NULL,
    content_json text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    locale_id integer NOT NULL,
    object_id uuid NOT NULL,
    specific_content_type_id integer NOT NULL,
    last_updated_at timestamp with time zone NOT NULL,
    schema_version character varying(255) NOT NULL
);


ALTER TABLE public.wagtail_localize_translationsource OWNER TO postgres;

--
-- Name: wagtail_localize_translationsource_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtail_localize_translationsource_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtail_localize_translationsource_id_seq OWNER TO postgres;

--
-- Name: wagtail_localize_translationsource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtail_localize_translationsource_id_seq OWNED BY public.wagtail_localize_translationsource.id;


--
-- Name: wagtailadmin_admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailadmin_admin (
    id integer NOT NULL
);


ALTER TABLE public.wagtailadmin_admin OWNER TO postgres;

--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailadmin_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailadmin_admin_id_seq OWNER TO postgres;

--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailadmin_admin_id_seq OWNED BY public.wagtailadmin_admin.id;


--
-- Name: wagtailcore_collection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collection (
    id integer NOT NULL,
    path character varying(255) NOT NULL COLLATE pg_catalog."C",
    depth integer NOT NULL,
    numchild integer NOT NULL,
    name character varying(255) NOT NULL,
    CONSTRAINT wagtailcore_collection_depth_check CHECK ((depth >= 0)),
    CONSTRAINT wagtailcore_collection_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE public.wagtailcore_collection OWNER TO postgres;

--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collection_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collection_id_seq OWNED BY public.wagtailcore_collection.id;


--
-- Name: wagtailcore_collectionviewrestriction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collectionviewrestriction (
    id integer NOT NULL,
    restriction_type character varying(20) NOT NULL,
    password character varying(255) NOT NULL,
    collection_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_collectionviewrestriction OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collectionviewrestriction_groups (
    id integer NOT NULL,
    collectionviewrestriction_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_collectionviewrestriction_groups OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collectionviewrestriction_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collectionviewrestriction_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collectionviewrestriction_groups_id_seq OWNED BY public.wagtailcore_collectionviewrestriction_groups.id;


--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collectionviewrestriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collectionviewrestriction_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collectionviewrestriction_id_seq OWNED BY public.wagtailcore_collectionviewrestriction.id;


--
-- Name: wagtailcore_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_comment (
    id integer NOT NULL,
    text text NOT NULL,
    contentpath text NOT NULL,
    "position" text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    resolved_at timestamp with time zone,
    page_id integer NOT NULL,
    resolved_by_id integer,
    revision_created_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_comment OWNER TO postgres;

--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_comment_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_comment_id_seq OWNED BY public.wagtailcore_comment.id;


--
-- Name: wagtailcore_commentreply; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_commentreply (
    id integer NOT NULL,
    text text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    comment_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_commentreply OWNER TO postgres;

--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_commentreply_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_commentreply_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_commentreply_id_seq OWNED BY public.wagtailcore_commentreply.id;


--
-- Name: wagtailcore_groupapprovaltask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupapprovaltask (
    task_ptr_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupapprovaltask OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupapprovaltask_groups (
    id integer NOT NULL,
    groupapprovaltask_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupapprovaltask_groups OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_groupapprovaltask_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_groupapprovaltask_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_groupapprovaltask_groups_id_seq OWNED BY public.wagtailcore_groupapprovaltask_groups.id;


--
-- Name: wagtailcore_groupcollectionpermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupcollectionpermission (
    id integer NOT NULL,
    collection_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupcollectionpermission OWNER TO postgres;

--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_groupcollectionpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_groupcollectionpermission_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_groupcollectionpermission_id_seq OWNED BY public.wagtailcore_groupcollectionpermission.id;


--
-- Name: wagtailcore_grouppagepermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_grouppagepermission (
    id integer NOT NULL,
    permission_type character varying(20) NOT NULL,
    group_id integer NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_grouppagepermission OWNER TO postgres;

--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_grouppagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_grouppagepermission_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_grouppagepermission_id_seq OWNED BY public.wagtailcore_grouppagepermission.id;


--
-- Name: wagtailcore_locale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_locale (
    id integer NOT NULL,
    language_code character varying(100) NOT NULL
);


ALTER TABLE public.wagtailcore_locale OWNER TO postgres;

--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_locale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_locale_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_locale_id_seq OWNED BY public.wagtailcore_locale.id;


--
-- Name: wagtailcore_page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_page (
    id integer NOT NULL,
    path character varying(255) NOT NULL COLLATE pg_catalog."C",
    depth integer NOT NULL,
    numchild integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    live boolean NOT NULL,
    has_unpublished_changes boolean NOT NULL,
    url_path text NOT NULL,
    seo_title character varying(255) NOT NULL,
    show_in_menus boolean NOT NULL,
    search_description text NOT NULL,
    go_live_at timestamp with time zone,
    expire_at timestamp with time zone,
    expired boolean NOT NULL,
    content_type_id integer NOT NULL,
    owner_id integer,
    locked boolean NOT NULL,
    latest_revision_created_at timestamp with time zone,
    first_published_at timestamp with time zone,
    live_revision_id integer,
    last_published_at timestamp with time zone,
    draft_title character varying(255) NOT NULL,
    locked_at timestamp with time zone,
    locked_by_id integer,
    translation_key uuid NOT NULL,
    locale_id integer NOT NULL,
    alias_of_id integer,
    CONSTRAINT wagtailcore_page_depth_check CHECK ((depth >= 0)),
    CONSTRAINT wagtailcore_page_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE public.wagtailcore_page OWNER TO postgres;

--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_page_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_page_id_seq OWNED BY public.wagtailcore_page.id;


--
-- Name: wagtailcore_pagelogentry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagelogentry (
    id integer NOT NULL,
    label text NOT NULL,
    action character varying(255) NOT NULL,
    data_json text NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    content_changed boolean NOT NULL,
    deleted boolean NOT NULL,
    content_type_id integer,
    page_id integer NOT NULL,
    revision_id integer,
    user_id integer
);


ALTER TABLE public.wagtailcore_pagelogentry OWNER TO postgres;

--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagelogentry_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagelogentry_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagelogentry_id_seq OWNED BY public.wagtailcore_pagelogentry.id;


--
-- Name: wagtailcore_pagerevision; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagerevision (
    id integer NOT NULL,
    submitted_for_moderation boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    content_json text NOT NULL,
    approved_go_live_at timestamp with time zone,
    page_id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.wagtailcore_pagerevision OWNER TO postgres;

--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagerevision_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagerevision_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagerevision_id_seq OWNED BY public.wagtailcore_pagerevision.id;


--
-- Name: wagtailcore_pagesubscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagesubscription (
    id integer NOT NULL,
    comment_notifications boolean NOT NULL,
    page_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_pagesubscription OWNER TO postgres;

--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagesubscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagesubscription_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagesubscription_id_seq OWNED BY public.wagtailcore_pagesubscription.id;


--
-- Name: wagtailcore_pageviewrestriction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pageviewrestriction (
    id integer NOT NULL,
    password character varying(255) NOT NULL,
    page_id integer NOT NULL,
    restriction_type character varying(20) NOT NULL
);


ALTER TABLE public.wagtailcore_pageviewrestriction OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pageviewrestriction_groups (
    id integer NOT NULL,
    pageviewrestriction_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_pageviewrestriction_groups OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pageviewrestriction_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pageviewrestriction_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pageviewrestriction_groups_id_seq OWNED BY public.wagtailcore_pageviewrestriction_groups.id;


--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pageviewrestriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pageviewrestriction_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pageviewrestriction_id_seq OWNED BY public.wagtailcore_pageviewrestriction.id;


--
-- Name: wagtailcore_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_site (
    id integer NOT NULL,
    hostname character varying(255) NOT NULL,
    port integer NOT NULL,
    is_default_site boolean NOT NULL,
    root_page_id integer NOT NULL,
    site_name character varying(255) NOT NULL
);


ALTER TABLE public.wagtailcore_site OWNER TO postgres;

--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_site_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_site_id_seq OWNED BY public.wagtailcore_site.id;


--
-- Name: wagtailcore_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_task (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    active boolean NOT NULL,
    content_type_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_task OWNER TO postgres;

--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_task_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_task_id_seq OWNED BY public.wagtailcore_task.id;


--
-- Name: wagtailcore_taskstate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_taskstate (
    id integer NOT NULL,
    status character varying(50) NOT NULL,
    started_at timestamp with time zone NOT NULL,
    finished_at timestamp with time zone,
    content_type_id integer NOT NULL,
    page_revision_id integer NOT NULL,
    task_id integer NOT NULL,
    workflow_state_id integer NOT NULL,
    finished_by_id integer,
    comment text NOT NULL
);


ALTER TABLE public.wagtailcore_taskstate OWNER TO postgres;

--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_taskstate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_taskstate_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_taskstate_id_seq OWNED BY public.wagtailcore_taskstate.id;


--
-- Name: wagtailcore_workflow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflow (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE public.wagtailcore_workflow OWNER TO postgres;

--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflow_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflow_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflow_id_seq OWNED BY public.wagtailcore_workflow.id;


--
-- Name: wagtailcore_workflowpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowpage (
    page_id integer NOT NULL,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowpage OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowstate (
    id integer NOT NULL,
    status character varying(50) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    current_task_state_id integer,
    page_id integer NOT NULL,
    requested_by_id integer,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowstate OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflowstate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflowstate_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflowstate_id_seq OWNED BY public.wagtailcore_workflowstate.id;


--
-- Name: wagtailcore_workflowtask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowtask (
    id integer NOT NULL,
    sort_order integer,
    task_id integer NOT NULL,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowtask OWNER TO postgres;

--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflowtask_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflowtask_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflowtask_id_seq OWNED BY public.wagtailcore_workflowtask.id;


--
-- Name: wagtaildocs_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtaildocs_document (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    uploaded_by_user_id integer,
    collection_id integer NOT NULL,
    file_size integer,
    file_hash character varying(40) NOT NULL,
    CONSTRAINT wagtaildocs_document_file_size_check CHECK ((file_size >= 0))
);


ALTER TABLE public.wagtaildocs_document OWNER TO postgres;

--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtaildocs_document_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtaildocs_document_id_seq OWNER TO postgres;

--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtaildocs_document_id_seq OWNED BY public.wagtaildocs_document.id;


--
-- Name: wagtaildocs_uploadeddocument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtaildocs_uploadeddocument (
    id integer NOT NULL,
    file character varying(200) NOT NULL,
    uploaded_by_user_id integer
);


ALTER TABLE public.wagtaildocs_uploadeddocument OWNER TO postgres;

--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtaildocs_uploadeddocument_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtaildocs_uploadeddocument_id_seq OWNER TO postgres;

--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtaildocs_uploadeddocument_id_seq OWNED BY public.wagtaildocs_uploadeddocument.id;


--
-- Name: wagtailembeds_embed; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailembeds_embed (
    id integer NOT NULL,
    url text NOT NULL,
    max_width smallint,
    type character varying(10) NOT NULL,
    html text NOT NULL,
    title text NOT NULL,
    author_name text NOT NULL,
    provider_name text NOT NULL,
    thumbnail_url text NOT NULL,
    width integer,
    height integer,
    last_updated timestamp with time zone NOT NULL,
    hash character varying(32) NOT NULL,
    cache_until timestamp with time zone
);


ALTER TABLE public.wagtailembeds_embed OWNER TO postgres;

--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailembeds_embed_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailembeds_embed_id_seq OWNER TO postgres;

--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailembeds_embed_id_seq OWNED BY public.wagtailembeds_embed.id;


--
-- Name: wagtailforms_formsubmission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailforms_formsubmission (
    id integer NOT NULL,
    form_data text NOT NULL,
    submit_time timestamp with time zone NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.wagtailforms_formsubmission OWNER TO postgres;

--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailforms_formsubmission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailforms_formsubmission_id_seq OWNER TO postgres;

--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailforms_formsubmission_id_seq OWNED BY public.wagtailforms_formsubmission.id;


--
-- Name: wagtailimages_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_image (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    focal_point_x integer,
    focal_point_y integer,
    focal_point_width integer,
    focal_point_height integer,
    uploaded_by_user_id integer,
    file_size integer,
    collection_id integer NOT NULL,
    file_hash character varying(40) NOT NULL,
    CONSTRAINT wagtailimages_image_file_size_check CHECK ((file_size >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_height_check CHECK ((focal_point_height >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_width_check CHECK ((focal_point_width >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_x_check CHECK ((focal_point_x >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_y_check CHECK ((focal_point_y >= 0))
);


ALTER TABLE public.wagtailimages_image OWNER TO postgres;

--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_image_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_image_id_seq OWNED BY public.wagtailimages_image.id;


--
-- Name: wagtailimages_rendition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_rendition (
    id integer NOT NULL,
    file character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    focal_point_key character varying(16) NOT NULL,
    filter_spec character varying(255) NOT NULL,
    image_id integer NOT NULL
);


ALTER TABLE public.wagtailimages_rendition OWNER TO postgres;

--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_rendition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_rendition_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_rendition_id_seq OWNED BY public.wagtailimages_rendition.id;


--
-- Name: wagtailimages_uploadedimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_uploadedimage (
    id integer NOT NULL,
    file character varying(200) NOT NULL,
    uploaded_by_user_id integer
);


ALTER TABLE public.wagtailimages_uploadedimage OWNER TO postgres;

--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_uploadedimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_uploadedimage_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_uploadedimage_id_seq OWNED BY public.wagtailimages_uploadedimage.id;


--
-- Name: wagtailredirects_redirect; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailredirects_redirect (
    id integer NOT NULL,
    old_path character varying(255) NOT NULL,
    is_permanent boolean NOT NULL,
    redirect_link character varying(255) NOT NULL,
    redirect_page_id integer,
    site_id integer
);


ALTER TABLE public.wagtailredirects_redirect OWNER TO postgres;

--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailredirects_redirect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailredirects_redirect_id_seq OWNER TO postgres;

--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailredirects_redirect_id_seq OWNED BY public.wagtailredirects_redirect.id;


--
-- Name: wagtailsearch_editorspick; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_editorspick (
    id integer NOT NULL,
    sort_order integer,
    description text NOT NULL,
    page_id integer NOT NULL,
    query_id integer NOT NULL
);


ALTER TABLE public.wagtailsearch_editorspick OWNER TO postgres;

--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_editorspick_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_editorspick_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_editorspick_id_seq OWNED BY public.wagtailsearch_editorspick.id;


--
-- Name: wagtailsearch_query; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_query (
    id integer NOT NULL,
    query_string character varying(255) NOT NULL
);


ALTER TABLE public.wagtailsearch_query OWNER TO postgres;

--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_query_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_query_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_query_id_seq OWNED BY public.wagtailsearch_query.id;


--
-- Name: wagtailsearch_querydailyhits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_querydailyhits (
    id integer NOT NULL,
    date date NOT NULL,
    hits integer NOT NULL,
    query_id integer NOT NULL
);


ALTER TABLE public.wagtailsearch_querydailyhits OWNER TO postgres;

--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_querydailyhits_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_querydailyhits_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_querydailyhits_id_seq OWNED BY public.wagtailsearch_querydailyhits.id;


--
-- Name: wagtailusers_userprofile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailusers_userprofile (
    id integer NOT NULL,
    submitted_notifications boolean NOT NULL,
    approved_notifications boolean NOT NULL,
    rejected_notifications boolean NOT NULL,
    user_id integer NOT NULL,
    preferred_language character varying(10) NOT NULL,
    current_time_zone character varying(40) NOT NULL,
    avatar character varying(100) NOT NULL,
    updated_comments_notifications boolean NOT NULL
);


ALTER TABLE public.wagtailusers_userprofile OWNER TO postgres;

--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailusers_userprofile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailusers_userprofile_id_seq OWNER TO postgres;

--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailusers_userprofile_id_seq OWNED BY public.wagtailusers_userprofile.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: taggit_tag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag ALTER COLUMN id SET DEFAULT nextval('public.taggit_tag_id_seq'::regclass);


--
-- Name: taggit_taggeditem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem ALTER COLUMN id SET DEFAULT nextval('public.taggit_taggeditem_id_seq'::regclass);


--
-- Name: wagtail_localize_localesynchronization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_localesynchronization ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_localesynchronization_id_seq'::regclass);


--
-- Name: wagtail_localize_overridablesegment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_overridablesegment ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_overridablesegment_id_seq'::regclass);


--
-- Name: wagtail_localize_relatedobjectsegment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_relatedobjectsegment ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_relatedobjectsegment_id_seq'::regclass);


--
-- Name: wagtail_localize_segmentoverride id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_segmentoverride ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_segmentoverride_id_seq'::regclass);


--
-- Name: wagtail_localize_string id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_string ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_string_id_seq'::regclass);


--
-- Name: wagtail_localize_stringsegment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringsegment ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_stringsegment_id_seq'::regclass);


--
-- Name: wagtail_localize_stringtranslation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_stringtranslation_id_seq'::regclass);


--
-- Name: wagtail_localize_template id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_template ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_template_id_seq'::regclass);


--
-- Name: wagtail_localize_templatesegment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_templatesegment ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_templatesegment_id_seq'::regclass);


--
-- Name: wagtail_localize_translation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_translation_id_seq'::regclass);


--
-- Name: wagtail_localize_translationcontext id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationcontext ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_translationcontext_id_seq'::regclass);


--
-- Name: wagtail_localize_translationlog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationlog ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_translationlog_id_seq'::regclass);


--
-- Name: wagtail_localize_translationsource id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource ALTER COLUMN id SET DEFAULT nextval('public.wagtail_localize_translationsource_id_seq'::regclass);


--
-- Name: wagtailadmin_admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailadmin_admin ALTER COLUMN id SET DEFAULT nextval('public.wagtailadmin_admin_id_seq'::regclass);


--
-- Name: wagtailcore_collection id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collection_id_seq'::regclass);


--
-- Name: wagtailcore_collectionviewrestriction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collectionviewrestriction_id_seq'::regclass);


--
-- Name: wagtailcore_collectionviewrestriction_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collectionviewrestriction_groups_id_seq'::regclass);


--
-- Name: wagtailcore_comment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_comment_id_seq'::regclass);


--
-- Name: wagtailcore_commentreply id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_commentreply_id_seq'::regclass);


--
-- Name: wagtailcore_groupapprovaltask_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_groupapprovaltask_groups_id_seq'::regclass);


--
-- Name: wagtailcore_groupcollectionpermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_groupcollectionpermission_id_seq'::regclass);


--
-- Name: wagtailcore_grouppagepermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_grouppagepermission_id_seq'::regclass);


--
-- Name: wagtailcore_locale id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_locale_id_seq'::regclass);


--
-- Name: wagtailcore_page id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_page_id_seq'::regclass);


--
-- Name: wagtailcore_pagelogentry id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagelogentry_id_seq'::regclass);


--
-- Name: wagtailcore_pagerevision id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagerevision_id_seq'::regclass);


--
-- Name: wagtailcore_pagesubscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagesubscription_id_seq'::regclass);


--
-- Name: wagtailcore_pageviewrestriction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pageviewrestriction_id_seq'::regclass);


--
-- Name: wagtailcore_pageviewrestriction_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pageviewrestriction_groups_id_seq'::regclass);


--
-- Name: wagtailcore_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_site_id_seq'::regclass);


--
-- Name: wagtailcore_task id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_task_id_seq'::regclass);


--
-- Name: wagtailcore_taskstate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_taskstate_id_seq'::regclass);


--
-- Name: wagtailcore_workflow id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflow ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflow_id_seq'::regclass);


--
-- Name: wagtailcore_workflowstate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflowstate_id_seq'::regclass);


--
-- Name: wagtailcore_workflowtask id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflowtask_id_seq'::regclass);


--
-- Name: wagtaildocs_document id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document ALTER COLUMN id SET DEFAULT nextval('public.wagtaildocs_document_id_seq'::regclass);


--
-- Name: wagtaildocs_uploadeddocument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument ALTER COLUMN id SET DEFAULT nextval('public.wagtaildocs_uploadeddocument_id_seq'::regclass);


--
-- Name: wagtailembeds_embed id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed ALTER COLUMN id SET DEFAULT nextval('public.wagtailembeds_embed_id_seq'::regclass);


--
-- Name: wagtailforms_formsubmission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission ALTER COLUMN id SET DEFAULT nextval('public.wagtailforms_formsubmission_id_seq'::regclass);


--
-- Name: wagtailimages_image id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_image_id_seq'::regclass);


--
-- Name: wagtailimages_rendition id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_rendition_id_seq'::regclass);


--
-- Name: wagtailimages_uploadedimage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_uploadedimage_id_seq'::regclass);


--
-- Name: wagtailredirects_redirect id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect ALTER COLUMN id SET DEFAULT nextval('public.wagtailredirects_redirect_id_seq'::regclass);


--
-- Name: wagtailsearch_editorspick id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_editorspick_id_seq'::regclass);


--
-- Name: wagtailsearch_query id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_query_id_seq'::regclass);


--
-- Name: wagtailsearch_querydailyhits id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_querydailyhits_id_seq'::regclass);


--
-- Name: wagtailusers_userprofile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile ALTER COLUMN id SET DEFAULT nextval('public.wagtailusers_userprofile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
1	Moderators
2	Editors
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	1
2	2	1
3	1	2
4	1	3
5	1	4
6	2	2
7	2	3
8	2	4
9	1	5
10	2	5
11	1	8
12	1	6
13	1	7
14	2	8
15	2	6
16	2	7
17	1	9
18	2	9
19	1	164
20	2	164
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can access Wagtail admin	3	access_admin
2	Can add document	5	add_document
3	Can change document	5	change_document
4	Can delete document	5	delete_document
5	Can choose document	5	choose_document
6	Can add image	6	add_image
7	Can change image	6	change_image
8	Can delete image	6	delete_image
9	Can choose image	6	choose_image
10	Can add home page	2	add_homepage
11	Can change home page	2	change_homepage
12	Can delete home page	2	delete_homepage
13	Can view home page	2	view_homepage
14	Can add form submission	7	add_formsubmission
15	Can change form submission	7	change_formsubmission
16	Can delete form submission	7	delete_formsubmission
17	Can view form submission	7	view_formsubmission
18	Can add redirect	8	add_redirect
19	Can change redirect	8	change_redirect
20	Can delete redirect	8	delete_redirect
21	Can view redirect	8	view_redirect
22	Can add embed	9	add_embed
23	Can change embed	9	change_embed
24	Can delete embed	9	delete_embed
25	Can view embed	9	view_embed
26	Can add user profile	10	add_userprofile
27	Can change user profile	10	change_userprofile
28	Can delete user profile	10	delete_userprofile
29	Can view user profile	10	view_userprofile
30	Can view document	5	view_document
31	Can add uploaded document	11	add_uploadeddocument
32	Can change uploaded document	11	change_uploadeddocument
33	Can delete uploaded document	11	delete_uploadeddocument
34	Can view uploaded document	11	view_uploadeddocument
35	Can view image	6	view_image
36	Can add rendition	12	add_rendition
37	Can change rendition	12	change_rendition
38	Can delete rendition	12	delete_rendition
39	Can view rendition	12	view_rendition
40	Can add uploaded image	13	add_uploadedimage
41	Can change uploaded image	13	change_uploadedimage
42	Can delete uploaded image	13	delete_uploadedimage
43	Can view uploaded image	13	view_uploadedimage
44	Can add query	14	add_query
45	Can change query	14	change_query
46	Can delete query	14	delete_query
47	Can view query	14	view_query
48	Can add Query Daily Hits	15	add_querydailyhits
49	Can change Query Daily Hits	15	change_querydailyhits
50	Can delete Query Daily Hits	15	delete_querydailyhits
51	Can view Query Daily Hits	15	view_querydailyhits
52	Can add page	1	add_page
53	Can change page	1	change_page
54	Can delete page	1	delete_page
55	Can view page	1	view_page
56	Can add group page permission	16	add_grouppagepermission
57	Can change group page permission	16	change_grouppagepermission
58	Can delete group page permission	16	delete_grouppagepermission
59	Can view group page permission	16	view_grouppagepermission
60	Can add page revision	17	add_pagerevision
61	Can change page revision	17	change_pagerevision
62	Can delete page revision	17	delete_pagerevision
63	Can view page revision	17	view_pagerevision
64	Can add page view restriction	18	add_pageviewrestriction
65	Can change page view restriction	18	change_pageviewrestriction
66	Can delete page view restriction	18	delete_pageviewrestriction
67	Can view page view restriction	18	view_pageviewrestriction
68	Can add site	19	add_site
69	Can change site	19	change_site
70	Can delete site	19	delete_site
71	Can view site	19	view_site
72	Can add collection	20	add_collection
73	Can change collection	20	change_collection
74	Can delete collection	20	delete_collection
75	Can view collection	20	view_collection
76	Can add group collection permission	21	add_groupcollectionpermission
77	Can change group collection permission	21	change_groupcollectionpermission
78	Can delete group collection permission	21	delete_groupcollectionpermission
79	Can view group collection permission	21	view_groupcollectionpermission
80	Can add collection view restriction	22	add_collectionviewrestriction
81	Can change collection view restriction	22	change_collectionviewrestriction
82	Can delete collection view restriction	22	delete_collectionviewrestriction
83	Can view collection view restriction	22	view_collectionviewrestriction
84	Can add task	23	add_task
85	Can change task	23	change_task
86	Can delete task	23	delete_task
87	Can view task	23	view_task
88	Can add Task state	24	add_taskstate
89	Can change Task state	24	change_taskstate
90	Can delete Task state	24	delete_taskstate
91	Can view Task state	24	view_taskstate
92	Can add workflow	25	add_workflow
93	Can change workflow	25	change_workflow
94	Can delete workflow	25	delete_workflow
95	Can view workflow	25	view_workflow
96	Can add Group approval task	4	add_groupapprovaltask
97	Can change Group approval task	4	change_groupapprovaltask
98	Can delete Group approval task	4	delete_groupapprovaltask
99	Can view Group approval task	4	view_groupapprovaltask
100	Can add Workflow state	26	add_workflowstate
101	Can change Workflow state	26	change_workflowstate
102	Can delete Workflow state	26	delete_workflowstate
103	Can view Workflow state	26	view_workflowstate
104	Can add workflow page	27	add_workflowpage
105	Can change workflow page	27	change_workflowpage
106	Can delete workflow page	27	delete_workflowpage
107	Can view workflow page	27	view_workflowpage
108	Can add workflow task order	28	add_workflowtask
109	Can change workflow task order	28	change_workflowtask
110	Can delete workflow task order	28	delete_workflowtask
111	Can view workflow task order	28	view_workflowtask
112	Can add page log entry	29	add_pagelogentry
113	Can change page log entry	29	change_pagelogentry
114	Can delete page log entry	29	delete_pagelogentry
115	Can view page log entry	29	view_pagelogentry
116	Can add locale	30	add_locale
117	Can change locale	30	change_locale
118	Can delete locale	30	delete_locale
119	Can view locale	30	view_locale
120	Can add comment	31	add_comment
121	Can change comment	31	change_comment
122	Can delete comment	31	delete_comment
123	Can view comment	31	view_comment
124	Can add comment reply	32	add_commentreply
125	Can change comment reply	32	change_commentreply
126	Can delete comment reply	32	delete_commentreply
127	Can view comment reply	32	view_commentreply
128	Can add page subscription	33	add_pagesubscription
129	Can change page subscription	33	change_pagesubscription
130	Can delete page subscription	33	delete_pagesubscription
131	Can view page subscription	33	view_pagesubscription
132	Can add tag	34	add_tag
133	Can change tag	34	change_tag
134	Can delete tag	34	delete_tag
135	Can view tag	34	view_tag
136	Can add tagged item	35	add_taggeditem
137	Can change tagged item	35	change_taggeditem
138	Can delete tagged item	35	delete_taggeditem
139	Can view tagged item	35	view_taggeditem
140	Can add log entry	36	add_logentry
141	Can change log entry	36	change_logentry
142	Can delete log entry	36	delete_logentry
143	Can view log entry	36	view_logentry
144	Can add permission	37	add_permission
145	Can change permission	37	change_permission
146	Can delete permission	37	delete_permission
147	Can view permission	37	view_permission
148	Can add group	38	add_group
149	Can change group	38	change_group
150	Can delete group	38	delete_group
151	Can view group	38	view_group
152	Can add user	39	add_user
153	Can change user	39	change_user
154	Can delete user	39	delete_user
155	Can view user	39	view_user
156	Can add content type	40	add_contenttype
157	Can change content type	40	change_contenttype
158	Can delete content type	40	delete_contenttype
159	Can view content type	40	view_contenttype
160	Can add session	41	add_session
161	Can change session	41	change_session
162	Can delete session	41	delete_session
163	Can view session	41	view_session
164	Can submit translations	42	submit_translation
165	Can add string	43	add_string
166	Can change string	43	change_string
167	Can delete string	43	delete_string
168	Can view string	43	view_string
169	Can add template	44	add_template
170	Can change template	44	change_template
171	Can delete template	44	delete_template
172	Can view template	44	view_template
173	Can add translatable object	45	add_translatableobject
174	Can change translatable object	45	change_translatableobject
175	Can delete translatable object	45	delete_translatableobject
176	Can view translatable object	45	view_translatableobject
177	Can add translation source	46	add_translationsource
178	Can change translation source	46	change_translationsource
179	Can delete translation source	46	delete_translationsource
180	Can view translation source	46	view_translationsource
181	Can add translation log	47	add_translationlog
182	Can change translation log	47	change_translationlog
183	Can delete translation log	47	delete_translationlog
184	Can view translation log	47	view_translationlog
185	Can add translation context	48	add_translationcontext
186	Can change translation context	48	change_translationcontext
187	Can delete translation context	48	delete_translationcontext
188	Can view translation context	48	view_translationcontext
189	Can add template segment	49	add_templatesegment
190	Can change template segment	49	change_templatesegment
191	Can delete template segment	49	delete_templatesegment
192	Can view template segment	49	view_templatesegment
193	Can add string segment	50	add_stringsegment
194	Can change string segment	50	change_stringsegment
195	Can delete string segment	50	delete_stringsegment
196	Can view string segment	50	view_stringsegment
197	Can add related object segment	51	add_relatedobjectsegment
198	Can change related object segment	51	change_relatedobjectsegment
199	Can delete related object segment	51	delete_relatedobjectsegment
200	Can view related object segment	51	view_relatedobjectsegment
201	Can add string translation	52	add_stringtranslation
202	Can change string translation	52	change_stringtranslation
203	Can delete string translation	52	delete_stringtranslation
204	Can view string translation	52	view_stringtranslation
205	Can add translation	42	add_translation
206	Can change translation	42	change_translation
207	Can delete translation	42	delete_translation
208	Can view translation	42	view_translation
209	Can add overridable segment	53	add_overridablesegment
210	Can change overridable segment	53	change_overridablesegment
211	Can delete overridable segment	53	delete_overridablesegment
212	Can view overridable segment	53	view_overridablesegment
213	Can add segment override	54	add_segmentoverride
214	Can change segment override	54	change_segmentoverride
215	Can delete segment override	54	delete_segmentoverride
216	Can view segment override	54	view_segmentoverride
217	Can add locale synchronization	55	add_localesynchronization
218	Can change locale synchronization	55	change_localesynchronization
219	Can delete locale synchronization	55	delete_localesynchronization
220	Can view locale synchronization	55	view_localesynchronization
221	Can add blog listing page	56	add_bloglistingpage
222	Can change blog listing page	56	change_bloglistingpage
223	Can delete blog listing page	56	delete_bloglistingpage
224	Can view blog listing page	56	view_bloglistingpage
225	Can add cookie policy	57	add_cookiepolicy
226	Can change cookie policy	57	change_cookiepolicy
227	Can delete cookie policy	57	delete_cookiepolicy
228	Can view cookie policy	57	view_cookiepolicy
229	Can add gdpr	58	add_gdpr
230	Can change gdpr	58	change_gdpr
231	Can delete gdpr	58	delete_gdpr
232	Can view gdpr	58	view_gdpr
233	Can add privacy	59	add_privacy
234	Can change privacy	59	change_privacy
235	Can delete privacy	59	delete_privacy
236	Can view privacy	59	view_privacy
237	Can add resources	60	add_resources
238	Can change resources	60	change_resources
239	Can delete resources	60	delete_resources
240	Can view resources	60	view_resources
241	Can add terms of services	61	add_termsofservices
242	Can change terms of services	61	change_termsofservices
243	Can delete terms of services	61	delete_termsofservices
244	Can view terms of services	61	view_termsofservices
245	Can add blog detail page	62	add_blogdetailpage
246	Can change blog detail page	62	change_blogdetailpage
247	Can delete blog detail page	62	delete_blogdetailpage
248	Can view blog detail page	62	view_blogdetailpage
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$216000$LnnJtNM3OVUn$H12lWdtjlreojQxmOcbllJyk8zUzBvwnzk5fB+fbgI4=	2021-10-20 10:56:22.433993+05:30	t	ranjitha				t	t	2021-10-20 10:22:16.010962+05:30
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: blog_blogdetailpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_blogdetailpage (page_ptr_id, header_title, meta_content, category, date, read_time, requestcontent, requestbutton, content, blog_detail_image_id, blog_list_image_id) FROM stdin;
\.


--
-- Data for Name: blog_bloglistingpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_bloglistingpage (page_ptr_id, header_title, meta_content, category, requestcontent, requestbutton) FROM stdin;
\.


--
-- Data for Name: blog_cookiepolicy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_cookiepolicy (page_ptr_id, cookie_title, cookie_content) FROM stdin;
\.


--
-- Data for Name: blog_gdpr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_gdpr (page_ptr_id, gdpr_title, gdpr_content) FROM stdin;
\.


--
-- Data for Name: blog_privacy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_privacy (page_ptr_id, privacy_title, privacy_content) FROM stdin;
\.


--
-- Data for Name: blog_resources; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_resources (page_ptr_id) FROM stdin;
\.


--
-- Data for Name: blog_termsofservices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_termsofservices (page_ptr_id, terms_title, terms_content) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	wagtailcore	page
2	home	homepage
3	wagtailadmin	admin
4	wagtailcore	groupapprovaltask
5	wagtaildocs	document
6	wagtailimages	image
7	wagtailforms	formsubmission
8	wagtailredirects	redirect
9	wagtailembeds	embed
10	wagtailusers	userprofile
11	wagtaildocs	uploadeddocument
12	wagtailimages	rendition
13	wagtailimages	uploadedimage
14	wagtailsearch	query
15	wagtailsearch	querydailyhits
16	wagtailcore	grouppagepermission
17	wagtailcore	pagerevision
18	wagtailcore	pageviewrestriction
19	wagtailcore	site
20	wagtailcore	collection
21	wagtailcore	groupcollectionpermission
22	wagtailcore	collectionviewrestriction
23	wagtailcore	task
24	wagtailcore	taskstate
25	wagtailcore	workflow
26	wagtailcore	workflowstate
27	wagtailcore	workflowpage
28	wagtailcore	workflowtask
29	wagtailcore	pagelogentry
30	wagtailcore	locale
31	wagtailcore	comment
32	wagtailcore	commentreply
33	wagtailcore	pagesubscription
34	taggit	tag
35	taggit	taggeditem
36	admin	logentry
37	auth	permission
38	auth	group
39	auth	user
40	contenttypes	contenttype
41	sessions	session
42	wagtail_localize	translation
43	wagtail_localize	string
44	wagtail_localize	template
45	wagtail_localize	translatableobject
46	wagtail_localize	translationsource
47	wagtail_localize	translationlog
48	wagtail_localize	translationcontext
49	wagtail_localize	templatesegment
50	wagtail_localize	stringsegment
51	wagtail_localize	relatedobjectsegment
52	wagtail_localize	stringtranslation
53	wagtail_localize	overridablesegment
54	wagtail_localize	segmentoverride
55	wagtail_localize	localesynchronization
56	blog	bloglistingpage
57	blog	cookiepolicy
58	blog	gdpr
59	blog	privacy
60	blog	resources
61	blog	termsofservices
62	blog	blogdetailpage
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-10-20 10:20:13.676378+05:30
2	auth	0001_initial	2021-10-20 10:20:14.186122+05:30
3	admin	0001_initial	2021-10-20 10:20:14.282979+05:30
4	admin	0002_logentry_remove_auto_add	2021-10-20 10:20:14.300971+05:30
5	admin	0003_logentry_add_action_flag_choices	2021-10-20 10:20:14.322248+05:30
6	contenttypes	0002_remove_content_type_name	2021-10-20 10:20:14.354599+05:30
7	auth	0002_alter_permission_name_max_length	2021-10-20 10:20:14.372484+05:30
8	auth	0003_alter_user_email_max_length	2021-10-20 10:20:14.385528+05:30
9	auth	0004_alter_user_username_opts	2021-10-20 10:20:14.401812+05:30
10	auth	0005_alter_user_last_login_null	2021-10-20 10:20:14.419887+05:30
11	auth	0006_require_contenttypes_0002	2021-10-20 10:20:14.423622+05:30
12	auth	0007_alter_validators_add_error_messages	2021-10-20 10:20:14.441537+05:30
13	auth	0008_alter_user_username_max_length	2021-10-20 10:20:14.47568+05:30
14	auth	0009_alter_user_last_name_max_length	2021-10-20 10:20:14.490749+05:30
15	auth	0010_alter_group_name_max_length	2021-10-20 10:20:14.507488+05:30
16	auth	0011_update_proxy_permissions	2021-10-20 10:20:14.522237+05:30
17	auth	0012_alter_user_first_name_max_length	2021-10-20 10:20:14.53568+05:30
18	wagtailcore	0001_initial	2021-10-20 10:20:15.437685+05:30
19	wagtailcore	0002_initial_data	2021-10-20 10:20:15.442748+05:30
20	wagtailcore	0003_add_uniqueness_constraint_on_group_page_permission	2021-10-20 10:20:15.445101+05:30
21	wagtailcore	0004_page_locked	2021-10-20 10:20:15.446439+05:30
22	wagtailcore	0005_add_page_lock_permission_to_moderators	2021-10-20 10:20:15.448571+05:30
23	wagtailcore	0006_add_lock_page_permission	2021-10-20 10:20:15.451154+05:30
24	wagtailcore	0007_page_latest_revision_created_at	2021-10-20 10:20:15.453095+05:30
25	wagtailcore	0008_populate_latest_revision_created_at	2021-10-20 10:20:15.455099+05:30
26	wagtailcore	0009_remove_auto_now_add_from_pagerevision_created_at	2021-10-20 10:20:15.457085+05:30
27	wagtailcore	0010_change_page_owner_to_null_on_delete	2021-10-20 10:20:15.459573+05:30
28	wagtailcore	0011_page_first_published_at	2021-10-20 10:20:15.479293+05:30
29	wagtailcore	0012_extend_page_slug_field	2021-10-20 10:20:15.486298+05:30
30	wagtailcore	0013_update_golive_expire_help_text	2021-10-20 10:20:15.493092+05:30
31	wagtailcore	0014_add_verbose_name	2021-10-20 10:20:15.500461+05:30
32	wagtailcore	0015_add_more_verbose_names	2021-10-20 10:20:15.507548+05:30
33	wagtailcore	0016_change_page_url_path_to_text_field	2021-10-20 10:20:15.510051+05:30
34	wagtailcore	0017_change_edit_page_permission_description	2021-10-20 10:20:15.52775+05:30
35	wagtailcore	0018_pagerevision_submitted_for_moderation_index	2021-10-20 10:20:15.56581+05:30
36	wagtailcore	0019_verbose_names_cleanup	2021-10-20 10:20:15.640882+05:30
37	wagtailcore	0020_add_index_on_page_first_published_at	2021-10-20 10:20:15.674588+05:30
38	wagtailcore	0021_capitalizeverbose	2021-10-20 10:20:16.172624+05:30
39	wagtailcore	0022_add_site_name	2021-10-20 10:20:16.202372+05:30
40	wagtailcore	0023_alter_page_revision_on_delete_behaviour	2021-10-20 10:20:16.226154+05:30
41	wagtailcore	0024_collection	2021-10-20 10:20:16.287136+05:30
42	wagtailcore	0025_collection_initial_data	2021-10-20 10:20:16.315422+05:30
43	wagtailcore	0026_group_collection_permission	2021-10-20 10:20:16.431057+05:30
44	wagtailcore	0027_fix_collection_path_collation	2021-10-20 10:20:16.529118+05:30
45	wagtailcore	0024_alter_page_content_type_on_delete_behaviour	2021-10-20 10:20:16.573016+05:30
46	wagtailcore	0028_merge	2021-10-20 10:20:16.579841+05:30
47	wagtailcore	0029_unicode_slugfield_dj19	2021-10-20 10:20:16.617651+05:30
48	wagtailcore	0030_index_on_pagerevision_created_at	2021-10-20 10:20:16.665473+05:30
49	wagtailcore	0031_add_page_view_restriction_types	2021-10-20 10:20:16.798303+05:30
50	wagtailcore	0032_add_bulk_delete_page_permission	2021-10-20 10:20:16.835744+05:30
51	wagtailcore	0033_remove_golive_expiry_help_text	2021-10-20 10:20:16.878557+05:30
52	wagtailcore	0034_page_live_revision	2021-10-20 10:20:16.919764+05:30
53	wagtailcore	0035_page_last_published_at	2021-10-20 10:20:16.952266+05:30
54	wagtailcore	0036_populate_page_last_published_at	2021-10-20 10:20:16.983022+05:30
55	wagtailcore	0037_set_page_owner_editable	2021-10-20 10:20:17.02218+05:30
56	wagtailcore	0038_make_first_published_at_editable	2021-10-20 10:20:17.051154+05:30
57	wagtailcore	0039_collectionviewrestriction	2021-10-20 10:20:17.237945+05:30
58	wagtailcore	0040_page_draft_title	2021-10-20 10:20:17.29855+05:30
59	home	0001_initial	2021-10-20 10:20:17.355159+05:30
60	home	0002_create_homepage	2021-10-20 10:20:17.416316+05:30
61	sessions	0001_initial	2021-10-20 10:20:17.47647+05:30
62	taggit	0001_initial	2021-10-20 10:20:17.644785+05:30
63	taggit	0002_auto_20150616_2121	2021-10-20 10:20:17.787008+05:30
64	taggit	0003_taggeditem_add_unique_index	2021-10-20 10:20:17.809573+05:30
65	wagtailadmin	0001_create_admin_access_permissions	2021-10-20 10:20:17.857022+05:30
66	wagtailadmin	0002_admin	2021-10-20 10:20:17.863726+05:30
67	wagtailadmin	0003_admin_managed	2021-10-20 10:20:17.885789+05:30
68	wagtailcore	0041_group_collection_permissions_verbose_name_plural	2021-10-20 10:20:17.914938+05:30
69	wagtailcore	0042_index_on_pagerevision_approved_go_live_at	2021-10-20 10:20:17.963437+05:30
70	wagtailcore	0043_lock_fields	2021-10-20 10:20:18.04823+05:30
71	wagtailcore	0044_add_unlock_grouppagepermission	2021-10-20 10:20:18.075091+05:30
72	wagtailcore	0045_assign_unlock_grouppagepermission	2021-10-20 10:20:18.125307+05:30
73	wagtailcore	0046_site_name_remove_null	2021-10-20 10:20:18.156392+05:30
74	wagtailcore	0047_add_workflow_models	2021-10-20 10:20:18.913747+05:30
75	wagtailcore	0048_add_default_workflows	2021-10-20 10:20:19.045417+05:30
76	wagtailcore	0049_taskstate_finished_by	2021-10-20 10:20:19.117372+05:30
77	wagtailcore	0050_workflow_rejected_to_needs_changes	2021-10-20 10:20:19.221077+05:30
78	wagtailcore	0051_taskstate_comment	2021-10-20 10:20:19.271562+05:30
79	wagtailcore	0052_pagelogentry	2021-10-20 10:20:19.424695+05:30
80	wagtailcore	0053_locale_model	2021-10-20 10:20:19.507884+05:30
81	wagtailcore	0054_initial_locale	2021-10-20 10:20:19.603379+05:30
82	wagtailcore	0055_page_locale_fields	2021-10-20 10:20:19.804224+05:30
83	wagtailcore	0056_page_locale_fields_populate	2021-10-20 10:20:20.022274+05:30
84	wagtailcore	0057_page_locale_fields_notnull	2021-10-20 10:20:20.129714+05:30
85	wagtailcore	0058_page_alias_of	2021-10-20 10:20:20.250912+05:30
86	wagtailcore	0059_apply_collection_ordering	2021-10-20 10:20:20.306014+05:30
87	wagtailcore	0060_fix_workflow_unique_constraint	2021-10-20 10:20:20.382021+05:30
88	wagtailcore	0061_change_promote_tab_helpt_text_and_verbose_names	2021-10-20 10:20:20.442689+05:30
89	wagtailcore	0062_comment_models_and_pagesubscription	2021-10-20 10:20:20.90076+05:30
90	wagtaildocs	0001_initial	2021-10-20 10:20:21.041204+05:30
91	wagtaildocs	0002_initial_data	2021-10-20 10:20:21.122982+05:30
92	wagtaildocs	0003_add_verbose_names	2021-10-20 10:20:21.225567+05:30
93	wagtaildocs	0004_capitalizeverbose	2021-10-20 10:20:21.412515+05:30
94	wagtaildocs	0005_document_collection	2021-10-20 10:20:21.552478+05:30
95	wagtaildocs	0006_copy_document_permissions_to_collections	2021-10-20 10:20:21.616154+05:30
96	wagtaildocs	0005_alter_uploaded_by_user_on_delete_action	2021-10-20 10:20:21.664636+05:30
97	wagtaildocs	0007_merge	2021-10-20 10:20:21.66766+05:30
98	wagtaildocs	0008_document_file_size	2021-10-20 10:20:21.709339+05:30
99	wagtaildocs	0009_document_verbose_name_plural	2021-10-20 10:20:21.748032+05:30
100	wagtaildocs	0010_document_file_hash	2021-10-20 10:20:21.782457+05:30
101	wagtaildocs	0011_add_choose_permissions	2021-10-20 10:20:21.92593+05:30
102	wagtaildocs	0012_uploadeddocument	2021-10-20 10:20:22.0552+05:30
103	wagtailembeds	0001_initial	2021-10-20 10:20:22.13897+05:30
104	wagtailembeds	0002_add_verbose_names	2021-10-20 10:20:22.164059+05:30
105	wagtailembeds	0003_capitalizeverbose	2021-10-20 10:20:22.176516+05:30
106	wagtailembeds	0004_embed_verbose_name_plural	2021-10-20 10:20:22.184397+05:30
107	wagtailembeds	0005_specify_thumbnail_url_max_length	2021-10-20 10:20:22.195581+05:30
108	wagtailembeds	0006_add_embed_hash	2021-10-20 10:20:22.20724+05:30
109	wagtailembeds	0007_populate_hash	2021-10-20 10:20:22.271645+05:30
110	wagtailembeds	0008_allow_long_urls	2021-10-20 10:20:22.342513+05:30
111	wagtailembeds	0009_embed_cache_until	2021-10-20 10:20:22.406855+05:30
112	wagtailforms	0001_initial	2021-10-20 10:20:22.502638+05:30
113	wagtailforms	0002_add_verbose_names	2021-10-20 10:20:22.557491+05:30
114	wagtailforms	0003_capitalizeverbose	2021-10-20 10:20:22.614203+05:30
115	wagtailforms	0004_add_verbose_name_plural	2021-10-20 10:20:22.647881+05:30
116	wagtailimages	0001_initial	2021-10-20 10:20:23.050634+05:30
117	wagtailimages	0002_initial_data	2021-10-20 10:20:23.052215+05:30
118	wagtailimages	0003_fix_focal_point_fields	2021-10-20 10:20:23.055782+05:30
119	wagtailimages	0004_make_focal_point_key_not_nullable	2021-10-20 10:20:23.058715+05:30
120	wagtailimages	0005_make_filter_spec_unique	2021-10-20 10:20:23.062565+05:30
121	wagtailimages	0006_add_verbose_names	2021-10-20 10:20:23.068386+05:30
122	wagtailimages	0007_image_file_size	2021-10-20 10:20:23.072036+05:30
123	wagtailimages	0008_image_created_at_index	2021-10-20 10:20:23.075333+05:30
124	wagtailimages	0009_capitalizeverbose	2021-10-20 10:20:23.081867+05:30
125	wagtailimages	0010_change_on_delete_behaviour	2021-10-20 10:20:23.085807+05:30
126	wagtailimages	0011_image_collection	2021-10-20 10:20:23.089796+05:30
127	wagtailimages	0012_copy_image_permissions_to_collections	2021-10-20 10:20:23.09352+05:30
128	wagtailimages	0013_make_rendition_upload_callable	2021-10-20 10:20:23.096723+05:30
129	wagtailimages	0014_add_filter_spec_field	2021-10-20 10:20:23.098795+05:30
130	wagtailimages	0015_fill_filter_spec_field	2021-10-20 10:20:23.102554+05:30
131	wagtailimages	0016_deprecate_rendition_filter_relation	2021-10-20 10:20:23.105738+05:30
132	wagtailimages	0017_reduce_focal_point_key_max_length	2021-10-20 10:20:23.107517+05:30
133	wagtailimages	0018_remove_rendition_filter	2021-10-20 10:20:23.109649+05:30
134	wagtailimages	0019_delete_filter	2021-10-20 10:20:23.111795+05:30
135	wagtailimages	0020_add-verbose-name	2021-10-20 10:20:23.113958+05:30
136	wagtailimages	0021_image_file_hash	2021-10-20 10:20:23.116435+05:30
137	wagtailimages	0022_uploadedimage	2021-10-20 10:20:23.30817+05:30
138	wagtailimages	0023_add_choose_permissions	2021-10-20 10:20:23.476413+05:30
139	wagtailredirects	0001_initial	2021-10-20 10:20:23.61257+05:30
140	wagtailredirects	0002_add_verbose_names	2021-10-20 10:20:23.742463+05:30
141	wagtailredirects	0003_make_site_field_editable	2021-10-20 10:20:23.800047+05:30
142	wagtailredirects	0004_set_unique_on_path_and_site	2021-10-20 10:20:23.884759+05:30
143	wagtailredirects	0005_capitalizeverbose	2021-10-20 10:20:24.140417+05:30
144	wagtailredirects	0006_redirect_increase_max_length	2021-10-20 10:20:24.195785+05:30
145	wagtailsearch	0001_initial	2021-10-20 10:20:24.494064+05:30
146	wagtailsearch	0002_add_verbose_names	2021-10-20 10:20:24.691996+05:30
147	wagtailsearch	0003_remove_editors_pick	2021-10-20 10:20:24.813551+05:30
148	wagtailsearch	0004_querydailyhits_verbose_name_plural	2021-10-20 10:20:24.821577+05:30
149	wagtailusers	0001_initial	2021-10-20 10:20:24.979211+05:30
150	wagtailusers	0002_add_verbose_name_on_userprofile	2021-10-20 10:20:25.128763+05:30
151	wagtailusers	0003_add_verbose_names	2021-10-20 10:20:25.186409+05:30
152	wagtailusers	0004_capitalizeverbose	2021-10-20 10:20:25.367588+05:30
153	wagtailusers	0005_make_related_name_wagtail_specific	2021-10-20 10:20:25.45361+05:30
154	wagtailusers	0006_userprofile_prefered_language	2021-10-20 10:20:25.486323+05:30
155	wagtailusers	0007_userprofile_current_time_zone	2021-10-20 10:20:25.530831+05:30
156	wagtailusers	0008_userprofile_avatar	2021-10-20 10:20:25.6145+05:30
157	wagtailusers	0009_userprofile_verbose_name_plural	2021-10-20 10:20:25.664179+05:30
158	wagtailusers	0010_userprofile_updated_comments_notifications	2021-10-20 10:20:25.720356+05:30
159	wagtailimages	0001_squashed_0021	2021-10-20 10:20:25.727816+05:30
160	wagtailcore	0001_squashed_0016_change_page_url_path_to_text_field	2021-10-20 10:20:25.73014+05:30
161	wagtail_localize	0001_initial	2021-10-20 10:55:47.013691+05:30
162	wagtail_localize	0002_translation	2021-10-20 10:55:47.831581+05:30
163	wagtail_localize	0003_delete_translation_sources	2021-10-20 10:55:47.963765+05:30
164	wagtail_localize	0004_one_source_per_objectlocale	2021-10-20 10:55:48.092236+05:30
165	wagtail_localize	0005_remove_translationsource_object	2021-10-20 10:55:48.207639+05:30
166	wagtail_localize	0006_create_submit_translation_permission	2021-10-20 10:55:48.289697+05:30
167	wagtail_localize	0007_stringtranslation_type_and_tool_name	2021-10-20 10:55:48.33003+05:30
168	wagtail_localize	0008_stringtranslation_last_translated_by	2021-10-20 10:55:48.390415+05:30
169	wagtail_localize	0009_stringtranslation_errors	2021-10-20 10:55:48.479006+05:30
170	wagtail_localize	0010_overridablesegment	2021-10-20 10:55:48.566175+05:30
171	wagtail_localize	0011_segmentoverride	2021-10-20 10:55:48.763924+05:30
172	wagtail_localize	0012_localesynchronization	2021-10-20 10:55:49.031103+05:30
173	wagtail_localize	0013_translationsource_schema_version	2021-10-20 10:55:49.124456+05:30
174	blog	0001_initial	2021-10-20 11:05:54.262846+05:30
175	home	0003_auto_20211020_1105	2021-10-20 11:05:56.351504+05:30
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
av4cgr4smlzr4izzpohrs5jnxre86wwl	.eJxVjEEOwiAQRe_C2hAHGGhduu8ZyACDVA0kpV0Z765NutDtf-_9l_C0rcVvnRc_J3ERIE6_W6D44LqDdKd6azK2ui5zkLsiD9rl1BI_r4f7d1Col28dADKcyRICZQsjjsgctLEJkTGQyQoGiqwMu2z14KIDUmBBp4icQbw_5LE38w:1md47K:DPTWccy7g2AnRsW6_Jc_I2wl__aif-TQeLPRxJWKEaY	2021-11-03 10:56:22.436445+05:30
\.


--
-- Data for Name: home_homepage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.home_homepage (page_ptr_id, assests_content1, assests_content2, assests_title1, box_text1, box_text2, box_text3, box_title1, box_title2, box_title3, button1, button2, button3, button4, covid_content1, covid_content2, covid_title1, crm_content1, crm_content2, crm_title1, customer_content, facility_content1, facility_content2, facility_title1, header_title, manage_covid_content, manage_covid_subtitle, meta_content, news_content, page_title_content, plan1, plan1_content, plan2, plan2_point1, plan2_point2, plan2_point3, plan2_point4, plan2_point5, plan3, plan3_content, plan_button, project_unit, quote_content, requestbutton, requestcontent, snippet_content1, snippet_content2, snippet_content3, snippet_content4, snippet_content5, snippet_title, snippet_title1, snippet_title2, snippet_title3, snippet_title4, snippet_title5, tenants_managed, testimonials, total_project, view_button) FROM stdin;
3	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"kfzag\\">Asset Management &amp; Maintenance</h2>", "id": "8470a818-9219-489a-b823-4530f5aa6ee7"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"1mqod\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>", "id": "72f87fd1-2677-42f6-86da-e9aa59b6c94b"}]	Asset Management &amp; Maintenance	Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe	Facility & Property Management, Availability, Client Onboarding & Reallocation.	Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues	Tenant & Client Management	Facility Management	Assets & Maintenance	See Features	Discover More	Discover More	Discover More	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"s3h96\\">COVID Isolation &amp; Control</h2>", "id": "00ed78fc-4a89-46cc-9752-2a817d3390c5"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"fuftu\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>", "id": "62a55662-0ef3-45c1-bcc9-7afac1b36fde"}]	Try 15 days free trial	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"l2zlg\\">CRM specific to Facility Management</h2>", "id": "34abb62a-afdd-470a-85e9-1656f878be54"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"hwczy\\">Tailor Made CRM for Facility Management.</p>", "id": "06253dd1-cdea-4a5a-9364-7231d9086ed7"}]	Strong Sales Management	[{"type": "full_richtext", "value": "<p data-block-key=\\"y9ujp\\"></p><p data-block-key=\\"6rp38\\">TESTIMONIALS</p><h2 data-block-key=\\"4lbnj\\">Our Customers Reviews</h2>", "id": "9a1643ca-9c3e-478c-b22a-bfb7005544a9"}]	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"0ua3x\\">Discover Facility Manager Core</h2>", "id": "d01ee00d-3ca2-4bed-948a-9e7daa08e504"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"52958\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>", "id": "55869e56-326c-4d33-bf60-547ef4852647"}]	Dont worry,we have covered End to End	#1 Facility Management Software | Maintenance | Covid Management	[{"type": "full_richtext", "value": "<p data-block-key=\\"of16i\\"></p><h2 data-block-key=\\"np5p\\">Manage COVID Facilities</h2><p data-block-key=\\"eediu\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\"7i7la\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\"flmnh\\"></p><ul><li data-block-key=\\"4g6pe\\">Manage Tenants and their details</li></ul><p data-block-key=\\"9vsqq\\"></p><ul><li data-block-key=\\"3k4cf\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\"1bo4i\\"></p><ul><li data-block-key=\\"ekc7c\\">Immediate Onboarding on AWS Cloud</li></ul>", "id": "3cbdb5c7-fb5b-4f38-aea5-242f742b6be4"}]	For Governments & NGOs	Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.	[{"type": "full_richtext", "value": "<p data-block-key=\\"m6mif\\"></p><p data-block-key=\\"c4ns0\\">LATEST INFO</p><h2 data-block-key=\\"at6tj\\">Our Latest News</h2>", "id": "84399c1e-8505-4bbe-be8c-fe41ccd293e7"}]	[]	ECONOMY	[{"type": "full_richtext", "value": "<p data-block-key=\\"rybf4\\"></p><p data-block-key=\\"d0pa2\\">Facility Management on Shared EC2 Instance</p>", "id": "73225f3d-3f21-438b-8be8-751e85089de9"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"ejoa\\">Shared EC2 Cluster Deployment</p>", "id": "20cc318b-ae52-4570-b0b1-19c66e90e358"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"3uc8l\\">48 hrs response time</p>", "id": "0ba138eb-0f8a-478d-9b2e-b2cc43329d6c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"7i19r\\">Shared account management</p>", "id": "1dd696b2-fc13-480e-8db3-08421bd2f8a4"}]	STANDARD	Facility Management on Dedicated EC2 Instance	Dedicated EC2 Cluster Deployment	24 hrs response time	Dedicated Account Manager	\N	PREMIUM	[{"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"7v5na\\">Facility Management on Higher Grade EC2 Instance</p>", "id": "1ca23272-4759-467d-ad56-f889dd9e8481"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"39n9f\\">Dedicated EC2 Cluster Deployment</p>", "id": "47299893-5a35-483d-b320-10f61f490999"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"s2fj\\">3 hrs response time</p>", "id": "486621b0-aabd-4ca5-88ae-24469851ff6a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"ckaro\\">Dedicated Account Manager</p>", "id": "a59b703c-b7c0-4770-b019-f946529f293b"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"9v11e\\">All the modules and future updates included</p>", "id": "e410a78f-fe33-444a-a891-8605c51dce01"}]	Book Live Demo	Units	[{"type": "full_richtext", "value": "<p data-block-key=\\"9xoig\\"></p><h2 data-block-key=\\"4drha\\">Contact Us for a Quote</h2><p data-block-key=\\"9mptk\\">Crafted with Future of Facility Management in mind.</p>", "id": "307f13cc-eaba-48c7-8470-35a6188d48ca"}]	REQUEST A DEMO	Request a demo to see how Domitos can supercharge your Facility Team.	[{"type": "full_richtext", "value": "<p data-block-key=\\"mj8vl\\"></p><h2 data-block-key=\\"7156n\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\"43hpa\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>", "id": "bdf6ae50-56bd-4847-a438-018644493779"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"3e6tw\\"></p><h2 data-block-key=\\"2mrg3\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\"8b7f5\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>", "id": "bc9c9b17-1c40-4b74-8efe-034176f41587"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"aktp4\\"></p><h2 data-block-key=\\"e3la1\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\"9bmnm\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>", "id": "8e558d7d-d11a-4ab9-a804-f74289653a35"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"axgmb\\"></p><h2 data-block-key=\\"e0s7\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\"2b8a3\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>", "id": "5f2c115d-3399-4ae2-8416-ee8b183787c3"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"o0zk8\\"></p><h2 data-block-key=\\"fqcgj\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\"16pv9\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>", "id": "1c57afa3-dd46-4a21-baa7-8cb2b3dce638"}]	Seamlessly engage tenants Deliver exceptional experiences	Facility	Assets	Tenants&Clients	Maintenance	Insights	Tenants Managed	[{"type": "cards", "value": {"title": "Testimonials", "cards": [{"title": "Alagu Shan", "text": "Crystal Clear Management", "subtitle": "You've saved our business! Thanks guys, keep up the good work! The best FMS Available!"}, {"title": "Vijesh Kamat", "text": "CEO, 3000 Apartment Chain, Bengaluru", "subtitle": "I strongly recommend Domitos for managing your real estate properties."}, {"title": "Raman Palaniappan", "text": "MES Group", "subtitle": "Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities"}, {"title": "John Joy", "text": "CEO, Backup International", "subtitle": "I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope"}, {"title": "Lim Jannat", "text": "UX/UI Designer", "subtitle": "You've saved our business! Thanks guys, keep up the good work! The best on the net!"}]}, "id": "b3509892-95b1-4d9e-8af0-69af423c6869"}]	Total Projects	VIEW ALL NEWS
4	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"kfzag\\">\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e41\\u0e25\\u0e30\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c</h2>", "id": "8470a818-9219-489a-b823-4530f5aa6ee7"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"1mqod\\">\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e40\\u0e2a\\u0e32\\u0e2b\\u0e25\\u0e31\\u0e01\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e04\\u0e38\\u0e13\\u0e20\\u0e32\\u0e1e\\u0e41\\u0e25\\u0e30\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e42\\u0e23\\u0e07\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13</p>", "id": "72f87fd1-2677-42f6-86da-e9aa59b6c94b"}]	Asset Management &amp; Maintenance	จัดการลูกค้าและผู้เช่าผ่านช่องทางเดียว รับการชำระเงินด้วยระบบอิเล็กทรอนิกส์โดย Stripe	การจัดการสิ่งอำนวยความสะดวกและทรัพย์สิน ความพร้อมใช้งาน การเริ่มต้นใช้งานของลูกค้า & การจัดสรรใหม่	จัดการทรัพย์สินของลูกค้าและทรัพย์สินขององค์กร รักษาเชิงรุก จัดการปัญหา	การจัดการผู้เช่าและลูกค้า	การจัดการสิ่งอำนวยความสะดวก	สินทรัพย์และการบำรุงรักษา	ดูคุณสมบัติ	ค้นพบเพิ่มเติม	ค้นพบเพิ่มเติม	ค้นพบเพิ่มเติม	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"s3h96\\">\\u0e01\\u0e32\\u0e23\\u0e41\\u0e22\\u0e01\\u0e41\\u0e25\\u0e30\\u0e04\\u0e27\\u0e1a\\u0e04\\u0e38\\u0e21\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14</h2>", "id": "00ed78fc-4a89-46cc-9752-2a817d3390c5"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"fuftu\\">\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e0a\\u0e48\\u0e27\\u0e22\\u0e40\\u0e2b\\u0e25\\u0e37\\u0e2d\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30 NGOs \\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e4d\\u0e32\\u0e32\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e41\\u0e22\\u0e01\\u0e40\\u0e0a\\u0e37\\u0e49\\u0e2d COVID \\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e07\\u0e48\\u0e32\\u0e22\\u0e14\\u0e32\\u0e22</p>", "id": "62a55662-0ef3-45c1-bcc9-7afac1b36fde"}]	Try 15 days free trial	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"l2zlg\\">\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e23\\u0e34\\u0e2b\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e2a\\u0e31\\u0e21\\u0e1e\\u0e31\\u0e19\\u0e18\\u0e4c \\u0e40\\u0e09\\u0e1e\\u0e32\\u0e30\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21</h2>", "id": "34abb62a-afdd-470a-85e9-1656f878be54"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"hwczy\\">\\u0e01\\u0e32\\u0e23\\u0e2d\\u0e2d\\u0e01\\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e23\\u0e34\\u0e2b\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e2a\\u0e31\\u0e21\\u0e1e\\u0e31\\u0e19\\u0e18\\u0e4c\\u0e40\\u0e09\\u0e1e\\u0e32\\u0e30\\u0e01\\u0e25\\u0e38\\u0e48\\u0e21  \\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01</p>", "id": "06253dd1-cdea-4a5a-9364-7231d9086ed7"}]	Strong Sales Management	[{"type": "full_richtext", "value": "<p data-block-key=\\"y9ujp\\"></p><p data-block-key=\\"6rp38\\">\\u0e02\\u0e49\\u0e2d\\u0e04\\u0e27\\u0e32\\u0e21\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e2d\\u0e07 </p><h2 data-block-key=\\"4lbnj\\">\\u0e23\\u0e35\\u0e27\\u0e34\\u0e27\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32</h2>", "id": "9a1643ca-9c3e-478c-b22a-bfb7005544a9"}]	[{"type": "full_richtext", "value": "<h2 data-block-key=\\"0ua3x\\">\\u0e04\\u0e49\\u0e19\\u0e2b\\u0e32\\u0e1c\\u0e39\\u0e49\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e4d\\u0e32\\u0e32\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 </h2>", "id": "d01ee00d-3ca2-4bed-948a-9e7daa08e504"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"52958\\">\\u0e01\\u0e32\\u0e23\\u0e41\\u0e01\\u0e49\\u0e1b\\u0e31 \\u0e0d\\u0e2b\\u0e32\\u0e2b\\u0e25\\u0e31\\u0e01 \\u0e40\\u0e2b\\u0e21\\u0e32\\u0e30\\u0e01\\u0e31\\u0e1a\\u0e04\\u0e27\\u0e32\\u0e21\\u0e15\\u0e49\\u0e2d\\u0e07\\u0e01\\u0e32\\u0e23\\u0e2b\\u0e25\\u0e31\\u0e01\\u0e17\\u0e31 \\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14\\u0e2a\\u0e4d\\u0e32\\u0e32\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e18\\u0e38\\u0e23\\u0e01\\u0e34\\u0e08\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e4d\\u0e32\\u0e32\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13 \\u0e15\\u0e31 \\u0e49\\u0e07\\u0e41\\u0e15\\u0e48\\u0e01\\u0e32\\u0e23\\u0e02\\u0e32\\u0e22\\u0e44\\u0e1b\\u0e08\\u0e19\\u0e16\\u0e36\\u0e07\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e4d\\u0e32\\u0e32\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32</p>", "id": "55869e56-326c-4d33-bf60-547ef4852647"}]	Dont worry,we have covered End to End	#1 ซอฟต์แวร์การจัดการสิ่งอำนวยความสะดวก | การบำรุงรักษา | การจัดการโควิด	[{"type": "full_richtext", "value": "<p data-block-key=\\"of16i\\"></p><h2 data-block-key=\\"np5p\\">\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14</h2><p data-block-key=\\"eediu\\">\\u0e01\\u0e32\\u0e23\\u0e41\\u0e01\\u0e49\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32 \\u0e44\\u0e14\\u0e49\\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e23\\u0e48\\u0e27\\u0e21\\u0e01\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e2a\\u0e34\\u0e07\\u0e04\\u0e42\\u0e1b\\u0e23\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e2b\\u0e49\\u0e1a\\u0e23\\u0e34\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19\\u0e17\\u0e31\\u0e48\\u0e27\\u0e42\\u0e25\\u0e01</p><ul><li data-block-key=\\"7i7la\\">\\u0e2a\\u0e23\\u0e49\\u0e32\\u0e07\\u0e02\\u0e36\\u0e49\\u0e19\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e28\\u0e39\\u0e19\\u0e22\\u0e4c\\u0e01\\u0e31\\u0e01\\u0e01\\u0e31\\u0e19\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14</li></ul><p data-block-key=\\"flmnh\\"></p><ul><li data-block-key=\\"4g6pe\\">\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e23\\u0e32\\u0e22\\u0e25\\u0e30\\u0e40\\u0e2d\\u0e35\\u0e22\\u0e14\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e27\\u0e01\\u0e40\\u0e02\\u0e32</li></ul><p data-block-key=\\"9vsqq\\"></p><ul><li data-block-key=\\"3k4cf\\">\\u0e23\\u0e32\\u0e22\\u0e07\\u0e32\\u0e19\\u0e17\\u0e35\\u0e48\\u0e04\\u0e23\\u0e2d\\u0e1a\\u0e04\\u0e25\\u0e38\\u0e21\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25</li></ul><p data-block-key=\\"1bo4i\\"></p><ul><li data-block-key=\\"ekc7c\\">\\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e17\\u0e31\\u0e19\\u0e17\\u0e35\\u0e1a\\u0e19 AWS Cloud</li></ul>", "id": "3cbdb5c7-fb5b-4f38-aea5-242f742b6be4"}]	สำหรับรัฐบาลและองค์กรพัฒนาเอกชน	Domitos ให้การบำรุงรักษา &amp; ซอฟต์แวร์การจัดการสิ่งอำนวยความสะดวกเพื่อตรวจสอบและกำหนดคำสั่งงาน รวมถึงโมดูลการจัดการ covid-19 แบบกำหนดเองสำหรับที่พักของพนักงาน	[{"type": "full_richtext", "value": "<p data-block-key=\\"m6mif\\"></p><p data-block-key=\\"c4ns0\\">\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e25\\u0e48\\u0e32\\u0e2a\\u0e38\\u0e14   </p><h2 data-block-key=\\"at6tj\\">\\u0e02\\u0e48\\u0e32\\u0e27\\u0e25\\u0e48\\u0e32\\u0e2a\\u0e38\\u0e14\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32</h2>", "id": "84399c1e-8505-4bbe-be8c-fe41ccd293e7"}]	[]	ECONOMY	[{"type": "full_richtext", "value": "<p data-block-key=\\"rybf4\\"></p><p data-block-key=\\"d0pa2\\">Facility Management on Shared EC2 Instance</p>", "id": "73225f3d-3f21-438b-8be8-751e85089de9"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"ejoa\\">Shared EC2 Cluster Deployment</p>", "id": "20cc318b-ae52-4570-b0b1-19c66e90e358"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"3uc8l\\">48 hrs response time</p>", "id": "0ba138eb-0f8a-478d-9b2e-b2cc43329d6c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"znzpz\\"></p><p data-block-key=\\"7i19r\\">Shared account management</p>", "id": "1dd696b2-fc13-480e-8db3-08421bd2f8a4"}]	STANDARD	Facility Management on Dedicated EC2 Instance	Dedicated EC2 Cluster Deployment	24 hrs response time	Dedicated Account Manager	\N	PREMIUM	[{"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"7v5na\\">Facility Management on Higher Grade EC2 Instance</p>", "id": "1ca23272-4759-467d-ad56-f889dd9e8481"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"39n9f\\">Dedicated EC2 Cluster Deployment</p>", "id": "47299893-5a35-483d-b320-10f61f490999"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"s2fj\\">3 hrs response time</p>", "id": "486621b0-aabd-4ca5-88ae-24469851ff6a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"ckaro\\">Dedicated Account Manager</p>", "id": "a59b703c-b7c0-4770-b019-f946529f293b"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5m0r3\\"></p><p data-block-key=\\"9v11e\\">All the modules and future updates included</p>", "id": "e410a78f-fe33-444a-a891-8605c51dce01"}]	Book Live Demo	หน่วย	[{"type": "full_richtext", "value": "<p data-block-key=\\"9xoig\\"></p><h2 data-block-key=\\"4drha\\">\\u0e15\\u0e34\\u0e14\\u0e15\\u0e48\\u0e2d\\u0e40\\u0e23\\u0e32\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e02\\u0e2d\\u0e43\\u0e1a\\u0e40\\u0e2a\\u0e19\\u0e2d\\u0e23\\u0e32\\u0e04\\u0e32</h2><p data-block-key=\\"9mptk\\">\\u0e2a\\u0e23\\u0e49\\u0e32\\u0e07\\u0e02\\u0e36\\u0e49\\u0e19\\u0e42\\u0e14\\u0e22\\u0e04\\u0e33\\u0e19\\u0e36\\u0e07\\u0e16\\u0e36\\u0e07\\u0e2d\\u0e19\\u0e32\\u0e04\\u0e15\\u0e02\\u0e2d\\u0e07\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01</p>", "id": "307f13cc-eaba-48c7-8470-35a6188d48ca"}]	ขอตัวอย่าง	ขอการสาธิตเพื่อดูว่า Domitos สามารถเพิ่มพลังให้กับทีม Facility ของคุณได้อย่างไร	[{"type": "full_richtext", "value": "<p data-block-key=\\"mj8vl\\"></p><h2 data-block-key=\\"7156n\\">\\u0e2b\\u0e2d\\u0e04\\u0e27\\u0e1a\\u0e04\\u0e38\\u0e21\\u0e17\\u0e35\\u0e48\\u0e04\\u0e23\\u0e2d\\u0e1a\\u0e04\\u0e25\\u0e38\\u0e21\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13</h2><p data-block-key=\\"43hpa\\">\\u0e14\\u0e49\\u0e27\\u0e22\\u0e20\\u0e32\\u0e1e\\u0e23\\u0e27\\u0e21\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 \\u0e04\\u0e38\\u0e13\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e17\\u0e23\\u0e32\\u0e1a\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e20\\u0e32\\u0e22\\u0e43\\u0e15\\u0e49\\u0e1a\\u0e31\\u0e0d\\u0e0a\\u0e35\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13 \\u0e14\\u0e39\\u0e2d\\u0e31\\u0e15\\u0e23\\u0e32\\u0e01\\u0e32\\u0e23\\u0e40\\u0e02\\u0e49\\u0e32\\u0e1e\\u0e31\\u0e01 \\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e1b\\u0e31\\u0e08\\u0e08\\u0e38\\u0e1a\\u0e31\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e0b\\u0e37\\u0e49\\u0e2d\\u0e15\\u0e32\\u0e21\\u0e02\\u0e27\\u0e32\\u0e07\\u0e2b\\u0e23\\u0e37\\u0e2d\\u0e15\\u0e32\\u0e21\\u0e2b\\u0e2d\\u0e04\\u0e2d\\u0e22 \\u0e41\\u0e25\\u0e30\\u0e04\\u0e32\\u0e14\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e41\\u0e19\\u0e27\\u0e42\\u0e19\\u0e49\\u0e21\\u0e43\\u0e19\\u0e23\\u0e30\\u0e14\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e40\\u0e02\\u0e49\\u0e32\\u0e1e\\u0e31\\u0e01</p>", "id": "bdf6ae50-56bd-4847-a438-018644493779"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"3e6tw\\"></p><h2 data-block-key=\\"2mrg3\\"><b>\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e07\\u0e48\\u0e32\\u0e22\\u0e14\\u0e32\\u0e22 - \\u0e40\\u0e1b\\u0e47\\u0e19\\u0e40\\u0e08\\u0e49\\u0e32\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e2d\\u0e07\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32</b></h2><p data-block-key=\\"8b7f5\\">\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e0a\\u0e48\\u0e27\\u0e22\\u0e04\\u0e38\\u0e13\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e15\\u0e34\\u0e14\\u0e15\\u0e32\\u0e21 \\u0e40\\u0e04\\u0e23\\u0e37\\u0e48\\u0e2d\\u0e07\\u0e08\\u0e31\\u0e01\\u0e23 HVAC \\u0e23\\u0e32\\u0e22\\u0e01\\u0e32\\u0e23\\u0e44\\u0e1f\\u0e1f\\u0e49\\u0e32 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e35\\u0e48\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e19\\u0e33\\u0e21\\u0e32\\u0e22\\u0e31\\u0e07\\u0e2a\\u0e16\\u0e32\\u0e19\\u0e17\\u0e35\\u0e48\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13 \\u0e14\\u0e39\\u0e04\\u0e48\\u0e32\\u0e40\\u0e2a\\u0e37\\u0e48\\u0e2d\\u0e21\\u0e23\\u0e32\\u0e04\\u0e32\\u0e02\\u0e2d\\u0e07\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e27\\u0e31\\u0e14\\u0e2d\\u0e32\\u0e22\\u0e38\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e14\\u0e49\\u0e27\\u0e22</p>", "id": "bc9c9b17-1c40-4b74-8efe-034176f41587"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"aktp4\\"></p><h2 data-block-key=\\"e3la1\\"><b>\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07 \\u0e41\\u0e25\\u0e30\\u0e41\\u0e2d\\u0e1e\\u0e21\\u0e37\\u0e2d\\u0e16\\u0e37\\u0e2d\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32</b></h2><p data-block-key=\\"9bmnm\\">\\u0e01\\u0e32\\u0e23\\u0e41\\u0e01\\u0e49\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32 \\u0e04\\u0e23\\u0e1a\\u0e27\\u0e07\\u0e08\\u0e23\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e38\\u0e01\\u0e04\\u0e27\\u0e32\\u0e21\\u0e15\\u0e49\\u0e2d\\u0e07\\u0e01\\u0e32\\u0e23\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32 \\u0e15\\u0e31\\u0e49\\u0e07\\u0e41\\u0e15\\u0e48\\u0e1a\\u0e31\\u0e19\\u0e17\\u0e36\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e1b\\u0e08\\u0e19\\u0e16\\u0e36\\u0e07\\u0e1b\\u0e23\\u0e30\\u0e01\\u0e32\\u0e28\\u0e41\\u0e25\\u0e30\\u0e1b\\u0e23\\u0e30\\u0e40\\u0e14\\u0e47\\u0e19\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e04\\u0e23\\u0e2d\\u0e1a\\u0e04\\u0e25\\u0e38\\u0e21\\u0e1b\\u0e23\\u0e30\\u0e40\\u0e14\\u0e47\\u0e19\\u0e2a\\u0e33\\u0e04\\u0e31\\u0e0d\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14\\u0e43\\u0e19\\u0e01\\u0e32\\u0e23\\u0e14\\u0e33\\u0e40\\u0e19\\u0e34\\u0e19\\u0e18\\u0e38\\u0e23\\u0e01\\u0e34\\u0e08\\u0e2b\\u0e25\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13</p>", "id": "8e558d7d-d11a-4ab9-a804-f74289653a35"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"axgmb\\"></p><h2 data-block-key=\\"e0s7\\"><b>\\u0e14\\u0e33\\u0e40\\u0e19\\u0e34\\u0e19\\u0e18\\u0e38\\u0e23\\u0e01\\u0e34\\u0e08\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e14\\u0e49\\u0e27\\u0e22\\u0e01\\u0e32\\u0e23\\u0e14\\u0e39\\u0e41\\u0e25\\u0e17\\u0e35\\u0e48\\u0e40\\u0e2b\\u0e21\\u0e32\\u0e30\\u0e2a\\u0e21\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e23\\u0e30\\u0e22\\u0e30 </b></h2><p data-block-key=\\"2b8a3\\">\\u0e04\\u0e27\\u0e1a\\u0e04\\u0e39\\u0e48\\u0e44\\u0e1b\\u0e01\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c \\u0e01\\u0e32\\u0e23\\u0e41\\u0e01\\u0e49\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32 \\u0e19\\u0e35\\u0e49\\u0e40\\u0e2b\\u0e21\\u0e32\\u0e30\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e22\\u0e34\\u0e48\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e41\\u0e25\\u0e30\\u0e2a\\u0e16\\u0e32\\u0e19\\u0e17\\u0e35\\u0e48\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13 \\u0e04\\u0e38\\u0e13\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e2a\\u0e23\\u0e49\\u0e32\\u0e07\\u0e07\\u0e32\\u0e19\\u0e40\\u0e0a\\u0e48\\u0e19 Cleaning</p>", "id": "5f2c115d-3399-4ae2-8416-ee8b183787c3"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"o0zk8\\"></p><h2 data-block-key=\\"fqcgj\\"><b>\\u0e20\\u0e32\\u0e1e\\u0e23\\u0e27\\u0e21 , \\u0e21\\u0e38\\u0e21\\u0e21\\u0e2d\\u0e07\\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e27\\u0e49\\u0e32\\u0e07\\u0e41\\u0e25\\u0e30\\u0e21\\u0e38\\u0e21\\u0e21\\u0e2d\\u0e07\\u0e41\\u0e1a\\u0e1a\\u0e40\\u0e08\\u0e32\\u0e30\\u0e25\\u0e36\\u0e01</b></h2><p data-block-key=\\"16pv9\\">\\u0e17\\u0e38\\u0e01\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e21\\u0e35\\u0e2d\\u0e22\\u0e39\\u0e48\\u0e43\\u0e19 BI \\u0e23\\u0e39\\u0e49\\u0e17\\u0e38\\u0e01\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e30\\u0e41\\u0e25\\u0e30\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e30\\u0e02\\u0e2d\\u0e07\\u0e18\\u0e38\\u0e23\\u0e01\\u0e34\\u0e08\\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e14\\u0e49\\u0e27\\u0e22 ELK Stack \\u0e17\\u0e35\\u0e48\\u0e02\\u0e31\\u0e1a\\u0e40\\u0e04\\u0e25\\u0e37\\u0e48\\u0e2d\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22 Query Search Tool \\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e37\\u0e1a\\u0e04\\u0e49\\u0e19\\u0e1a\\u0e31\\u0e19\\u0e17\\u0e36\\u0e01\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21</p>", "id": "1c57afa3-dd46-4a21-baa7-8cb2b3dce638"}]	ดึงดูดผู้เช่าได้อย่างลงตัว มอบประสบการณ์สุดพิเศษ	สิ่งอำนวยความสะดวก	ทรัพย์สิน	ผู้เช่าและลูกค้า	การซ่อมบำรุง	ข้อมูลเชิงลึก	จัดการผู้เช่า	[{"type": "cards", "value": {"title": "Testimonials", "cards": [{"title": "Alagu Shan", "text": "Crystal Clear Management", "subtitle": "You've saved our business! Thanks guys, keep up the good work! The best FMS Available!"}, {"title": "Vijesh Kamat", "text": "CEO, 3000 Apartment Chain, Bengaluru", "subtitle": "I strongly recommend Domitos for managing your real estate properties."}, {"title": "Raman Palaniappan", "text": "MES Group", "subtitle": "Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities"}, {"title": "John Joy", "text": "CEO, Backup International", "subtitle": "I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope"}, {"title": "Lim Jannat", "text": "UX/UI Designer", "subtitle": "You've saved our business! Thanks guys, keep up the good work! The best on the net!"}]}, "id": "b3509892-95b1-4d9e-8af0-69af423c6869"}]	โครงการทั้งหมด	ดูข่าวทั้งหมด
\.


--
-- Data for Name: taggit_tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.taggit_tag (id, name, slug) FROM stdin;
\.


--
-- Data for Name: taggit_taggeditem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.taggit_taggeditem (id, object_id, content_type_id, tag_id) FROM stdin;
\.


--
-- Data for Name: wagtail_localize_localesynchronization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_localesynchronization (id, locale_id, sync_from_id) FROM stdin;
1	2	1
\.


--
-- Data for Name: wagtail_localize_overridablesegment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_overridablesegment (id, "order", data_json, context_id, source_id) FROM stdin;
\.


--
-- Data for Name: wagtail_localize_relatedobjectsegment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_relatedobjectsegment (id, "order", context_id, object_id, source_id) FROM stdin;
\.


--
-- Data for Name: wagtail_localize_segmentoverride; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_segmentoverride (id, created_at, updated_at, data_json, has_error, field_error, context_id, last_translated_by_id, locale_id) FROM stdin;
\.


--
-- Data for Name: wagtail_localize_string; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_string (id, data_hash, data, locale_id) FROM stdin;
1	781f49ed-a476-5cd3-8602-e6c19c9958e3	Home	1
2	d6b6148d-7628-5c7b-b603-7ed96dfcb5a4	home	1
3	d6e0427c-d456-59b3-90dc-3a3366984e49	#1 Facility Management Software | Maintenance | Covid Management	1
4	d113f68a-e207-545f-92d4-fcf4f40fba53	Domitos provide maintenance &amp;amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.	1
5	b29eaa22-f9bd-5119-9198-49bbd4ee5d8b	Try 15 days free trial	1
6	3b4b331b-e432-5463-a304-ca9803dbdfe2	COVID Isolation &amp; Control	1
7	cb83c490-4b75-5ca5-b845-deafc3e612c9	Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease	1
8	e4dac7ba-0179-542d-a586-6754f35e03ea	Dont worry,we have covered End to End	1
9	1b71716d-19c8-5f3d-9bff-94be88027c64	Discover Facility Manager Core	1
10	c46e689a-4479-53a2-b7af-3ef138a4f18f	Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance	1
11	a964f9f6-3d49-5e5b-baf2-1291b242aa08	Strong Sales Management	1
12	b49cd05b-4dda-5148-ac07-18fda7e6a3a1	CRM specific to Facility Management	1
13	492c1c71-968c-5d81-b4ce-af30fe6b23ae	Tailor Made CRM for Facility Management.	1
14	7b7f7a33-339a-5b83-8129-7c33a8c28cc2	Asset Management &amp;amp; Maintenance	1
15	24e1c7b2-97c4-5a11-9946-bb827fad1288	Asset Management &amp; Maintenance	1
16	936aa98d-b955-5aa7-8ba1-67ffa1908399	Maintenance is the key pillar on maintaining your Facility quality and usability.	1
17	28ba84cc-1c77-5197-b3aa-b196af3f1acc	See Features	1
18	078e5d99-1e74-5939-803d-a6b16a599641	Discover More	1
19	38422bc3-19ca-5ecc-b131-abcb9397842b	Tenant &amp; Client Management	1
20	3ca349f5-2b9f-5436-a26a-a42b09c1e44e	Manage Clients &amp; Tenants through Single Portal.Get Payment Electronically powered by Stripe	1
21	8505b501-4914-5cbd-ab72-fb851c136c4c	Facility Management	1
22	098d5cd2-bbf9-54ab-9578-dd0fdc5316e2	Facility &amp; Property Management, Availability, Client Onboarding &amp; Reallocation.	1
23	6199bf95-34d9-5c5b-a28f-280fb43b29c2	Assets &amp; Maintenance	1
24	44cd13b5-f184-59c1-8cce-46be4dac2ff2	Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues	1
25	f030b56f-efed-5f56-869d-0ccd71fc412c	Seamlessly engage tenants Deliver exceptional experiences	1
26	a3bb9fd0-3c8c-53f1-bee6-361318ca2415	Facility	1
27	fb36e5f8-a326-5f09-bf49-1116d7a4cbc3	A Comprehensive control Tower for your Facility	1
28	e79b53c2-2ceb-5f68-bae5-ab3f05cb59c8	With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level	1
29	1a1f9ed7-f11c-5e73-8e64-14a61a7f7b3c	Assets	1
30	ba1e9461-1443-55b9-8144-1b20c3611bdd	Manage Assets with Ease - Self Owned as well as Client Assets	1
31	30c60e1d-eac0-5e1b-9b85-227dea5adcd7	Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well	1
32	fb153950-4e1e-52f8-b5ad-a72a3ad04d61	Tenants&amp;Clients	1
33	8f362aaa-d441-51ae-b74f-2d6875fd7eb1	Portals and Mobile apps for Clients as well as Tenants	1
34	01bee15a-6e8e-52e1-8ff4-d46be2b2b250	One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.	1
35	414e6736-9d52-5f20-b310-8c4fcd8ef401	Maintenance	1
36	7ba2abc5-1794-50d9-831c-5895fd08d66d	Run your business as with proper periodic care	1
37	14d22b8c-903d-5af8-8341-e8b9c7c55f8f	Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,	1
38	c859f2a6-94fe-5730-9902-bc4d8afffceb	Insights	1
39	44cebb89-cb6e-5bfe-a11a-79c6e7a2ffaf	Overview , Birds Eye View and Drilled Down View	1
40	c3acc352-3721-57c9-a3f0-4b94b4aebea2	Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries	1
41	de9ede9d-fe4d-554a-8763-041809602517	For Governments &amp; NGOs	1
42	5ad4d9a9-27ab-5735-9ee1-7c0429532e54	Manage COVID Facilities	1
43	2500ff53-17c8-5f7b-8631-503ca874c57a	Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world	1
44	2d960c18-729f-5077-8745-aece852f7726	Built Custom for COVID Isolation Centers	1
45	c957386c-9144-5109-9a3d-f2715952afe9	Manage Tenants and their details	1
46	3cf494ee-beb4-5db9-8854-61422bb6e875	Comprehensive Reports for Governments	1
47	52889e84-3d8e-53c0-9e2d-91ea55f5808b	Immediate Onboarding on AWS Cloud	1
48	5369eda5-494e-543c-98f9-7e63fdb22b87	Contact Us for a Quote	1
49	4782cbc3-d3c6-53d4-a8aa-6c4a0cbec498	Crafted with Future of Facility Management in mind.	1
50	6cc77521-8cff-52fb-88d8-14e57702e75f	ECONOMY	1
51	2eaf767c-9bfc-564a-bb97-4460831c4fea	Facility Management on Shared EC2 Instance	1
52	5fa3c6c3-a421-5860-9a62-90b4fbe742b8	Shared EC2 Cluster Deployment	1
53	8f870501-3339-5f3d-a82f-20ab1d371321	48 hrs response time	1
54	27d7eace-2d4c-5923-8a88-e728629aab01	Shared account management	1
55	e13863ee-4ba1-56e0-b96f-55ad72d8b810	STANDARD	1
56	29a02a99-7bff-55b0-9688-979f6630be9e	Facility Management on Dedicated EC2 Instance	1
57	be3cb5ab-1384-51f9-80fe-cb5b36b7ecc7	Dedicated EC2 Cluster Deployment	1
58	bd27bb35-cd7f-5ee7-be61-5f36b2971149	24 hrs response time	1
59	659a38ac-873e-5ae1-8c8e-969c39e78715	Dedicated Account Manager	1
60	17cb4e82-62ad-5b29-83fe-96641fd233e3	PREMIUM	1
61	864ec4d6-fa4f-596a-bd70-6e90a5c8843b	Facility Management on Higher Grade EC2 Instance	1
62	68e79811-74f1-5102-a02e-a3c636dd0bca	3 hrs response time	1
63	164472a0-4a12-5e88-bbbc-19ec75cf9767	All the modules and future updates included	1
64	6ba9e084-1bd3-5cd7-9a62-ee79e130db5d	Book Live Demo	1
65	8149ec41-4a14-59ba-be2a-b102a0b71d15	TESTIMONIALS	1
66	a83e4710-e611-56ba-a93f-798ed80a05d1	Our Customers Reviews	1
67	fbc958a7-dcf5-5ee9-9b4d-b74d844fa28c	Testimonials	1
68	d95902c4-b076-51bb-99c0-ba378b3b8b13	Total Projects	1
69	64d0cedd-6f87-5df5-a42d-eda5fbf58d58	Units	1
70	6b4cc902-f87d-5d77-989d-cb18e971b348	Tenants Managed	1
71	99b712d6-4d0f-56ae-b40c-c5af30b76aa7	LATEST INFO	1
72	7038d897-9e09-54fb-8b38-2ab0ff83ee4c	Our Latest News	1
73	e20d64fc-c0e3-5f37-b87d-19b1bf8ff098	VIEW ALL NEWS	1
74	bb3cf8b2-73f0-5094-913f-8992b1232cee	Request a demo to see how Domitos can supercharge your Facility Team.	1
75	a6b7ab8a-f198-52a0-b280-5ba2c586d4e7	REQUEST A DEMO	1
\.


--
-- Data for Name: wagtail_localize_stringsegment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_stringsegment (id, "order", attrs, context_id, source_id, string_id) FROM stdin;
1	1	null	1	1	1
2	2	null	2	1	2
3	3	null	3	1	3
4	4	null	4	1	4
5	5	null	5	1	5
6	7	null	6	1	6
7	9	null	7	1	7
8	10	null	8	1	8
9	12	null	9	1	9
10	14	null	10	1	10
11	15	null	11	1	11
12	17	null	12	1	12
13	19	null	13	1	13
14	20	null	14	1	14
15	22	null	15	1	15
16	24	null	16	1	16
17	25	null	17	1	17
18	26	null	18	1	18
19	27	null	19	1	18
20	28	null	20	1	18
21	29	null	21	1	19
22	30	null	22	1	20
23	31	null	23	1	21
24	32	null	24	1	22
25	33	null	25	1	23
26	34	null	26	1	24
27	35	null	27	1	25
28	36	null	28	1	26
29	38	null	29	1	27
30	39	null	29	1	28
31	40	null	30	1	29
32	42	null	31	1	30
33	43	null	31	1	31
34	44	null	32	1	32
35	46	null	33	1	33
36	47	null	33	1	34
37	48	null	34	1	35
38	50	null	35	1	36
39	51	null	35	1	37
40	52	null	36	1	38
41	54	null	37	1	39
42	55	null	37	1	40
43	56	null	38	1	41
44	58	null	39	1	42
45	59	null	39	1	43
46	60	null	39	1	44
47	61	null	39	1	45
48	62	null	39	1	46
49	63	null	39	1	47
50	65	null	40	1	48
51	66	null	40	1	49
52	67	null	41	1	50
53	69	null	42	1	51
54	71	null	43	1	52
55	73	null	44	1	53
56	75	null	45	1	54
57	76	null	46	1	55
58	77	null	47	1	56
59	78	null	48	1	57
60	79	null	49	1	58
61	80	null	50	1	59
62	81	null	51	1	60
63	83	null	52	1	61
64	85	null	53	1	57
65	87	null	54	1	62
66	89	null	55	1	59
67	91	null	56	1	63
68	92	null	57	1	64
69	94	null	58	1	65
70	95	null	58	1	66
71	96	null	59	1	67
72	97	null	60	1	68
73	98	null	61	1	69
74	99	null	62	1	70
75	101	null	63	1	71
76	102	null	63	1	72
77	103	null	64	1	73
78	104	null	65	1	74
79	105	null	66	1	75
\.


--
-- Data for Name: wagtail_localize_stringtranslation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_stringtranslation (id, data, created_at, updated_at, context_id, locale_id, translation_of_id, tool_name, translation_type, last_translated_by_id, field_error, has_error) FROM stdin;
1	การแยกและควบคุมโควิด	2021-10-20 12:09:04.479962+05:30	2021-10-20 12:09:04.479962+05:30	6	2	6		manual	1		f
2	ค้นพบว่า Domitos สามารถช่วยเหลือรัฐบาลและ NGOs ในการจัดการสิ่งอําานวยความสะดวกในการแยกเชื้อ COVID ได้อย่างง่ายดาย	2021-10-20 12:09:26.862165+05:30	2021-10-20 12:09:26.862165+05:30	7	2	7		manual	1		f
3	ค้นหาผู้จัดการสิ่งอําานวยความสะดวก 	2021-10-20 12:09:51.278393+05:30	2021-10-20 12:09:51.278393+05:30	9	2	9		manual	1		f
4	การแก้ปั ญหาหลัก เหมาะกับความต้องการหลักทั ้งหมดสําาหรับธุรกิจสิ่งอําานวยความสะดวกของคุณ ตั ้งแต่การขายไปจนถึงการบําารุงรักษา	2021-10-20 12:10:09.180058+05:30	2021-10-20 12:10:09.180058+05:30	10	2	10		manual	1		f
5	การบริหารจัดการลูกค้าสัมพันธ์ เฉพาะสำหรับการจัดการสิ่งอำนวยความ	2021-10-20 12:11:29.361368+05:30	2021-10-20 12:11:29.361368+05:30	12	2	12		manual	1		f
6	การออกแบบการบริหารจัดการลูกค้าสัมพันธ์เฉพาะกลุ่ม  สำหรับการจัดการสิ่งอำนวยความสะดวก	2021-10-20 12:11:58.071354+05:30	2021-10-20 12:11:58.071354+05:30	13	2	13		manual	1		f
7	การจัดการและบำรุงรักษาสินทรัพย์	2021-10-20 12:12:10.684359+05:30	2021-10-20 12:12:10.684359+05:30	15	2	15		manual	1		f
8	การบำรุงรักษาเป็นเสาหลักในการรักษาคุณภาพและความสามารถในการใช้งานของโรงงานของคุณ	2021-10-20 12:12:21.798984+05:30	2021-10-20 12:12:21.798984+05:30	16	2	16		manual	1		f
9	การจัดการผู้เช่าและลูกค้า	2021-10-20 12:13:14.548162+05:30	2021-10-20 12:13:14.548162+05:30	21	2	19		manual	1		f
10	จัดการลูกค้าและผู้เช่าผ่านช่องทางเดียว รับการชำระเงินด้วยระบบอิเล็กทรอนิกส์โดย Stripe	2021-10-20 12:13:35.399828+05:30	2021-10-20 12:13:35.399828+05:30	22	2	20		manual	1		f
11	การจัดการสิ่งอำนวยความสะดวก	2021-10-20 12:14:16.543513+05:30	2021-10-20 12:14:16.543513+05:30	23	2	21		manual	1		f
12	การจัดการสิ่งอำนวยความสะดวกและทรัพย์สิน ความพร้อมใช้งาน การเริ่มต้นใช้งานของลูกค้า & การจัดสรรใหม่	2021-10-20 12:14:26.466603+05:30	2021-10-20 12:14:26.466603+05:30	24	2	22		manual	1		f
13	สินทรัพย์และการบำรุงรักษา	2021-10-20 12:14:33.744357+05:30	2021-10-20 12:14:33.74539+05:30	25	2	23		manual	1		f
14	ดึงดูดผู้เช่าได้อย่างลงตัว มอบประสบการณ์สุดพิเศษ	2021-10-20 12:16:35.692509+05:30	2021-10-20 12:16:35.692509+05:30	27	2	25		manual	1		f
15	จัดการทรัพย์สินของลูกค้าและทรัพย์สินขององค์กร รักษาเชิงรุก จัดการปัญหา	2021-10-20 12:16:36.525427+05:30	2021-10-20 12:16:36.525427+05:30	26	2	24		manual	1		f
16	สิ่งอำนวยความสะดวก	2021-10-20 12:18:01.09751+05:30	2021-10-20 12:18:01.09751+05:30	28	2	26		manual	1		f
17	หอควบคุมที่ครอบคลุมสำหรับสิ่งอำนวยความสะดวกของคุณ	2021-10-20 12:18:11.726824+05:30	2021-10-20 12:18:11.726824+05:30	29	2	27		manual	1		f
18	ด้วยภาพรวมสิ่งอำนวยความสะดวก คุณสามารถทราบสิ่งอำนวยความสะดวกภายใต้บัญชีของคุณ ดูอัตราการเข้าพัก ลูกค้าปัจจุบัน ความพร้อมในการจัดซื้อตามขวางหรือตามหอคอย และคาดการณ์แนวโน้มในระดับการเข้าพัก	2021-10-20 12:18:20.884739+05:30	2021-10-20 12:18:20.884739+05:30	29	2	28		manual	1		f
19	ทรัพย์สิน	2021-10-20 12:18:31.14158+05:30	2021-10-20 12:18:31.14158+05:30	30	2	29		manual	1		f
20	จัดการสินทรัพย์ได้อย่างง่ายดาย - เป็นเจ้าของเองและทรัพย์สินของลูกค้า	2021-10-20 12:18:40.134474+05:30	2021-10-20 12:18:40.134474+05:30	31	2	30		manual	1		f
21	การจัดการสินทรัพย์ช่วยคุณในการติดตาม เครื่องจักร HVAC รายการไฟฟ้า รวมถึงทรัพย์สินที่ลูกค้านำมายังสถานที่ของคุณ ดูค่าเสื่อมราคาของสินทรัพย์และวัดอายุสินทรัพย์ด้วย	2021-10-20 12:18:58.69169+05:30	2021-10-20 12:18:58.69169+05:30	31	2	31		manual	1		f
22	ผู้เช่าและลูกค้า	2021-10-20 12:19:09.643702+05:30	2021-10-20 12:19:09.643702+05:30	32	2	32		manual	1		f
23	ช่องทาง และแอพมือถือสำหรับลูกค้าและผู้เช่า	2021-10-20 12:19:24.698838+05:30	2021-10-20 12:19:24.698838+05:30	33	2	33		manual	1		f
24	การแก้ปัญหา ครบวงจรสำหรับทุกความต้องการของลูกค้าและผู้เช่า ตั้งแต่บันทึกของผู้เช่าไปจนถึงประกาศและประเด็นการชำระเงิน การจัดการลูกค้าครอบคลุมประเด็นสำคัญทั้งหมดในการดำเนินธุรกิจหลักของคุณ	2021-10-20 12:19:35.62288+05:30	2021-10-20 12:19:35.62288+05:30	33	2	34		manual	1		f
25	การซ่อมบำรุง	2021-10-20 12:20:16.469644+05:30	2021-10-20 12:20:16.469644+05:30	34	2	35		manual	1		f
26	ดำเนินธุรกิจของคุณด้วยการดูแลที่เหมาะสมเป็นระยะ 	2021-10-20 12:20:25.022072+05:30	2021-10-20 12:20:25.022072+05:30	35	2	36		manual	1		f
27	ควบคู่ไปกับการจัดการสินทรัพย์ การแก้ปัญหา นี้เหมาะอย่างยิ่งสำหรับการบำรุงรักษาทรัพย์สินและสถานที่ของคุณ คุณสามารถสร้างงานเช่น Cleaning	2021-10-20 12:20:33.708786+05:30	2021-10-20 12:20:33.708786+05:30	35	2	37		manual	1		f
28	ข้อมูลเชิงลึก	2021-10-20 12:20:41.043104+05:30	2021-10-20 12:20:41.043104+05:30	36	2	38		manual	1		f
29	ภาพรวม , มุมมองแบบกว้างและมุมมองแบบเจาะลึก	2021-10-20 12:20:51.462236+05:30	2021-10-20 12:20:51.462236+05:30	37	2	39		manual	1		f
30	ทุกอย่างมีอยู่ใน BI รู้ทุกจังหวะและจังหวะของธุรกิจของคุณด้วย ELK Stack ที่ขับเคลื่อนด้วย Query Search Tool ของเราเองสำหรับการสืบค้นบันทึกเพิ่มเติม	2021-10-20 12:21:00.432163+05:30	2021-10-20 12:21:00.432163+05:30	37	2	40		manual	1		f
31	สำหรับรัฐบาลและองค์กรพัฒนาเอกชน	2021-10-20 12:21:08.943447+05:30	2021-10-20 12:21:08.943447+05:30	38	2	41		manual	1		f
32	จัดการสิ่งอำนวยความสะดวกโควิด	2021-10-20 12:21:17.348967+05:30	2021-10-20 12:21:17.348967+05:30	39	2	42		manual	1		f
33	การแก้ปัญหา ได้รับการพัฒนาร่วมกับรัฐบาลสิงคโปร์และพร้อมให้บริการสำหรับรัฐบาลและองค์กรพัฒนาเอกชนทั่วโลก	2021-10-20 12:21:36.983039+05:30	2021-10-20 12:21:36.983039+05:30	39	2	43		manual	1		f
34	สร้างขึ้นเองสำหรับศูนย์กักกันโควิด	2021-10-20 12:21:55.142012+05:30	2021-10-20 12:21:55.142012+05:30	39	2	44		manual	1		f
35	จัดการผู้เช่าและรายละเอียดของพวกเขา	2021-10-20 12:22:03.233446+05:30	2021-10-20 12:22:03.233446+05:30	39	2	45		manual	1		f
36	รายงานที่ครอบคลุมสำหรับรัฐบาล	2021-10-20 12:22:11.209354+05:30	2021-10-20 12:22:11.209354+05:30	39	2	46		manual	1		f
37	การเริ่มต้นใช้งานทันทีบน AWS Cloud	2021-10-20 12:22:17.277627+05:30	2021-10-20 12:22:17.277627+05:30	39	2	47		manual	1		f
38	ติดต่อเราเพื่อขอใบเสนอราคา	2021-10-20 12:22:25.713591+05:30	2021-10-20 12:22:25.714587+05:30	40	2	48		manual	1		f
39	สร้างขึ้นโดยคำนึงถึงอนาคตของการจัดการสิ่งอำนวยความสะดวก	2021-10-20 12:22:36.232538+05:30	2021-10-20 12:22:36.232538+05:30	40	2	49		manual	1		f
40	ข้อความรับรอง 	2021-10-20 12:23:02.402862+05:30	2021-10-20 12:23:02.402862+05:30	58	2	65		manual	1		f
41	รีวิวลูกค้าของเรา	2021-10-20 12:23:09.205444+05:30	2021-10-20 12:23:09.205444+05:30	58	2	66		manual	1		f
42	โครงการทั้งหมด	2021-10-20 12:23:22.164584+05:30	2021-10-20 12:23:22.164584+05:30	60	2	68		manual	1		f
43	หน่วย	2021-10-20 12:23:28.251044+05:30	2021-10-20 12:23:28.251044+05:30	61	2	69		manual	1		f
44	จัดการผู้เช่า	2021-10-20 12:23:35.49971+05:30	2021-10-20 12:23:35.49971+05:30	62	2	70		manual	1		f
45	ข้อมูลล่าสุด   	2021-10-20 12:23:47.228092+05:30	2021-10-20 12:23:47.228092+05:30	63	2	71		manual	1		f
46	ข่าวล่าสุดของเรา	2021-10-20 12:23:54.546599+05:30	2021-10-20 12:23:54.546599+05:30	63	2	72		manual	1		f
47	ดูข่าวทั้งหมด	2021-10-20 12:24:15.069756+05:30	2021-10-20 12:24:15.069756+05:30	64	2	73		manual	1		f
48	ขอการสาธิตเพื่อดูว่า Domitos สามารถเพิ่มพลังให้กับทีม Facility ของคุณได้อย่างไร	2021-10-20 12:24:25.470743+05:30	2021-10-20 12:24:25.470743+05:30	65	2	74		manual	1		f
49	ขอตัวอย่าง	2021-10-20 12:24:32.430422+05:30	2021-10-20 12:24:32.430422+05:30	66	2	75		manual	1		f
50	ดูคุณสมบัติ	2021-10-20 12:25:09.87454+05:30	2021-10-20 12:25:09.87454+05:30	17	2	17		manual	1		f
51	ค้นพบเพิ่มเติม	2021-10-20 12:25:23.22957+05:30	2021-10-20 12:25:23.22957+05:30	18	2	18		manual	1		f
52	ค้นพบเพิ่มเติม	2021-10-20 12:25:25.228142+05:30	2021-10-20 12:25:25.228142+05:30	19	2	18		manual	1		f
53	ค้นพบเพิ่มเติม	2021-10-20 12:25:27.795978+05:30	2021-10-20 12:25:27.795978+05:30	20	2	18		manual	1		f
54	#1 ซอฟต์แวร์การจัดการสิ่งอำนวยความสะดวก | การบำรุงรักษา | การจัดการโควิด	2021-10-20 12:25:47.269567+05:30	2021-10-20 12:25:47.269567+05:30	3	2	3		manual	1		f
55	Domitos ให้การบำรุงรักษา &amp;amp; ซอฟต์แวร์การจัดการสิ่งอำนวยความสะดวกเพื่อตรวจสอบและกำหนดคำสั่งงาน รวมถึงโมดูลการจัดการ covid-19 แบบกำหนดเองสำหรับที่พักของพนักงาน	2021-10-20 12:26:04.749001+05:30	2021-10-20 12:26:04.749001+05:30	4	2	4		manual	1		f
56	บ้าน	2021-10-20 12:28:54.907828+05:30	2021-10-20 12:28:54.907828+05:30	2	2	2		manual	1		f
57	บ้าน	2021-10-20 12:28:58.693824+05:30	2021-10-20 12:28:58.693824+05:30	1	2	1		manual	1		f
\.


--
-- Data for Name: wagtail_localize_template; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_template (id, uuid, template, template_format, string_count) FROM stdin;
1	4403262c-6607-55f8-8f81-ce3454da7a0b	<h2 data-block-key="s3h96"><text position="0"></text></h2>	html	1
2	e37f6ae7-b7e3-5217-a336-083b13808704	<p data-block-key="fuftu"><text position="0"></text></p>	html	1
3	a47e1a14-2374-5fcc-b421-e4777c076c43	<h2 data-block-key="0ua3x"><text position="0"></text></h2>	html	1
4	92d4b5a6-c3eb-5979-bed7-8c2d536bdf2f	<p data-block-key="52958"><text position="0"></text></p>	html	1
5	a00fb197-53a8-590e-b0c7-014f4ee655f2	<h2 data-block-key="l2zlg"><text position="0"></text></h2>	html	1
6	3ded831f-c674-510a-af94-f412e5dfb69a	<p data-block-key="hwczy"><text position="0"></text></p>	html	1
7	10530006-4d2e-5e50-a8dd-0dae3a80b102	<h2 data-block-key="kfzag"><text position="0"></text></h2>	html	1
8	7d633282-86ab-53cd-9a5f-780e1ff9cecc	<p data-block-key="1mqod"><text position="0"></text></p>	html	1
9	b75e0049-bdfd-50b6-88b1-bb0c97904a93	<p data-block-key="mj8vl"></p><h2 data-block-key="7156n"><text position="0"></text></h2><p data-block-key="43hpa"><text position="1"></text></p>	html	2
10	65fa4b85-63a4-5e2a-b53d-8cf97f8404f4	<p data-block-key="3e6tw"></p><h2 data-block-key="2mrg3"><b><text position="0"></text></b></h2><p data-block-key="8b7f5"><text position="1"></text></p>	html	2
11	fdfed26f-0ed5-5236-858b-2685cf86ec72	<p data-block-key="aktp4"></p><h2 data-block-key="e3la1"><b><text position="0"></text></b></h2><p data-block-key="9bmnm"><text position="1"></text></p>	html	2
12	5ce916ea-d538-527b-bb16-61319cfdbc97	<p data-block-key="axgmb"></p><h2 data-block-key="e0s7"><b><text position="0"></text></b></h2><p data-block-key="2b8a3"><text position="1"></text></p>	html	2
13	77ece2ec-d71f-5e62-aa5f-e8cbb8d20f82	<p data-block-key="o0zk8"></p><h2 data-block-key="fqcgj"><b><text position="0"></text></b></h2><p data-block-key="16pv9"><text position="1"></text></p>	html	2
14	d960a1a7-ce8c-56d8-88cd-847a469c65e6	<p data-block-key="of16i"></p><h2 data-block-key="np5p"><text position="0"></text></h2><p data-block-key="eediu"><text position="1"></text></p><ul><li data-block-key="7i7la"><text position="2"></text></li></ul><p data-block-key="flmnh"></p><ul><li data-block-key="4g6pe"><text position="3"></text></li></ul><p data-block-key="9vsqq"></p><ul><li data-block-key="3k4cf"><text position="4"></text></li></ul><p data-block-key="1bo4i"></p><ul><li data-block-key="ekc7c"><text position="5"></text></li></ul>	html	6
15	7f9ccba2-6498-517d-a7c4-edb79874b9c5	<p data-block-key="9xoig"></p><h2 data-block-key="4drha"><text position="0"></text></h2><p data-block-key="9mptk"><text position="1"></text></p>	html	2
16	aa8e104e-02b2-5299-afad-f43ff473d805	<p data-block-key="rybf4"></p><p data-block-key="d0pa2"><text position="0"></text></p>	html	1
17	8894b619-c733-5a07-9016-d4b24d839cc4	<p data-block-key="znzpz"></p><p data-block-key="ejoa"><text position="0"></text></p>	html	1
18	0805c88e-7349-56d0-a5b0-fc099a8917af	<p data-block-key="znzpz"></p><p data-block-key="3uc8l"><text position="0"></text></p>	html	1
19	395a4e37-fe7a-5c8c-bbe4-32fc4c020dfc	<p data-block-key="znzpz"></p><p data-block-key="7i19r"><text position="0"></text></p>	html	1
20	f817dd4e-95ae-5571-bbd4-6281231a3b82	<p data-block-key="5m0r3"></p><p data-block-key="7v5na"><text position="0"></text></p>	html	1
21	7a97be25-5c34-585c-b1b2-25e21616221c	<p data-block-key="5m0r3"></p><p data-block-key="39n9f"><text position="0"></text></p>	html	1
22	aad2ac84-f495-5b7a-af89-d93d82594a3a	<p data-block-key="5m0r3"></p><p data-block-key="s2fj"><text position="0"></text></p>	html	1
23	c0e0d11e-ba25-553e-8c4f-340e038436c7	<p data-block-key="5m0r3"></p><p data-block-key="ckaro"><text position="0"></text></p>	html	1
24	c0c81308-b43f-52cb-b805-704418f11691	<p data-block-key="5m0r3"></p><p data-block-key="9v11e"><text position="0"></text></p>	html	1
25	89a2b5b5-441d-51fe-88e5-46b88b761fa5	<p data-block-key="y9ujp"></p><p data-block-key="6rp38"><text position="0"></text></p><h2 data-block-key="4lbnj"><text position="1"></text></h2>	html	2
26	556ea0b4-56d9-5a6d-8e85-c52f3d2c1b52	<p data-block-key="m6mif"></p><p data-block-key="c4ns0"><text position="0"></text></p><h2 data-block-key="at6tj"><text position="1"></text></h2>	html	2
\.


--
-- Data for Name: wagtail_localize_templatesegment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_templatesegment (id, "order", context_id, source_id, template_id) FROM stdin;
1	6	6	1	1
2	8	7	1	2
3	11	9	1	3
4	13	10	1	4
5	16	12	1	5
6	18	13	1	6
7	21	15	1	7
8	23	16	1	8
9	37	29	1	9
10	41	31	1	10
11	45	33	1	11
12	49	35	1	12
13	53	37	1	13
14	57	39	1	14
15	64	40	1	15
16	68	42	1	16
17	70	43	1	17
18	72	44	1	18
19	74	45	1	19
20	82	52	1	20
21	84	53	1	21
22	86	54	1	22
23	88	55	1	23
24	90	56	1	24
25	93	58	1	25
26	100	63	1	26
\.


--
-- Data for Name: wagtail_localize_translatableobject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_translatableobject (translation_key, content_type_id) FROM stdin;
0715f2f3-4350-4922-9a2d-1376f7d73632	1
\.


--
-- Data for Name: wagtail_localize_translation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_translation (id, uuid, created_at, source_last_updated_at, translations_last_updated_at, destination_last_updated_at, enabled, source_id, target_locale_id) FROM stdin;
1	f8ab5dcd-4f7f-4d58-9b17-4d8fd903d835	2021-10-20 12:07:37.918508+05:30	2021-10-20 12:07:37.918508+05:30	\N	\N	t	1	2
\.


--
-- Data for Name: wagtail_localize_translationcontext; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_translationcontext (id, path_id, path, object_id) FROM stdin;
1	f749566c-8a06-5c72-89b8-682e4e49f74f	title	0715f2f3-4350-4922-9a2d-1376f7d73632
2	d71ad27b-d7c2-5061-865d-752015b09e05	slug	0715f2f3-4350-4922-9a2d-1376f7d73632
3	6ad86517-6e91-5948-b40c-39d7295a9000	header_title	0715f2f3-4350-4922-9a2d-1376f7d73632
4	de3bdf7b-4c83-52a9-a8a6-be41b8ce5cd5	meta_content	0715f2f3-4350-4922-9a2d-1376f7d73632
5	1b31ab07-d2f6-5452-8e7e-cc4c4d48be08	covid_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
6	1604474c-db33-5f02-aaa8-c5cb25c20662	covid_content1.00ed78fc-4a89-46cc-9752-2a817d3390c5	0715f2f3-4350-4922-9a2d-1376f7d73632
7	c43fa1fb-4691-5c94-a9b9-ee96db8e0dfa	covid_content2.62a55662-0ef3-45c1-bcc9-7afac1b36fde	0715f2f3-4350-4922-9a2d-1376f7d73632
8	a741a857-d4e1-514c-88e4-e21f3068aac8	facility_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
9	b6cb2017-f790-5250-b4c9-d64f186214d6	facility_content1.d01ee00d-3ca2-4bed-948a-9e7daa08e504	0715f2f3-4350-4922-9a2d-1376f7d73632
10	31628ccd-ce61-5651-9f8d-d721b03a404a	facility_content2.55869e56-326c-4d33-bf60-547ef4852647	0715f2f3-4350-4922-9a2d-1376f7d73632
11	379f3f3c-e7cd-56af-976d-c1673cceff1a	crm_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
12	f1e35e0a-7aba-55cd-b310-b22f58ea757c	crm_content1.34abb62a-afdd-470a-85e9-1656f878be54	0715f2f3-4350-4922-9a2d-1376f7d73632
13	c096860f-e8e4-5f63-9dd6-0c5e8c0b22b2	crm_content2.06253dd1-cdea-4a5a-9364-7231d9086ed7	0715f2f3-4350-4922-9a2d-1376f7d73632
14	4e8b6d80-2b54-5424-a8c4-334a75709f04	assests_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
15	f92b8e32-1c58-555c-a335-43a2f9c94e57	assests_content1.8470a818-9219-489a-b823-4530f5aa6ee7	0715f2f3-4350-4922-9a2d-1376f7d73632
16	4a70f034-5661-5fc3-a55b-589c738c3c60	assests_content2.72f87fd1-2677-42f6-86da-e9aa59b6c94b	0715f2f3-4350-4922-9a2d-1376f7d73632
17	bfaee82d-69f8-5835-9ffc-da3bfe25a223	button1	0715f2f3-4350-4922-9a2d-1376f7d73632
18	34a85006-252d-5d0c-8d71-36c47e1efa48	button2	0715f2f3-4350-4922-9a2d-1376f7d73632
19	b2a18dda-4fb5-5a04-bc43-f1c0e1438f7d	button3	0715f2f3-4350-4922-9a2d-1376f7d73632
20	edb41c7c-349c-525f-973a-9101acfff5d0	button4	0715f2f3-4350-4922-9a2d-1376f7d73632
21	4e3354f0-9ac1-5d9f-8f04-bc1c00f1fce3	box_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
22	d5c68d48-82a7-57b6-a813-5d2ac500ff55	box_text1	0715f2f3-4350-4922-9a2d-1376f7d73632
23	8cf65000-d190-5b4a-b6cf-030b5f7bd226	box_title2	0715f2f3-4350-4922-9a2d-1376f7d73632
24	e13bac8c-1f9d-5318-9874-26359efbb6d8	box_text2	0715f2f3-4350-4922-9a2d-1376f7d73632
25	b9a419b8-c512-5582-9ed8-3962a1a7f121	box_title3	0715f2f3-4350-4922-9a2d-1376f7d73632
26	97199182-a8be-5615-82bc-b45e3ec1b692	box_text3	0715f2f3-4350-4922-9a2d-1376f7d73632
27	6ffb9f9b-0283-5cce-9b31-89a90ae5dfb5	snippet_title	0715f2f3-4350-4922-9a2d-1376f7d73632
28	5280bb6b-6695-5456-8f38-dee5ba49d206	snippet_title1	0715f2f3-4350-4922-9a2d-1376f7d73632
29	1be37be2-e0d2-5c2c-b0a5-19424119efcf	snippet_content1.bdf6ae50-56bd-4847-a438-018644493779	0715f2f3-4350-4922-9a2d-1376f7d73632
30	94dff156-00c2-59d2-b361-88d5525c8e63	snippet_title2	0715f2f3-4350-4922-9a2d-1376f7d73632
31	b3b3b72b-4c71-5af1-9deb-00fade7e4a72	snippet_content2.bc9c9b17-1c40-4b74-8efe-034176f41587	0715f2f3-4350-4922-9a2d-1376f7d73632
32	e66fdaab-e88e-5ede-805e-e3ea087abe07	snippet_title3	0715f2f3-4350-4922-9a2d-1376f7d73632
33	a1e87399-add6-5227-af03-6e6a33e7c25a	snippet_content3.8e558d7d-d11a-4ab9-a804-f74289653a35	0715f2f3-4350-4922-9a2d-1376f7d73632
34	8a5ab757-4b65-5fda-82cc-2061e57df921	snippet_title4	0715f2f3-4350-4922-9a2d-1376f7d73632
35	25017f79-948c-5b04-8861-798875a6df34	snippet_content4.5f2c115d-3399-4ae2-8416-ee8b183787c3	0715f2f3-4350-4922-9a2d-1376f7d73632
36	f8f8aae4-6149-5262-89ff-98ee60a4a96f	snippet_title5	0715f2f3-4350-4922-9a2d-1376f7d73632
37	87f7b668-c409-5868-be21-2017eefcd0aa	snippet_content5.1c57afa3-dd46-4a21-baa7-8cb2b3dce638	0715f2f3-4350-4922-9a2d-1376f7d73632
38	612bafa2-748e-566f-8345-11e52c214490	manage_covid_subtitle	0715f2f3-4350-4922-9a2d-1376f7d73632
39	50705f74-615f-5d94-bb2f-a3a67fd9d3f3	manage_covid_content.3cbdb5c7-fb5b-4f38-aea5-242f742b6be4	0715f2f3-4350-4922-9a2d-1376f7d73632
40	96a7eb28-4941-5d73-9c89-1cdf341a05c3	quote_content.307f13cc-eaba-48c7-8470-35a6188d48ca	0715f2f3-4350-4922-9a2d-1376f7d73632
41	20497349-2572-5005-ae81-4affb9da7ffc	plan1	0715f2f3-4350-4922-9a2d-1376f7d73632
42	312b28cf-dd90-5c6a-bd0b-631342afa1f8	plan1_content.73225f3d-3f21-438b-8be8-751e85089de9	0715f2f3-4350-4922-9a2d-1376f7d73632
43	cf0b6671-4dde-5563-9606-526e10e96fd6	plan1_content.20cc318b-ae52-4570-b0b1-19c66e90e358	0715f2f3-4350-4922-9a2d-1376f7d73632
44	e6b0e4da-d8b7-59cb-93ca-cb08d24198c7	plan1_content.0ba138eb-0f8a-478d-9b2e-b2cc43329d6c	0715f2f3-4350-4922-9a2d-1376f7d73632
45	996eecd5-d6e5-5e54-bd3d-4bb4bd345882	plan1_content.1dd696b2-fc13-480e-8db3-08421bd2f8a4	0715f2f3-4350-4922-9a2d-1376f7d73632
46	3cbd5f4c-0b9b-5156-b992-fcce16e5799f	plan2	0715f2f3-4350-4922-9a2d-1376f7d73632
47	e780234c-b900-5a02-8aa3-51962b60799d	plan2_point1	0715f2f3-4350-4922-9a2d-1376f7d73632
48	687467e0-96b6-523b-a4f0-1ef2d88bed96	plan2_point2	0715f2f3-4350-4922-9a2d-1376f7d73632
49	764eea32-e011-5a2e-b16a-f4171babad1c	plan2_point3	0715f2f3-4350-4922-9a2d-1376f7d73632
50	5d1ac3a6-b89b-5bf7-97ed-ef14623c72e8	plan2_point4	0715f2f3-4350-4922-9a2d-1376f7d73632
51	a94a2e56-97ef-5992-a067-c2fac994f082	plan3	0715f2f3-4350-4922-9a2d-1376f7d73632
52	eaabde48-b1ff-5e0c-97bc-9c3a0ff3d6cf	plan3_content.1ca23272-4759-467d-ad56-f889dd9e8481	0715f2f3-4350-4922-9a2d-1376f7d73632
53	d1d7f39d-c020-5237-b23a-0667e3095c53	plan3_content.47299893-5a35-483d-b320-10f61f490999	0715f2f3-4350-4922-9a2d-1376f7d73632
54	026c7c8e-68db-5679-ab82-1ed45f200b15	plan3_content.486621b0-aabd-4ca5-88ae-24469851ff6a	0715f2f3-4350-4922-9a2d-1376f7d73632
55	4e5674e2-efe9-5acc-bb6b-bdf5cb773dde	plan3_content.a59b703c-b7c0-4770-b019-f946529f293b	0715f2f3-4350-4922-9a2d-1376f7d73632
56	1e81898f-4ada-52fd-b909-d935b577bf0c	plan3_content.e410a78f-fe33-444a-a891-8605c51dce01	0715f2f3-4350-4922-9a2d-1376f7d73632
57	577b59fd-5222-582a-a62c-1c2331a617ba	plan_button	0715f2f3-4350-4922-9a2d-1376f7d73632
58	d5f0ce16-e2f5-51e9-81b1-0692f5d48102	customer_content.9a1643ca-9c3e-478c-b22a-bfb7005544a9	0715f2f3-4350-4922-9a2d-1376f7d73632
59	5f60be2c-df5e-55c4-91d8-8487f113e0f9	testimonials.b3509892-95b1-4d9e-8af0-69af423c6869.title	0715f2f3-4350-4922-9a2d-1376f7d73632
60	a05be727-5d33-5002-8b7d-1af7fe3f14f8	total_project	0715f2f3-4350-4922-9a2d-1376f7d73632
61	60102a5c-6704-5f20-b548-ab1a29f6d548	project_unit	0715f2f3-4350-4922-9a2d-1376f7d73632
62	87d467c8-5ffa-56a2-85b9-938b666cb68a	tenants_managed	0715f2f3-4350-4922-9a2d-1376f7d73632
63	2e4cbab7-7668-508b-a238-0497aa952cbc	news_content.84399c1e-8505-4bbe-be8c-fe41ccd293e7	0715f2f3-4350-4922-9a2d-1376f7d73632
64	a03c3bcc-4436-53cd-a961-e0cca37ab734	view_button	0715f2f3-4350-4922-9a2d-1376f7d73632
65	5e3671f0-9d93-51b2-b2f9-2ca399569eca	requestcontent	0715f2f3-4350-4922-9a2d-1376f7d73632
66	f59b3bc9-d85c-5d26-be2b-de96162f99d7	requestbutton	0715f2f3-4350-4922-9a2d-1376f7d73632
\.


--
-- Data for Name: wagtail_localize_translationlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_translationlog (id, created_at, locale_id, page_revision_id, source_id) FROM stdin;
1	2021-10-20 12:07:38.234115+05:30	2	23	1
2	2021-10-20 12:26:12.490804+05:30	2	24	1
3	2021-10-20 12:28:14.547692+05:30	2	25	1
4	2021-10-20 12:29:02.312588+05:30	2	26	1
5	2021-10-20 12:30:29.795765+05:30	2	27	1
6	2021-10-20 12:35:49.416889+05:30	2	28	1
7	2021-10-20 12:36:10.044614+05:30	2	29	1
8	2021-10-20 12:36:55.906758+05:30	2	30	1
\.


--
-- Data for Name: wagtail_localize_translationsource; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtail_localize_translationsource (id, object_repr, content_json, created_at, locale_id, object_id, specific_content_type_id, last_updated_at, schema_version) FROM stdin;
1	Home	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:29:46.414Z", "latest_revision_created_at": "2021-10-20T06:29:46.324Z", "live_revision": 21, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	2021-10-20 12:07:36.611632+05:30	1	0715f2f3-4350-4922-9a2d-1376f7d73632	2	2021-10-20 12:36:54.804695+05:30	0003_auto_20211020_1105
\.


--
-- Data for Name: wagtailadmin_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailadmin_admin (id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_collection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collection (id, path, depth, numchild, name) FROM stdin;
1	0001	1	0	Root
\.


--
-- Data for Name: wagtailcore_collectionviewrestriction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collectionviewrestriction (id, restriction_type, password, collection_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_collectionviewrestriction_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collectionviewrestriction_groups (id, collectionviewrestriction_id, group_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_comment (id, text, contentpath, "position", created_at, updated_at, resolved_at, page_id, resolved_by_id, revision_created_id, user_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_commentreply; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_commentreply (id, text, created_at, updated_at, comment_id, user_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_groupapprovaltask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupapprovaltask (task_ptr_id) FROM stdin;
1
\.


--
-- Data for Name: wagtailcore_groupapprovaltask_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupapprovaltask_groups (id, groupapprovaltask_id, group_id) FROM stdin;
1	1	1
\.


--
-- Data for Name: wagtailcore_groupcollectionpermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupcollectionpermission (id, collection_id, group_id, permission_id) FROM stdin;
1	1	1	2
2	1	2	2
3	1	1	3
4	1	2	3
5	1	1	5
6	1	2	5
7	1	1	6
8	1	2	6
9	1	1	7
10	1	2	7
11	1	1	9
12	1	2	9
\.


--
-- Data for Name: wagtailcore_grouppagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_grouppagepermission (id, permission_type, group_id, page_id) FROM stdin;
1	add	1	1
2	edit	1	1
3	publish	1	1
4	add	2	1
5	edit	2	1
6	lock	1	1
7	unlock	1	1
\.


--
-- Data for Name: wagtailcore_locale; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_locale (id, language_code) FROM stdin;
1	en
2	th
\.


--
-- Data for Name: wagtailcore_page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_page (id, path, depth, numchild, title, slug, live, has_unpublished_changes, url_path, seo_title, show_in_menus, search_description, go_live_at, expire_at, expired, content_type_id, owner_id, locked, latest_revision_created_at, first_published_at, live_revision_id, last_published_at, draft_title, locked_at, locked_by_id, translation_key, locale_id, alias_of_id) FROM stdin;
1	0001	1	2	Root	root	t	f	/		f		\N	\N	f	1	\N	f	\N	\N	\N	\N	Root	\N	\N	e5057d67-3799-45ff-a87d-a29c673f287b	1	\N
4	00010002	2	0	บ้าน	บาน	t	f	/บาน/		f		\N	\N	f	2	\N	f	2021-10-20 12:36:55.804032+05:30	2021-10-20 12:07:38.167299+05:30	30	2021-10-20 12:36:55.840934+05:30	บ้าน	\N	\N	0715f2f3-4350-4922-9a2d-1376f7d73632	2	\N
3	00010001	2	0	Home	home	t	f	/home/		f		\N	\N	f	2	\N	f	2021-10-20 11:59:46.324036+05:30	2021-10-20 11:24:53.822654+05:30	21	2021-10-20 11:59:46.414729+05:30	Home	\N	\N	0715f2f3-4350-4922-9a2d-1376f7d73632	1	\N
\.


--
-- Data for Name: wagtailcore_pagelogentry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagelogentry (id, label, action, data_json, "timestamp", content_changed, deleted, content_type_id, page_id, revision_id, user_id) FROM stdin;
1	Home	wagtail.create	""	2021-10-20 10:56:40.033685+05:30	t	f	2	4	\N	\N
2	Home	wagtail.create_alias	{"page": {"id": 4, "title": "Home"}, "source": {"id": 1, "title": "Root"}, "destination": {"id": 1, "title": "Root"}}	2021-10-20 10:56:40.047735+05:30	f	f	2	4	\N	\N
3	Home	wagtail.publish	""	2021-10-20 10:56:40.052869+05:30	f	f	2	4	\N	\N
4	Home	wagtail.edit	""	2021-10-20 11:24:53.798056+05:30	t	f	2	3	1	1
5	Home	wagtail.publish	""	2021-10-20 11:24:53.903091+05:30	f	f	2	4	\N	1
6	Home	wagtail.publish	null	2021-10-20 11:24:53.915459+05:30	t	f	2	3	1	1
7	Home	wagtail.edit	""	2021-10-20 11:25:45.760733+05:30	t	f	2	3	2	1
8	Home	wagtail.publish	""	2021-10-20 11:25:45.859917+05:30	f	f	2	4	\N	1
9	Home	wagtail.publish	null	2021-10-20 11:25:45.868719+05:30	t	f	2	3	2	1
10	Home	wagtail.edit	""	2021-10-20 11:26:09.909773+05:30	t	f	2	3	3	1
11	Home	wagtail.publish	""	2021-10-20 11:26:10.02945+05:30	f	f	2	4	\N	1
12	Home	wagtail.publish	null	2021-10-20 11:26:10.037729+05:30	t	f	2	3	3	1
13	Home	wagtail.edit	""	2021-10-20 11:26:51.995282+05:30	t	f	2	3	4	1
14	Home	wagtail.publish	""	2021-10-20 11:26:52.167862+05:30	f	f	2	4	\N	1
15	Home	wagtail.publish	null	2021-10-20 11:26:52.183285+05:30	t	f	2	3	4	1
16	Home	wagtail.edit	""	2021-10-20 11:27:31.876301+05:30	t	f	2	3	5	1
17	Home	wagtail.publish	""	2021-10-20 11:27:32.011725+05:30	f	f	2	4	\N	1
18	Home	wagtail.publish	null	2021-10-20 11:27:32.023093+05:30	t	f	2	3	5	1
19	Home	wagtail.edit	""	2021-10-20 11:28:32.941127+05:30	t	f	2	3	6	1
20	Home	wagtail.publish	""	2021-10-20 11:28:33.060802+05:30	f	f	2	4	\N	1
21	Home	wagtail.publish	null	2021-10-20 11:28:33.067829+05:30	t	f	2	3	6	1
22	Home	wagtail.edit	""	2021-10-20 11:30:48.059625+05:30	t	f	2	3	7	1
23	Home	wagtail.publish	""	2021-10-20 11:30:48.189613+05:30	f	f	2	4	\N	1
24	Home	wagtail.publish	null	2021-10-20 11:30:48.202087+05:30	t	f	2	3	7	1
25	Home	wagtail.edit	""	2021-10-20 11:32:08.070645+05:30	t	f	2	3	8	1
26	Home	wagtail.publish	""	2021-10-20 11:32:08.225518+05:30	f	f	2	4	\N	1
27	Home	wagtail.publish	null	2021-10-20 11:32:08.236025+05:30	t	f	2	3	8	1
28	Home	wagtail.edit	""	2021-10-20 11:35:27.166391+05:30	t	f	2	3	9	1
29	Home	wagtail.publish	""	2021-10-20 11:35:27.348072+05:30	f	f	2	4	\N	1
30	Home	wagtail.publish	null	2021-10-20 11:35:27.36599+05:30	t	f	2	3	9	1
31	Home	wagtail.edit	""	2021-10-20 11:36:59.218206+05:30	t	f	2	3	10	1
32	Home	wagtail.publish	""	2021-10-20 11:36:59.390818+05:30	f	f	2	4	\N	1
33	Home	wagtail.publish	null	2021-10-20 11:36:59.408071+05:30	t	f	2	3	10	1
34	Home	wagtail.edit	""	2021-10-20 11:37:37.971569+05:30	t	f	2	3	11	1
35	Home	wagtail.publish	""	2021-10-20 11:37:38.117735+05:30	f	f	2	4	\N	1
36	Home	wagtail.publish	null	2021-10-20 11:37:38.129089+05:30	t	f	2	3	11	1
37	Home	wagtail.edit	""	2021-10-20 11:38:18.768741+05:30	t	f	2	3	12	1
38	Home	wagtail.publish	""	2021-10-20 11:38:18.87221+05:30	f	f	2	4	\N	1
39	Home	wagtail.publish	null	2021-10-20 11:38:18.879161+05:30	t	f	2	3	12	1
40	Home	wagtail.edit	""	2021-10-20 11:40:31.820542+05:30	t	f	2	3	13	1
41	Home	wagtail.publish	""	2021-10-20 11:40:31.993226+05:30	f	f	2	4	\N	1
42	Home	wagtail.publish	null	2021-10-20 11:40:32.013825+05:30	t	f	2	3	13	1
43	Home	wagtail.edit	""	2021-10-20 11:40:59.304768+05:30	t	f	2	3	14	1
44	Home	wagtail.publish	""	2021-10-20 11:40:59.453569+05:30	f	f	2	4	\N	1
45	Home	wagtail.publish	null	2021-10-20 11:40:59.46694+05:30	t	f	2	3	14	1
46	Home	wagtail.edit	""	2021-10-20 11:41:53.300942+05:30	t	f	2	3	15	1
47	Home	wagtail.publish	""	2021-10-20 11:41:53.432394+05:30	f	f	2	4	\N	1
48	Home	wagtail.publish	null	2021-10-20 11:41:53.445265+05:30	t	f	2	3	15	1
49	Home	wagtail.edit	""	2021-10-20 11:45:15.560541+05:30	t	f	2	3	16	1
50	Home	wagtail.publish	""	2021-10-20 11:45:15.704508+05:30	f	f	2	4	\N	1
51	Home	wagtail.publish	null	2021-10-20 11:45:15.715611+05:30	t	f	2	3	16	1
52	Home	wagtail.edit	""	2021-10-20 11:46:09.877191+05:30	t	f	2	3	17	1
53	Home	wagtail.publish	""	2021-10-20 11:46:10.006595+05:30	f	f	2	4	\N	1
54	Home	wagtail.publish	null	2021-10-20 11:46:10.021427+05:30	t	f	2	3	17	1
55	Home	wagtail.edit	""	2021-10-20 11:53:00.425663+05:30	t	f	2	3	18	1
56	Home	wagtail.publish	""	2021-10-20 11:53:00.559756+05:30	f	f	2	4	\N	1
57	Home	wagtail.publish	null	2021-10-20 11:53:00.571522+05:30	t	f	2	3	18	1
58	Home	wagtail.edit	""	2021-10-20 11:56:23.475761+05:30	t	f	2	3	19	1
59	Home	wagtail.publish	""	2021-10-20 11:56:23.652026+05:30	f	f	2	4	\N	1
60	Home	wagtail.publish	null	2021-10-20 11:56:23.668933+05:30	t	f	2	3	19	1
61	Home	wagtail.edit	""	2021-10-20 11:57:18.269049+05:30	t	f	2	3	20	1
62	Home	wagtail.publish	""	2021-10-20 11:57:18.430629+05:30	f	f	2	4	\N	1
63	Home	wagtail.publish	null	2021-10-20 11:57:18.44673+05:30	t	f	2	3	20	1
64	Home	wagtail.edit	""	2021-10-20 11:59:46.349616+05:30	t	f	2	3	21	1
65	Home	wagtail.publish	""	2021-10-20 11:59:46.503451+05:30	f	f	2	4	\N	1
66	Home	wagtail.publish	null	2021-10-20 11:59:46.51268+05:30	t	f	2	3	21	1
67	Home	wagtail.convert_alias	{"page": {"id": 4, "title": "Home"}}	2021-10-20 12:07:38.074795+05:30	f	f	2	4	22	1
68	Home	wagtail.publish	null	2021-10-20 12:07:38.224853+05:30	t	f	2	4	23	\N
69	Home	wagtail.publish	null	2021-10-20 12:26:12.479436+05:30	t	f	2	4	24	\N
70	Home	wagtail.publish	null	2021-10-20 12:28:14.535261+05:30	t	f	2	4	25	\N
71	บ้าน	wagtail.publish	null	2021-10-20 12:29:02.299824+05:30	t	f	2	4	26	\N
72	บ้าน	wagtail.publish	null	2021-10-20 12:30:29.780047+05:30	t	f	2	4	27	\N
73	บ้าน	wagtail.publish	null	2021-10-20 12:35:49.407911+05:30	t	f	2	4	28	\N
74	บ้าน	wagtail.publish	null	2021-10-20 12:36:10.026664+05:30	t	f	2	4	29	\N
75	บ้าน	wagtail.publish	null	2021-10-20 12:36:55.899776+05:30	t	f	2	4	30	\N
\.


--
-- Data for Name: wagtailcore_pagerevision; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagerevision (id, submitted_for_moderation, created_at, content_json, approved_go_live_at, page_id, user_id) FROM stdin;
30	f	2021-10-20 12:36:55.804032+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "draft_title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "slug": "\\u0e1a\\u0e32\\u0e19", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/\\u0e1a\\u0e32\\u0e19/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T07:06:09.961Z", "latest_revision_created_at": "2021-10-20T07:06:09.912Z", "live_revision": 29, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
22	f	2021-10-20 12:07:38.059319+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home-th/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": "2021-10-20T06:27:18.307Z", "latest_revision_created_at": "2021-10-20T06:29:46.324Z", "live_revision": 20, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	4	1
23	f	2021-10-20 12:07:38.132189+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "Home", "draft_title": "Home", "slug": "home-1", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home-1/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": "2021-10-20T06:27:18.307Z", "latest_revision_created_at": "2021-10-20T06:37:38.059Z", "live_revision": 20, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	4	1
19	f	2021-10-20 11:56:23.443769+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:23:00.458Z", "latest_revision_created_at": "2021-10-20T06:23:00.393Z", "live_revision": 18, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
16	f	2021-10-20 11:45:15.534444+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:11:53.326Z", "latest_revision_created_at": "2021-10-20T06:11:53.276Z", "live_revision": 15, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\"><b>Contact Us for a Quote</b></h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><ul><li data-block-key=\\\\\\"2vivo\\\\\\">Facility Management on Shared EC2 Instance</li><li data-block-key=\\\\\\"i4o2\\\\\\">Shared EC2 Cluster Deployment</li><li data-block-key=\\\\\\"brlgb\\\\\\">48 hrs response time</li><li data-block-key=\\\\\\"fjts0\\\\\\">Shared account management</li></ul>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"> </p><p data-block-key=\\\\\\"6rp38\\\\\\"><b>TESTIMONIALS</b></p><h2 data-block-key=\\\\\\"4lbnj\\\\\\"><b>Our Customers Reviews</b></h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"> </p><p data-block-key=\\\\\\"c4ns0\\\\\\"><b>LATEST INFO</b></p><h2 data-block-key=\\\\\\"at6tj\\\\\\"><b>Our Latest News</b></h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
12	f	2021-10-20 11:38:18.747114+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:07:37.996Z", "latest_revision_created_at": "2021-10-20T06:07:37.942Z", "live_revision": 11, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"> </p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
1	f	2021-10-20 11:24:53.771768+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"s3h96\\\\\\"> COVID Isolation &amp;amp; Control </p>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\"> Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease </p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"0ua3x\\\\\\"> Discover Facility Manager Core </p>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\"> Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance </p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"l2zlg\\\\\\"> CRM specific to Facility Management </p>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\"> Tailor Made CRM for Facility Management. </p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kfzag\\\\\\"> Asset Management &amp;amp; Maintenance </p>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\"> Maintenance is the key pillar on maintaining your Facility&amp;#x27;s quality and usability. </p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
2	f	2021-10-20 11:25:45.734986+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:54:53.822Z", "latest_revision_created_at": "2021-10-20T05:54:53.771Z", "live_revision": 1, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h3 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h3>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</p>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</p>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp;amp; Maintenance</p>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility&amp;#x27;s quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
24	f	2021-10-20 12:26:12.388224+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "Home", "draft_title": "Home", "slug": "home-1", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home-1/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T06:37:38.167Z", "latest_revision_created_at": "2021-10-20T06:37:38.132Z", "live_revision": 23, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
17	f	2021-10-20 11:46:09.852415+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:15:15.595Z", "latest_revision_created_at": "2021-10-20T06:15:15.534Z", "live_revision": 16, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\"><b>Contact Us for a Quote</b></h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><ul><li data-block-key=\\\\\\"2vivo\\\\\\">Facility Management on Shared EC2 Instance</li><li data-block-key=\\\\\\"i4o2\\\\\\">Shared EC2 Cluster Deployment</li><li data-block-key=\\\\\\"brlgb\\\\\\">48 hrs response time</li><li data-block-key=\\\\\\"fjts0\\\\\\">Shared account management</li></ul>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
20	f	2021-10-20 11:57:18.246349+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:26:23.513Z", "latest_revision_created_at": "2021-10-20T06:26:23.443Z", "live_revision": 19, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
5	f	2021-10-20 11:27:31.8502+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:56:52.032Z", "latest_revision_created_at": "2021-10-20T05:56:51.959Z", "live_revision": 4, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility&amp;#x27;s quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
6	f	2021-10-20 11:28:32.917632+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:57:31.908Z", "latest_revision_created_at": "2021-10-20T05:57:31.850Z", "live_revision": 5, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
25	f	2021-10-20 12:28:14.454151+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "Home", "draft_title": "Home", "slug": "home-1", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home-1/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T06:56:12.425Z", "latest_revision_created_at": "2021-10-20T06:56:12.388Z", "live_revision": 24, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
26	f	2021-10-20 12:29:02.210752+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "draft_title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "slug": "\\u0e1a\\u0e32\\u0e19", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/\\u0e1a\\u0e32\\u0e19/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T06:58:14.486Z", "latest_revision_created_at": "2021-10-20T06:58:14.454Z", "live_revision": 25, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
4	f	2021-10-20 11:26:51.959069+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:56:09.939Z", "latest_revision_created_at": "2021-10-20T05:56:09.890Z", "live_revision": 3, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\"><b>COVID Isolation &amp; Control</b></h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</p>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp;amp; Maintenance</p>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility&amp;#x27;s quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
27	f	2021-10-20 12:30:29.687475+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "draft_title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "slug": "\\u0e1a\\u0e32\\u0e19", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/\\u0e1a\\u0e32\\u0e19/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T06:59:02.249Z", "latest_revision_created_at": "2021-10-20T06:59:02.210Z", "live_revision": 26, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
7	f	2021-10-20 11:30:48.029662+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:58:32.966Z", "latest_revision_created_at": "2021-10-20T05:58:32.917Z", "live_revision": 6, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
14	f	2021-10-20 11:40:59.269347+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:10:31.855Z", "latest_revision_created_at": "2021-10-20T06:10:31.791Z", "live_revision": 13, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\"><b>Contact Us for a Quote</b></h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><ul><li data-block-key=\\\\\\"7vdv\\\\\\">Facility Management on Shared EC2 Instance</li><li data-block-key=\\\\\\"4t70e\\\\\\">Shared EC2 Cluster Deployment</li><li data-block-key=\\\\\\"5k82\\\\\\">48 hrs response time</li><li data-block-key=\\\\\\"3br7e\\\\\\">Shared account management</li></ul>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
28	f	2021-10-20 12:35:49.314633+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "draft_title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "slug": "\\u0e1a\\u0e32\\u0e19", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/\\u0e1a\\u0e32\\u0e19/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T07:00:29.729Z", "latest_revision_created_at": "2021-10-20T07:00:29.687Z", "live_revision": 27, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
8	f	2021-10-20 11:32:08.032071+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:00:48.085Z", "latest_revision_created_at": "2021-10-20T06:00:48.029Z", "live_revision": 7, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"> </p><h2 data-block-key=\\\\\\"7156n\\\\\\"><b>A Comprehensive control Tower for your Facility</b></h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
15	f	2021-10-20 11:41:53.276149+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:10:59.338Z", "latest_revision_created_at": "2021-10-20T06:10:59.269Z", "live_revision": 14, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\"><b>Contact Us for a Quote</b></h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><ul><li data-block-key=\\\\\\"2vivo\\\\\\">Facility Management on Shared EC2 Instance</li><li data-block-key=\\\\\\"i4o2\\\\\\">Shared EC2 Cluster Deployment</li><li data-block-key=\\\\\\"brlgb\\\\\\">48 hrs response time</li><li data-block-key=\\\\\\"fjts0\\\\\\">Shared account management</li></ul>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
18	f	2021-10-20 11:53:00.393773+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:16:09.907Z", "latest_revision_created_at": "2021-10-20T06:16:09.852Z", "live_revision": 17, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\">  </p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\">  </p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\">  </p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\">  </p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\">  </p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\">  </p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\">  </p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\">  </p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\">  </p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
3	f	2021-10-20 11:26:09.890964+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T05:55:45.785Z", "latest_revision_created_at": "2021-10-20T05:55:45.734Z", "live_revision": 2, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\"><b>COVID Isolation &amp; Control</b></h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</p>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</p>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp;amp; Maintenance</p>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility&amp;#x27;s quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": null, "box_text1": null, "box_title2": null, "box_text2": null, "box_title3": null, "box_text3": null, "snippet_title": null, "snippet_title1": null, "snippet_content1": "[]", "snippet_title2": null, "snippet_content2": "[]", "snippet_title3": null, "snippet_content3": "[]", "snippet_title4": null, "snippet_content4": "[]", "snippet_title5": null, "snippet_content5": "[]", "manage_covid_subtitle": null, "manage_covid_content": "[]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
13	f	2021-10-20 11:40:31.791074+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:08:18.793Z", "latest_revision_created_at": "2021-10-20T06:08:18.747Z", "live_revision": 12, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"> </p><h2 data-block-key=\\\\\\"4drha\\\\\\"><b>Contact Us for a Quote</b></h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\">  </p><p data-block-key=\\\\\\"7vdv\\\\\\">Facility Management on Shared EC2 Instance</p><p data-block-key=\\\\\\"4t70e\\\\\\">Shared EC2 Cluster Deployment</p><p data-block-key=\\\\\\"5k82\\\\\\">48 hrs response time</p><p data-block-key=\\\\\\"3br7e\\\\\\">Shared account management</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
21	f	2021-10-20 11:59:46.324036+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:27:18.307Z", "latest_revision_created_at": "2021-10-20T06:27:18.246Z", "live_revision": 20, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">Comprehensive Reports for Governments</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">Contact Us for a Quote</h2><p data-block-key=\\\\\\"9mptk\\\\\\">Crafted with Future of Facility Management in mind.</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">TESTIMONIALS</p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">Our Customers Reviews</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "Total Projects", "project_unit": "Units", "tenants_managed": "Tenants Managed", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">LATEST INFO</p><h2 data-block-key=\\\\\\"at6tj\\\\\\">Our Latest News</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "VIEW ALL NEWS", "requestcontent": "Request a demo to see how Domitos can supercharge your Facility Team.", "requestbutton": "REQUEST A DEMO", "comments": []}	\N	3	1
29	f	2021-10-20 12:36:09.912969+05:30	{"pk": 4, "path": "00010002", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 2, "title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "draft_title": "\\u0e1a\\u0e49\\u0e32\\u0e19", "slug": "\\u0e1a\\u0e32\\u0e19", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/\\u0e1a\\u0e32\\u0e19/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T06:37:38.167Z", "last_published_at": "2021-10-20T07:05:49.348Z", "latest_revision_created_at": "2021-10-20T07:05:49.314Z", "live_revision": 28, "alias_of": null, "header_title": "#1 \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01 | \\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 | \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e42\\u0e04\\u0e27\\u0e34\\u0e14", "meta_content": "Domitos \\u0e43\\u0e2b\\u0e49\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32 &amp; \\u0e0b\\u0e2d\\u0e1f\\u0e15\\u0e4c\\u0e41\\u0e27\\u0e23\\u0e4c\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e15\\u0e23\\u0e27\\u0e08\\u0e2a\\u0e2d\\u0e1a\\u0e41\\u0e25\\u0e30\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e04\\u0e33\\u0e2a\\u0e31\\u0e48\\u0e07\\u0e07\\u0e32\\u0e19 \\u0e23\\u0e27\\u0e21\\u0e16\\u0e36\\u0e07\\u0e42\\u0e21\\u0e14\\u0e39\\u0e25\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23 covid-19 \\u0e41\\u0e1a\\u0e1a\\u0e01\\u0e33\\u0e2b\\u0e19\\u0e14\\u0e40\\u0e2d\\u0e07\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\\u0e02\\u0e2d\\u0e07\\u0e1e\\u0e19\\u0e31\\u0e01\\u0e07\\u0e32\\u0e19", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1e\\\\u0e1a\\\\u0e27\\\\u0e48\\\\u0e32 Domitos \\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e40\\\\u0e2b\\\\u0e25\\\\u0e37\\\\u0e2d\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30 NGOs \\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e22\\\\u0e01\\\\u0e40\\\\u0e0a\\\\u0e37\\\\u0e49\\\\u0e2d COVID \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e2b\\\\u0e32\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 </h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31 \\\\u0e0d\\\\u0e2b\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01 \\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e17\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e2a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e15\\\\u0e31 \\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e32\\\\u0e22\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e4d\\\\u0e32\\\\u0e32\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c \\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2d\\\\u0e2d\\\\u0e01\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e2b\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e2a\\\\u0e31\\\\u0e21\\\\u0e1e\\\\u0e31\\\\u0e19\\\\u0e18\\\\u0e4c\\\\u0e40\\\\u0e09\\\\u0e1e\\\\u0e32\\\\u0e30\\\\u0e01\\\\u0e25\\\\u0e38\\\\u0e48\\\\u0e21  \\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e2a\\\\u0e32\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e42\\\\u0e23\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "\\u0e14\\u0e39\\u0e04\\u0e38\\u0e13\\u0e2a\\u0e21\\u0e1a\\u0e31\\u0e15\\u0e34", "button2": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button3": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "button4": "\\u0e04\\u0e49\\u0e19\\u0e1e\\u0e1a\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e40\\u0e15\\u0e34\\u0e21", "box_title1": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "box_text1": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e1c\\u0e48\\u0e32\\u0e19\\u0e0a\\u0e48\\u0e2d\\u0e07\\u0e17\\u0e32\\u0e07\\u0e40\\u0e14\\u0e35\\u0e22\\u0e27 \\u0e23\\u0e31\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e0a\\u0e33\\u0e23\\u0e30\\u0e40\\u0e07\\u0e34\\u0e19\\u0e14\\u0e49\\u0e27\\u0e22\\u0e23\\u0e30\\u0e1a\\u0e1a\\u0e2d\\u0e34\\u0e40\\u0e25\\u0e47\\u0e01\\u0e17\\u0e23\\u0e2d\\u0e19\\u0e34\\u0e01\\u0e2a\\u0e4c\\u0e42\\u0e14\\u0e22 Stripe", "box_title2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "box_text2": "\\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e1e\\u0e23\\u0e49\\u0e2d\\u0e21\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19 \\u0e01\\u0e32\\u0e23\\u0e40\\u0e23\\u0e34\\u0e48\\u0e21\\u0e15\\u0e49\\u0e19\\u0e43\\u0e0a\\u0e49\\u0e07\\u0e32\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32 & \\u0e01\\u0e32\\u0e23\\u0e08\\u0e31\\u0e14\\u0e2a\\u0e23\\u0e23\\u0e43\\u0e2b\\u0e21\\u0e48", "box_title3": "\\u0e2a\\u0e34\\u0e19\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e41\\u0e25\\u0e30\\u0e01\\u0e32\\u0e23\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07\\u0e23\\u0e31\\u0e01\\u0e29\\u0e32", "box_text3": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32\\u0e41\\u0e25\\u0e30\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23 \\u0e23\\u0e31\\u0e01\\u0e29\\u0e32\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e23\\u0e38\\u0e01 \\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1b\\u0e31\\u0e0d\\u0e2b\\u0e32", "snippet_title": "\\u0e14\\u0e36\\u0e07\\u0e14\\u0e39\\u0e14\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e25\\u0e07\\u0e15\\u0e31\\u0e27 \\u0e21\\u0e2d\\u0e1a\\u0e1b\\u0e23\\u0e30\\u0e2a\\u0e1a\\u0e01\\u0e32\\u0e23\\u0e13\\u0e4c\\u0e2a\\u0e38\\u0e14\\u0e1e\\u0e34\\u0e40\\u0e28\\u0e29", "snippet_title1": "\\u0e2a\\u0e34\\u0e48\\u0e07\\u0e2d\\u0e33\\u0e19\\u0e27\\u0e22\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e30\\u0e14\\u0e27\\u0e01", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e38\\\\u0e21\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</h2><p data-block-key=\\\\\\"43hpa\\\\\\">\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e17\\\\u0e23\\\\u0e32\\\\u0e1a\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e20\\\\u0e32\\\\u0e22\\\\u0e43\\\\u0e15\\\\u0e49\\\\u0e1a\\\\u0e31\\\\u0e0d\\\\u0e0a\\\\u0e35\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e2d\\\\u0e31\\\\u0e15\\\\u0e23\\\\u0e32\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01 \\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e1b\\\\u0e31\\\\u0e08\\\\u0e08\\\\u0e38\\\\u0e1a\\\\u0e31\\\\u0e19 \\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e0b\\\\u0e37\\\\u0e49\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e02\\\\u0e27\\\\u0e32\\\\u0e07\\\\u0e2b\\\\u0e23\\\\u0e37\\\\u0e2d\\\\u0e15\\\\u0e32\\\\u0e21\\\\u0e2b\\\\u0e2d\\\\u0e04\\\\u0e2d\\\\u0e22 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e04\\\\u0e32\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e13\\\\u0e4c\\\\u0e41\\\\u0e19\\\\u0e27\\\\u0e42\\\\u0e19\\\\u0e49\\\\u0e21\\\\u0e43\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e14\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e02\\\\u0e49\\\\u0e32\\\\u0e1e\\\\u0e31\\\\u0e01</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "\\u0e17\\u0e23\\u0e31\\u0e1e\\u0e22\\u0e4c\\u0e2a\\u0e34\\u0e19", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e48\\\\u0e32\\\\u0e22\\\\u0e14\\\\u0e32\\\\u0e22 - \\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e40\\\\u0e08\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e0a\\\\u0e48\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e32\\\\u0e21 \\\\u0e40\\\\u0e04\\\\u0e23\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e08\\\\u0e31\\\\u0e01\\\\u0e23 HVAC \\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e44\\\\u0e1f\\\\u0e1f\\\\u0e49\\\\u0e32 \\\\u0e23\\\\u0e27\\\\u0e21\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e19\\\\u0e33\\\\u0e21\\\\u0e32\\\\u0e22\\\\u0e31\\\\u0e07\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e14\\\\u0e39\\\\u0e04\\\\u0e48\\\\u0e32\\\\u0e40\\\\u0e2a\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e21\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e27\\\\u0e31\\\\u0e14\\\\u0e2d\\\\u0e32\\\\u0e22\\\\u0e38\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32\\u0e41\\u0e25\\u0e30\\u0e25\\u0e39\\u0e01\\u0e04\\u0e49\\u0e32", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>\\\\u0e0a\\\\u0e48\\\\u0e2d\\\\u0e07\\\\u0e17\\\\u0e32\\\\u0e07 \\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e41\\\\u0e2d\\\\u0e1e\\\\u0e21\\\\u0e37\\\\u0e2d\\\\u0e16\\\\u0e37\\\\u0e2d\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e04\\\\u0e23\\\\u0e1a\\\\u0e27\\\\u0e07\\\\u0e08\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32 \\\\u0e15\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e41\\\\u0e15\\\\u0e48\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e44\\\\u0e1b\\\\u0e08\\\\u0e19\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e01\\\\u0e32\\\\u0e28\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e0a\\\\u0e33\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e07\\\\u0e34\\\\u0e19 \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e1b\\\\u0e23\\\\u0e30\\\\u0e40\\\\u0e14\\\\u0e47\\\\u0e19\\\\u0e2a\\\\u0e33\\\\u0e04\\\\u0e31\\\\u0e0d\\\\u0e17\\\\u0e31\\\\u0e49\\\\u0e07\\\\u0e2b\\\\u0e21\\\\u0e14\\\\u0e43\\\\u0e19\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e2b\\\\u0e25\\\\u0e31\\\\u0e01\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "\\u0e01\\u0e32\\u0e23\\u0e0b\\u0e48\\u0e2d\\u0e21\\u0e1a\\u0e33\\u0e23\\u0e38\\u0e07", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>\\\\u0e14\\\\u0e33\\\\u0e40\\\\u0e19\\\\u0e34\\\\u0e19\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e14\\\\u0e39\\\\u0e41\\\\u0e25\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2a\\\\u0e21\\\\u0e40\\\\u0e1b\\\\u0e47\\\\u0e19\\\\u0e23\\\\u0e30\\\\u0e22\\\\u0e30 </b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">\\\\u0e04\\\\u0e27\\\\u0e1a\\\\u0e04\\\\u0e39\\\\u0e48\\\\u0e44\\\\u0e1b\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c \\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e19\\\\u0e35\\\\u0e49\\\\u0e40\\\\u0e2b\\\\u0e21\\\\u0e32\\\\u0e30\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e22\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1a\\\\u0e33\\\\u0e23\\\\u0e38\\\\u0e07\\\\u0e23\\\\u0e31\\\\u0e01\\\\u0e29\\\\u0e32\\\\u0e17\\\\u0e23\\\\u0e31\\\\u0e1e\\\\u0e22\\\\u0e4c\\\\u0e2a\\\\u0e34\\\\u0e19\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2a\\\\u0e16\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13 \\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e2a\\\\u0e32\\\\u0e21\\\\u0e32\\\\u0e23\\\\u0e16\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e19 Cleaning</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "\\u0e02\\u0e49\\u0e2d\\u0e21\\u0e39\\u0e25\\u0e40\\u0e0a\\u0e34\\u0e07\\u0e25\\u0e36\\u0e01", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>\\\\u0e20\\\\u0e32\\\\u0e1e\\\\u0e23\\\\u0e27\\\\u0e21 , \\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e01\\\\u0e27\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e21\\\\u0e38\\\\u0e21\\\\u0e21\\\\u0e2d\\\\u0e07\\\\u0e41\\\\u0e1a\\\\u0e1a\\\\u0e40\\\\u0e08\\\\u0e32\\\\u0e30\\\\u0e25\\\\u0e36\\\\u0e01</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e2d\\\\u0e22\\\\u0e48\\\\u0e32\\\\u0e07\\\\u0e21\\\\u0e35\\\\u0e2d\\\\u0e22\\\\u0e39\\\\u0e48\\\\u0e43\\\\u0e19 BI \\\\u0e23\\\\u0e39\\\\u0e49\\\\u0e17\\\\u0e38\\\\u0e01\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e08\\\\u0e31\\\\u0e07\\\\u0e2b\\\\u0e27\\\\u0e30\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e18\\\\u0e38\\\\u0e23\\\\u0e01\\\\u0e34\\\\u0e08\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e38\\\\u0e13\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 ELK Stack \\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e02\\\\u0e31\\\\u0e1a\\\\u0e40\\\\u0e04\\\\u0e25\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e19\\\\u0e14\\\\u0e49\\\\u0e27\\\\u0e22 Query Search Tool \\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e37\\\\u0e1a\\\\u0e04\\\\u0e49\\\\u0e19\\\\u0e1a\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e36\\\\u0e01\\\\u0e40\\\\u0e1e\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e40\\\\u0e15\\\\u0e34\\\\u0e21</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e23\\u0e31\\u0e10\\u0e1a\\u0e32\\u0e25\\u0e41\\u0e25\\u0e30\\u0e2d\\u0e07\\u0e04\\u0e4c\\u0e01\\u0e23\\u0e1e\\u0e31\\u0e12\\u0e19\\u0e32\\u0e40\\u0e2d\\u0e01\\u0e0a\\u0e19", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</h2><p data-block-key=\\\\\\"eediu\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e41\\\\u0e01\\\\u0e49\\\\u0e1b\\\\u0e31\\\\u0e0d\\\\u0e2b\\\\u0e32 \\\\u0e44\\\\u0e14\\\\u0e49\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e23\\\\u0e48\\\\u0e27\\\\u0e21\\\\u0e01\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e2a\\\\u0e34\\\\u0e07\\\\u0e04\\\\u0e42\\\\u0e1b\\\\u0e23\\\\u0e4c\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e1e\\\\u0e23\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e43\\\\u0e2b\\\\u0e49\\\\u0e1a\\\\u0e23\\\\u0e34\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e2d\\\\u0e07\\\\u0e04\\\\u0e4c\\\\u0e01\\\\u0e23\\\\u0e1e\\\\u0e31\\\\u0e12\\\\u0e19\\\\u0e32\\\\u0e40\\\\u0e2d\\\\u0e01\\\\u0e0a\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e48\\\\u0e27\\\\u0e42\\\\u0e25\\\\u0e01</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e40\\\\u0e2d\\\\u0e07\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e28\\\\u0e39\\\\u0e19\\\\u0e22\\\\u0e4c\\\\u0e01\\\\u0e31\\\\u0e01\\\\u0e01\\\\u0e31\\\\u0e19\\\\u0e42\\\\u0e04\\\\u0e27\\\\u0e34\\\\u0e14</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e1c\\\\u0e39\\\\u0e49\\\\u0e40\\\\u0e0a\\\\u0e48\\\\u0e32\\\\u0e41\\\\u0e25\\\\u0e30\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e25\\\\u0e30\\\\u0e40\\\\u0e2d\\\\u0e35\\\\u0e22\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e1e\\\\u0e27\\\\u0e01\\\\u0e40\\\\u0e02\\\\u0e32</li></ul><p data-block-key=\\\\\\"9vsqq\\\\\\"></p><ul><li data-block-key=\\\\\\"3k4cf\\\\\\">\\\\u0e23\\\\u0e32\\\\u0e22\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e48\\\\u0e04\\\\u0e23\\\\u0e2d\\\\u0e1a\\\\u0e04\\\\u0e25\\\\u0e38\\\\u0e21\\\\u0e2a\\\\u0e33\\\\u0e2b\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e31\\\\u0e10\\\\u0e1a\\\\u0e32\\\\u0e25</li></ul><p data-block-key=\\\\\\"1bo4i\\\\\\"></p><ul><li data-block-key=\\\\\\"ekc7c\\\\\\">\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e40\\\\u0e23\\\\u0e34\\\\u0e48\\\\u0e21\\\\u0e15\\\\u0e49\\\\u0e19\\\\u0e43\\\\u0e0a\\\\u0e49\\\\u0e07\\\\u0e32\\\\u0e19\\\\u0e17\\\\u0e31\\\\u0e19\\\\u0e17\\\\u0e35\\\\u0e1a\\\\u0e19 AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9xoig\\\\\\"></p><h2 data-block-key=\\\\\\"4drha\\\\\\">\\\\u0e15\\\\u0e34\\\\u0e14\\\\u0e15\\\\u0e48\\\\u0e2d\\\\u0e40\\\\u0e23\\\\u0e32\\\\u0e40\\\\u0e1e\\\\u0e37\\\\u0e48\\\\u0e2d\\\\u0e02\\\\u0e2d\\\\u0e43\\\\u0e1a\\\\u0e40\\\\u0e2a\\\\u0e19\\\\u0e2d\\\\u0e23\\\\u0e32\\\\u0e04\\\\u0e32</h2><p data-block-key=\\\\\\"9mptk\\\\\\">\\\\u0e2a\\\\u0e23\\\\u0e49\\\\u0e32\\\\u0e07\\\\u0e02\\\\u0e36\\\\u0e49\\\\u0e19\\\\u0e42\\\\u0e14\\\\u0e22\\\\u0e04\\\\u0e33\\\\u0e19\\\\u0e36\\\\u0e07\\\\u0e16\\\\u0e36\\\\u0e07\\\\u0e2d\\\\u0e19\\\\u0e32\\\\u0e04\\\\u0e15\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e08\\\\u0e31\\\\u0e14\\\\u0e01\\\\u0e32\\\\u0e23\\\\u0e2a\\\\u0e34\\\\u0e48\\\\u0e07\\\\u0e2d\\\\u0e33\\\\u0e19\\\\u0e27\\\\u0e22\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e2a\\\\u0e30\\\\u0e14\\\\u0e27\\\\u0e01</p>\\", \\"id\\": \\"307f13cc-eaba-48c7-8470-35a6188d48ca\\"}]", "plan1": "ECONOMY", "plan1_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rybf4\\\\\\"></p><p data-block-key=\\\\\\"d0pa2\\\\\\">Facility Management on Shared EC2 Instance</p>\\", \\"id\\": \\"73225f3d-3f21-438b-8be8-751e85089de9\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"ejoa\\\\\\">Shared EC2 Cluster Deployment</p>\\", \\"id\\": \\"20cc318b-ae52-4570-b0b1-19c66e90e358\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"3uc8l\\\\\\">48 hrs response time</p>\\", \\"id\\": \\"0ba138eb-0f8a-478d-9b2e-b2cc43329d6c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"znzpz\\\\\\"></p><p data-block-key=\\\\\\"7i19r\\\\\\">Shared account management</p>\\", \\"id\\": \\"1dd696b2-fc13-480e-8db3-08421bd2f8a4\\"}]", "plan2": "STANDARD", "plan2_point1": "Facility Management on Dedicated EC2 Instance", "plan2_point2": "Dedicated EC2 Cluster Deployment", "plan2_point3": "24 hrs response time", "plan2_point4": "Dedicated Account Manager", "plan2_point5": null, "plan3": "PREMIUM", "plan3_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"7v5na\\\\\\">Facility Management on Higher Grade EC2 Instance</p>\\", \\"id\\": \\"1ca23272-4759-467d-ad56-f889dd9e8481\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"39n9f\\\\\\">Dedicated EC2 Cluster Deployment</p>\\", \\"id\\": \\"47299893-5a35-483d-b320-10f61f490999\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"s2fj\\\\\\">3 hrs response time</p>\\", \\"id\\": \\"486621b0-aabd-4ca5-88ae-24469851ff6a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"ckaro\\\\\\">Dedicated Account Manager</p>\\", \\"id\\": \\"a59b703c-b7c0-4770-b019-f946529f293b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5m0r3\\\\\\"></p><p data-block-key=\\\\\\"9v11e\\\\\\">All the modules and future updates included</p>\\", \\"id\\": \\"e410a78f-fe33-444a-a891-8605c51dce01\\"}]", "plan_button": "Book Live Demo", "customer_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"y9ujp\\\\\\"></p><p data-block-key=\\\\\\"6rp38\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e04\\\\u0e27\\\\u0e32\\\\u0e21\\\\u0e23\\\\u0e31\\\\u0e1a\\\\u0e23\\\\u0e2d\\\\u0e07 </p><h2 data-block-key=\\\\\\"4lbnj\\\\\\">\\\\u0e23\\\\u0e35\\\\u0e27\\\\u0e34\\\\u0e27\\\\u0e25\\\\u0e39\\\\u0e01\\\\u0e04\\\\u0e49\\\\u0e32\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"9a1643ca-9c3e-478c-b22a-bfb7005544a9\\"}]", "testimonials": "[{\\"type\\": \\"cards\\", \\"value\\": {\\"title\\": \\"Testimonials\\", \\"cards\\": [{\\"title\\": \\"Alagu Shan\\", \\"text\\": \\"Crystal Clear Management\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best FMS Available!\\"}, {\\"title\\": \\"Vijesh Kamat\\", \\"text\\": \\"CEO, 3000 Apartment Chain, Bengaluru\\", \\"subtitle\\": \\"I strongly recommend Domitos for managing your real estate properties.\\"}, {\\"title\\": \\"Raman Palaniappan\\", \\"text\\": \\"MES Group\\", \\"subtitle\\": \\"Domitos helps MES to manage around 25,000 beds in our Singapore Dorm Facilities\\"}, {\\"title\\": \\"John Joy\\", \\"text\\": \\"CEO, Backup International\\", \\"subtitle\\": \\"I have partnered with Domitos to provide partnerships in Kenya. We have deployed Domitos in 25 prope\\"}, {\\"title\\": \\"Lim Jannat\\", \\"text\\": \\"UX/UI Designer\\", \\"subtitle\\": \\"You've saved our business! Thanks guys, keep up the good work! The best on the net!\\"}]}, \\"id\\": \\"b3509892-95b1-4d9e-8af0-69af423c6869\\"}]", "page_title_content": "[]", "total_project": "\\u0e42\\u0e04\\u0e23\\u0e07\\u0e01\\u0e32\\u0e23\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "project_unit": "\\u0e2b\\u0e19\\u0e48\\u0e27\\u0e22", "tenants_managed": "\\u0e08\\u0e31\\u0e14\\u0e01\\u0e32\\u0e23\\u0e1c\\u0e39\\u0e49\\u0e40\\u0e0a\\u0e48\\u0e32", "news_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"m6mif\\\\\\"></p><p data-block-key=\\\\\\"c4ns0\\\\\\">\\\\u0e02\\\\u0e49\\\\u0e2d\\\\u0e21\\\\u0e39\\\\u0e25\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14   </p><h2 data-block-key=\\\\\\"at6tj\\\\\\">\\\\u0e02\\\\u0e48\\\\u0e32\\\\u0e27\\\\u0e25\\\\u0e48\\\\u0e32\\\\u0e2a\\\\u0e38\\\\u0e14\\\\u0e02\\\\u0e2d\\\\u0e07\\\\u0e40\\\\u0e23\\\\u0e32</h2>\\", \\"id\\": \\"84399c1e-8505-4bbe-be8c-fe41ccd293e7\\"}]", "view_button": "\\u0e14\\u0e39\\u0e02\\u0e48\\u0e32\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14", "requestcontent": "\\u0e02\\u0e2d\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e32\\u0e18\\u0e34\\u0e15\\u0e40\\u0e1e\\u0e37\\u0e48\\u0e2d\\u0e14\\u0e39\\u0e27\\u0e48\\u0e32 Domitos \\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1e\\u0e25\\u0e31\\u0e07\\u0e43\\u0e2b\\u0e49\\u0e01\\u0e31\\u0e1a\\u0e17\\u0e35\\u0e21 Facility \\u0e02\\u0e2d\\u0e07\\u0e04\\u0e38\\u0e13\\u0e44\\u0e14\\u0e49\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e44\\u0e23", "requestbutton": "\\u0e02\\u0e2d\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07", "comments": []}	\N	4	1
9	f	2021-10-20 11:35:27.116803+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:02:08.107Z", "latest_revision_created_at": "2021-10-20T06:02:08.032Z", "live_revision": 8, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"> </p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"> </p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"> </p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"> </p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"> </p><h2 data-block-key=\\\\\\"np5p\\\\\\"><b>Manage COVID Facilities</b></h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"c1j2i\\\\\\">Built Custom for COVID Isolation Centers</li><li data-block-key=\\\\\\"c4f8f\\\\\\">Manage Tenants and their details</li><li data-block-key=\\\\\\"4qluo\\\\\\">Comprehensive Reports for Governments</li><li data-block-key=\\\\\\"qrg3\\\\\\">Immediate Onboarding on AWS Cloud</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
10	f	2021-10-20 11:36:59.170925+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:05:27.221Z", "latest_revision_created_at": "2021-10-20T06:05:27.116Z", "live_revision": 9, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h2 data-block-key=\\\\\\"np5p\\\\\\"><b>Manage COVID Facilities</b></h2><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
11	f	2021-10-20 11:37:37.942475+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "0715f2f3-4350-4922-9a2d-1376f7d73632", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-10-20T05:54:53.822Z", "last_published_at": "2021-10-20T06:06:59.247Z", "latest_revision_created_at": "2021-10-20T06:06:59.170Z", "live_revision": 10, "alias_of": null, "header_title": "#1 Facility Management Software | Maintenance | Covid Management", "meta_content": "Domitos provide maintenance &amp; facility management software to monitor and assign work orders, also custom covid-19 management module for workforce accommodation.", "covid_title1": "Try 15 days free trial", "covid_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"s3h96\\\\\\">COVID Isolation &amp; Control</h2>\\", \\"id\\": \\"00ed78fc-4a89-46cc-9752-2a817d3390c5\\"}]", "covid_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fuftu\\\\\\">Discover how Domitos can help Government and NGOs to manage COVID Isolation Facilities Ease</p>\\", \\"id\\": \\"62a55662-0ef3-45c1-bcc9-7afac1b36fde\\"}]", "facility_title1": "Dont worry,we have covered End to End", "facility_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"0ua3x\\\\\\">Discover Facility Manager Core</h2>\\", \\"id\\": \\"d01ee00d-3ca2-4bed-948a-9e7daa08e504\\"}]", "facility_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"52958\\\\\\">Core Solution suites all the major requirements for your Facility Business, right from Sales to Maintenance</p>\\", \\"id\\": \\"55869e56-326c-4d33-bf60-547ef4852647\\"}]", "crm_title1": "Strong Sales Management", "crm_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"l2zlg\\\\\\">CRM specific to Facility Management</h2>\\", \\"id\\": \\"34abb62a-afdd-470a-85e9-1656f878be54\\"}]", "crm_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hwczy\\\\\\">Tailor Made CRM for Facility Management.</p>\\", \\"id\\": \\"06253dd1-cdea-4a5a-9364-7231d9086ed7\\"}]", "assests_title1": "Asset Management &amp; Maintenance", "assests_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<h2 data-block-key=\\\\\\"kfzag\\\\\\">Asset Management &amp; Maintenance</h2>\\", \\"id\\": \\"8470a818-9219-489a-b823-4530f5aa6ee7\\"}]", "assests_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1mqod\\\\\\">Maintenance is the key pillar on maintaining your Facility quality and usability.</p>\\", \\"id\\": \\"72f87fd1-2677-42f6-86da-e9aa59b6c94b\\"}]", "button1": "See Features", "button2": "Discover More", "button3": "Discover More", "button4": "Discover More", "box_title1": "Tenant & Client Management", "box_text1": "Manage Clients & Tenants through Single Portal.Get Payment Electronically powered by Stripe", "box_title2": "Facility Management", "box_text2": "Facility & Property Management, Availability, Client Onboarding & Reallocation.", "box_title3": "Assets & Maintenance", "box_text3": "Manage Client Assets as well as Organisational Assets. Maintain Proactively, manage Issues", "snippet_title": "Seamlessly engage tenants Deliver exceptional experiences", "snippet_title1": "Facility", "snippet_content1": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mj8vl\\\\\\"></p><h2 data-block-key=\\\\\\"7156n\\\\\\">A Comprehensive control Tower for your Facility</h2><p data-block-key=\\\\\\"43hpa\\\\\\">With Facility Overview, you can know the Facilities under your account, view the occupancy rate, present clients, blockwise or tower wise procurement availability and predict trends in occupancy level</p>\\", \\"id\\": \\"bdf6ae50-56bd-4847-a438-018644493779\\"}]", "snippet_title2": "Assets", "snippet_content2": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"3e6tw\\\\\\"></p><h2 data-block-key=\\\\\\"2mrg3\\\\\\"><b>Manage Assets with Ease - Self Owned as well as Client Assets</b></h2><p data-block-key=\\\\\\"8b7f5\\\\\\">Asset Management helps you in tracking, machinaries, HVAC, Electrical Items as well as the Assets brought to your premise by the clients. View the depreciation of Assets and guage Asset Lifecycle as well</p>\\", \\"id\\": \\"bc9c9b17-1c40-4b74-8efe-034176f41587\\"}]", "snippet_title3": "Tenants&Clients", "snippet_content3": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"aktp4\\\\\\"></p><h2 data-block-key=\\\\\\"e3la1\\\\\\"><b>Portals and Mobile apps for Clients as well as Tenants</b></h2><p data-block-key=\\\\\\"9bmnm\\\\\\">One stop solution for all Client and Tenant Needs. Right from Tenant Records to Payment Notices &amp; Issues, Client Management covers all Major Aspects to run your core business.</p>\\", \\"id\\": \\"8e558d7d-d11a-4ab9-a804-f74289653a35\\"}]", "snippet_title4": "Maintenance", "snippet_content4": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"axgmb\\\\\\"></p><h2 data-block-key=\\\\\\"e0s7\\\\\\"><b>Run your business as with proper periodic care</b></h2><p data-block-key=\\\\\\"2b8a3\\\\\\">Coupled with Asset Management, the solution is well suited for maintenance of your assets and premises. You can create tasks like Cleaning,</p>\\", \\"id\\": \\"5f2c115d-3399-4ae2-8416-ee8b183787c3\\"}]", "snippet_title5": "Insights", "snippet_content5": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o0zk8\\\\\\"></p><h2 data-block-key=\\\\\\"fqcgj\\\\\\"><b>Overview , Birds Eye View and Drilled Down View</b></h2><p data-block-key=\\\\\\"16pv9\\\\\\">Everything is available on BI. Know every beats and pulses of your business with the ELK Stack powered with our own Query Search Tool for extended record queries</p>\\", \\"id\\": \\"1c57afa3-dd46-4a21-baa7-8cb2b3dce638\\"}]", "manage_covid_subtitle": "For Governments & NGOs", "manage_covid_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"of16i\\\\\\"></p><h3 data-block-key=\\\\\\"np5p\\\\\\">Manage COVID Facilities</h3><p data-block-key=\\\\\\"eediu\\\\\\">Solution was co-developed with Singapore Government and is available for Governments &amp; NGOs across the world</p><ul><li data-block-key=\\\\\\"7i7la\\\\\\">Built Custom for COVID Isolation Centers</li></ul><p data-block-key=\\\\\\"flmnh\\\\\\"></p><ul><li data-block-key=\\\\\\"4g6pe\\\\\\">Manage Tenants and their details</li></ul>\\", \\"id\\": \\"3cbdb5c7-fb5b-4f38-aea5-242f742b6be4\\"}]", "quote_content": "[]", "plan1": null, "plan1_content": "[]", "plan2": null, "plan2_point1": null, "plan2_point2": null, "plan2_point3": null, "plan2_point4": null, "plan2_point5": null, "plan3": null, "plan3_content": "[]", "plan_button": null, "customer_content": "[]", "testimonials": "[]", "page_title_content": "[]", "total_project": null, "project_unit": null, "tenants_managed": null, "news_content": "[]", "view_button": null, "requestcontent": null, "requestbutton": null, "comments": []}	\N	3	1
\.


--
-- Data for Name: wagtailcore_pagesubscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagesubscription (id, comment_notifications, page_id, user_id) FROM stdin;
1	f	3	1
\.


--
-- Data for Name: wagtailcore_pageviewrestriction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pageviewrestriction (id, password, page_id, restriction_type) FROM stdin;
\.


--
-- Data for Name: wagtailcore_pageviewrestriction_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pageviewrestriction_groups (id, pageviewrestriction_id, group_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_site (id, hostname, port, is_default_site, root_page_id, site_name) FROM stdin;
2	localhost	80	t	3	
\.


--
-- Data for Name: wagtailcore_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_task (id, name, active, content_type_id) FROM stdin;
1	Moderators approval	t	4
\.


--
-- Data for Name: wagtailcore_taskstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_taskstate (id, status, started_at, finished_at, content_type_id, page_revision_id, task_id, workflow_state_id, finished_by_id, comment) FROM stdin;
\.


--
-- Data for Name: wagtailcore_workflow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflow (id, name, active) FROM stdin;
1	Moderators approval	t
\.


--
-- Data for Name: wagtailcore_workflowpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowpage (page_id, workflow_id) FROM stdin;
1	1
\.


--
-- Data for Name: wagtailcore_workflowstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowstate (id, status, created_at, current_task_state_id, page_id, requested_by_id, workflow_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_workflowtask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowtask (id, sort_order, task_id, workflow_id) FROM stdin;
1	0	1	1
\.


--
-- Data for Name: wagtaildocs_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtaildocs_document (id, title, file, created_at, uploaded_by_user_id, collection_id, file_size, file_hash) FROM stdin;
\.


--
-- Data for Name: wagtaildocs_uploadeddocument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtaildocs_uploadeddocument (id, file, uploaded_by_user_id) FROM stdin;
\.


--
-- Data for Name: wagtailembeds_embed; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailembeds_embed (id, url, max_width, type, html, title, author_name, provider_name, thumbnail_url, width, height, last_updated, hash, cache_until) FROM stdin;
\.


--
-- Data for Name: wagtailforms_formsubmission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailforms_formsubmission (id, form_data, submit_time, page_id) FROM stdin;
\.


--
-- Data for Name: wagtailimages_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_image (id, title, file, width, height, created_at, focal_point_x, focal_point_y, focal_point_width, focal_point_height, uploaded_by_user_id, file_size, collection_id, file_hash) FROM stdin;
\.


--
-- Data for Name: wagtailimages_rendition; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_rendition (id, file, width, height, focal_point_key, filter_spec, image_id) FROM stdin;
\.


--
-- Data for Name: wagtailimages_uploadedimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_uploadedimage (id, file, uploaded_by_user_id) FROM stdin;
\.


--
-- Data for Name: wagtailredirects_redirect; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailredirects_redirect (id, old_path, is_permanent, redirect_link, redirect_page_id, site_id) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_editorspick; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_editorspick (id, sort_order, description, page_id, query_id) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_query; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_query (id, query_string) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_querydailyhits; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_querydailyhits (id, date, hits, query_id) FROM stdin;
\.


--
-- Data for Name: wagtailusers_userprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailusers_userprofile (id, submitted_notifications, approved_notifications, rejected_notifications, user_id, preferred_language, current_time_zone, avatar, updated_comments_notifications) FROM stdin;
1	t	t	t	1				t
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 2, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 20, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 248, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 62, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 175, true);


--
-- Name: taggit_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.taggit_tag_id_seq', 1, false);


--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.taggit_taggeditem_id_seq', 1, false);


--
-- Name: wagtail_localize_localesynchronization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_localesynchronization_id_seq', 1, true);


--
-- Name: wagtail_localize_overridablesegment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_overridablesegment_id_seq', 1, false);


--
-- Name: wagtail_localize_relatedobjectsegment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_relatedobjectsegment_id_seq', 1, false);


--
-- Name: wagtail_localize_segmentoverride_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_segmentoverride_id_seq', 1, false);


--
-- Name: wagtail_localize_string_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_string_id_seq', 75, true);


--
-- Name: wagtail_localize_stringsegment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_stringsegment_id_seq', 79, true);


--
-- Name: wagtail_localize_stringtranslation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_stringtranslation_id_seq', 57, true);


--
-- Name: wagtail_localize_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_template_id_seq', 26, true);


--
-- Name: wagtail_localize_templatesegment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_templatesegment_id_seq', 26, true);


--
-- Name: wagtail_localize_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_translation_id_seq', 1, true);


--
-- Name: wagtail_localize_translationcontext_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_translationcontext_id_seq', 66, true);


--
-- Name: wagtail_localize_translationlog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_translationlog_id_seq', 8, true);


--
-- Name: wagtail_localize_translationsource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtail_localize_translationsource_id_seq', 1, true);


--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailadmin_admin_id_seq', 1, false);


--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collection_id_seq', 1, true);


--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collectionviewrestriction_groups_id_seq', 1, false);


--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collectionviewrestriction_id_seq', 1, false);


--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_comment_id_seq', 1, false);


--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_commentreply_id_seq', 1, false);


--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_groupapprovaltask_groups_id_seq', 1, true);


--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_groupcollectionpermission_id_seq', 12, true);


--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_grouppagepermission_id_seq', 7, true);


--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_locale_id_seq', 2, true);


--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_page_id_seq', 4, true);


--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagelogentry_id_seq', 75, true);


--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagerevision_id_seq', 30, true);


--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagesubscription_id_seq', 1, true);


--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pageviewrestriction_groups_id_seq', 1, false);


--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pageviewrestriction_id_seq', 1, false);


--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_site_id_seq', 2, true);


--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_task_id_seq', 1, true);


--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_taskstate_id_seq', 1, false);


--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflow_id_seq', 1, true);


--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflowstate_id_seq', 1, false);


--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflowtask_id_seq', 1, true);


--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtaildocs_document_id_seq', 1, false);


--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtaildocs_uploadeddocument_id_seq', 1, false);


--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailembeds_embed_id_seq', 1, false);


--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailforms_formsubmission_id_seq', 1, false);


--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_image_id_seq', 1, false);


--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_rendition_id_seq', 1, false);


--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_uploadedimage_id_seq', 1, false);


--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailredirects_redirect_id_seq', 1, false);


--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_editorspick_id_seq', 1, false);


--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_query_id_seq', 1, false);


--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_querydailyhits_id_seq', 1, false);


--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailusers_userprofile_id_seq', 1, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: blog_blogdetailpage blog_blogdetailpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_bloglistingpage blog_bloglistingpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_bloglistingpage
    ADD CONSTRAINT blog_bloglistingpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_cookiepolicy blog_cookiepolicy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_cookiepolicy
    ADD CONSTRAINT blog_cookiepolicy_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_gdpr blog_gdpr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_gdpr
    ADD CONSTRAINT blog_gdpr_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_privacy blog_privacy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_privacy
    ADD CONSTRAINT blog_privacy_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_resources blog_resources_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_resources
    ADD CONSTRAINT blog_resources_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_termsofservices blog_termsofservices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_termsofservices
    ADD CONSTRAINT blog_termsofservices_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: home_homepage home_homepage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_homepage
    ADD CONSTRAINT home_homepage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: taggit_tag taggit_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_name_key UNIQUE (name);


--
-- Name: taggit_tag taggit_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_pkey PRIMARY KEY (id);


--
-- Name: taggit_tag taggit_tag_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_slug_key UNIQUE (slug);


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq UNIQUE (content_type_id, object_id, tag_id);


--
-- Name: taggit_taggeditem taggit_taggeditem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_localesynchronization wagtail_localize_localesynchronization_locale_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_localesynchronization
    ADD CONSTRAINT wagtail_localize_localesynchronization_locale_id_key UNIQUE (locale_id);


--
-- Name: wagtail_localize_localesynchronization wagtail_localize_localesynchronization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_localesynchronization
    ADD CONSTRAINT wagtail_localize_localesynchronization_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_overridablesegment wagtail_localize_overridablesegment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_overridablesegment
    ADD CONSTRAINT wagtail_localize_overridablesegment_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_relatedobjectsegment wagtail_localize_relatedobjectsegment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_relatedobjectsegment
    ADD CONSTRAINT wagtail_localize_relatedobjectsegment_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_segmentoverride wagtail_localize_segmentoverride_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_segmentoverride
    ADD CONSTRAINT wagtail_localize_segmentoverride_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_string wagtail_localize_string_locale_id_data_hash_bae71852_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_string
    ADD CONSTRAINT wagtail_localize_string_locale_id_data_hash_bae71852_uniq UNIQUE (locale_id, data_hash);


--
-- Name: wagtail_localize_string wagtail_localize_string_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_string
    ADD CONSTRAINT wagtail_localize_string_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_stringsegment wagtail_localize_stringsegment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringsegment
    ADD CONSTRAINT wagtail_localize_stringsegment_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_stringt_locale_id_translation_of_d2d59359_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_stringt_locale_id_translation_of_d2d59359_uniq UNIQUE (locale_id, translation_of_id, context_id);


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_stringtranslation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_stringtranslation_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_template wagtail_localize_template_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_template
    ADD CONSTRAINT wagtail_localize_template_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_template wagtail_localize_template_uuid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_template
    ADD CONSTRAINT wagtail_localize_template_uuid_key UNIQUE (uuid);


--
-- Name: wagtail_localize_templatesegment wagtail_localize_templatesegment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_templatesegment
    ADD CONSTRAINT wagtail_localize_templatesegment_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_translatableobject wagtail_localize_transla_content_type_id_translat_de875339_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translatableobject
    ADD CONSTRAINT wagtail_localize_transla_content_type_id_translat_de875339_uniq UNIQUE (content_type_id, translation_key);


--
-- Name: wagtail_localize_translationsource wagtail_localize_transla_object_id_locale_id_fb6a8d8e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource
    ADD CONSTRAINT wagtail_localize_transla_object_id_locale_id_fb6a8d8e_uniq UNIQUE (object_id, locale_id);


--
-- Name: wagtail_localize_translationcontext wagtail_localize_transla_object_id_path_id_c7f5d707_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationcontext
    ADD CONSTRAINT wagtail_localize_transla_object_id_path_id_c7f5d707_uniq UNIQUE (object_id, path_id);


--
-- Name: wagtail_localize_translation wagtail_localize_transla_source_id_target_locale__7e943cf8_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation
    ADD CONSTRAINT wagtail_localize_transla_source_id_target_locale__7e943cf8_uniq UNIQUE (source_id, target_locale_id);


--
-- Name: wagtail_localize_translatableobject wagtail_localize_translatableobject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translatableobject
    ADD CONSTRAINT wagtail_localize_translatableobject_pkey PRIMARY KEY (translation_key);


--
-- Name: wagtail_localize_translation wagtail_localize_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation
    ADD CONSTRAINT wagtail_localize_translation_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_translation wagtail_localize_translation_uuid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation
    ADD CONSTRAINT wagtail_localize_translation_uuid_key UNIQUE (uuid);


--
-- Name: wagtail_localize_translationcontext wagtail_localize_translationcontext_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationcontext
    ADD CONSTRAINT wagtail_localize_translationcontext_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_translationlog wagtail_localize_translationlog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationlog
    ADD CONSTRAINT wagtail_localize_translationlog_pkey PRIMARY KEY (id);


--
-- Name: wagtail_localize_translationsource wagtail_localize_translationsource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource
    ADD CONSTRAINT wagtail_localize_translationsource_pkey PRIMARY KEY (id);


--
-- Name: wagtailadmin_admin wagtailadmin_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailadmin_admin
    ADD CONSTRAINT wagtailadmin_admin_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collection wagtailcore_collection_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection
    ADD CONSTRAINT wagtailcore_collection_path_key UNIQUE (path);


--
-- Name: wagtailcore_collection wagtailcore_collection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection
    ADD CONSTRAINT wagtailcore_collection_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collectionvi_collectionviewrestrictio_988995ae_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collectionvi_collectionviewrestrictio_988995ae_uniq UNIQUE (collectionviewrestriction_id, group_id);


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collectionviewrestriction_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collectionviewrestriction_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collectionviewrestriction wagtailcore_collectionviewrestriction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction
    ADD CONSTRAINT wagtailcore_collectionviewrestriction_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_comment wagtailcore_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_commentreply wagtailcore_commentreply_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentreply_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapprova_groupapprovaltask_id_gro_bb5ee7eb_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapprova_groupapprovaltask_id_gro_bb5ee7eb_uniq UNIQUE (groupapprovaltask_id, group_id);


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapprovaltask_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapprovaltask_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_groupapprovaltask wagtailcore_groupapprovaltask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask
    ADD CONSTRAINT wagtailcore_groupapprovaltask_pkey PRIMARY KEY (task_ptr_id);


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcollect_group_id_collection_id_p_a21cefe9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcollect_group_id_collection_id_p_a21cefe9_uniq UNIQUE (group_id, collection_id, permission_id);


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcollectionpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcollectionpermission_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppageper_group_id_page_id_permiss_0898bdf8_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppageper_group_id_page_id_permiss_0898bdf8_uniq UNIQUE (group_id, page_id, permission_type);


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppagepermission_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_locale wagtailcore_locale_language_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale
    ADD CONSTRAINT wagtailcore_locale_language_code_key UNIQUE (language_code);


--
-- Name: wagtailcore_locale wagtailcore_locale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale
    ADD CONSTRAINT wagtailcore_locale_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_page wagtailcore_page_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_path_key UNIQUE (path);


--
-- Name: wagtailcore_page wagtailcore_page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_page wagtailcore_page_translation_key_locale_id_9b041bad_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_translation_key_locale_id_9b041bad_uniq UNIQUE (translation_key, locale_id);


--
-- Name: wagtailcore_pagelogentry wagtailcore_pagelogentry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry
    ADD CONSTRAINT wagtailcore_pagelogentry_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevision_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_page_id_user_id_0cef73ed_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_page_id_user_id_0cef73ed_uniq UNIQUE (page_id, user_id);


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageviewrest_pageviewrestriction_id_g_d23f80bb_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageviewrest_pageviewrestriction_id_g_d23f80bb_uniq UNIQUE (pageviewrestriction_id, group_id);


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageviewrestriction_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageviewrestriction_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pageviewrestriction wagtailcore_pageviewrestriction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction
    ADD CONSTRAINT wagtailcore_pageviewrestriction_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_site wagtailcore_site_hostname_port_2c626d70_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_hostname_port_2c626d70_uniq UNIQUE (hostname, port);


--
-- Name: wagtailcore_site wagtailcore_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_task wagtailcore_task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task
    ADD CONSTRAINT wagtailcore_task_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflow wagtailcore_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflow
    ADD CONSTRAINT wagtailcore_workflow_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowpage wagtailcore_workflowpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflowpage_pkey PRIMARY KEY (page_id);


--
-- Name: wagtailcore_workflowstate wagtailcore_workflowstate_current_task_state_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflowstate_current_task_state_id_key UNIQUE (current_task_state_id);


--
-- Name: wagtailcore_workflowstate wagtailcore_workflowstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflowstate_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowtask wagtailcore_workflowtask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflowtask_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowtask wagtailcore_workflowtask_workflow_id_task_id_4ec7a62b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflowtask_workflow_id_task_id_4ec7a62b_uniq UNIQUE (workflow_id, task_id);


--
-- Name: wagtaildocs_document wagtaildocs_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_pkey PRIMARY KEY (id);


--
-- Name: wagtaildocs_uploadeddocument wagtaildocs_uploadeddocument_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument
    ADD CONSTRAINT wagtaildocs_uploadeddocument_pkey PRIMARY KEY (id);


--
-- Name: wagtailembeds_embed wagtailembeds_embed_hash_c9bd8c9a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed
    ADD CONSTRAINT wagtailembeds_embed_hash_c9bd8c9a_uniq UNIQUE (hash);


--
-- Name: wagtailembeds_embed wagtailembeds_embed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed
    ADD CONSTRAINT wagtailembeds_embed_pkey PRIMARY KEY (id);


--
-- Name: wagtailforms_formsubmission wagtailforms_formsubmission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission
    ADD CONSTRAINT wagtailforms_formsubmission_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_image wagtailimages_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_rendition wagtailimages_rendition_image_id_filter_spec_foc_323c8fe0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendition_image_id_filter_spec_foc_323c8fe0_uniq UNIQUE (image_id, filter_spec, focal_point_key);


--
-- Name: wagtailimages_rendition wagtailimages_rendition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendition_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_uploadedimage wagtailimages_uploadedimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage
    ADD CONSTRAINT wagtailimages_uploadedimage_pkey PRIMARY KEY (id);


--
-- Name: wagtailredirects_redirect wagtailredirects_redirect_old_path_site_id_783622d7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_redirect_old_path_site_id_783622d7_uniq UNIQUE (old_path, site_id);


--
-- Name: wagtailredirects_redirect wagtailredirects_redirect_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_redirect_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_editorspick wagtailsearch_editorspick_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editorspick_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_query wagtailsearch_query_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query
    ADD CONSTRAINT wagtailsearch_query_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_query wagtailsearch_query_query_string_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query
    ADD CONSTRAINT wagtailsearch_query_query_string_key UNIQUE (query_string);


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_querydailyhits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_querydailyhits_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_querydailyhits_query_id_date_1dd232e6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_querydailyhits_query_id_date_1dd232e6_uniq UNIQUE (query_id, date);


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_pkey PRIMARY KEY (id);


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: blog_blogdetailpage_blog_detail_image_id_4fc0bf64; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX blog_blogdetailpage_blog_detail_image_id_4fc0bf64 ON public.blog_blogdetailpage USING btree (blog_detail_image_id);


--
-- Name: blog_blogdetailpage_blog_list_image_id_1657541b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX blog_blogdetailpage_blog_list_image_id_1657541b ON public.blog_blogdetailpage USING btree (blog_list_image_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: taggit_tag_name_58eb2ed9_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_tag_name_58eb2ed9_like ON public.taggit_tag USING btree (name varchar_pattern_ops);


--
-- Name: taggit_tag_slug_6be58b2c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_tag_slug_6be58b2c_like ON public.taggit_tag USING btree (slug varchar_pattern_ops);


--
-- Name: taggit_taggeditem_content_type_id_9957a03c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_content_type_id_9957a03c ON public.taggit_taggeditem USING btree (content_type_id);


--
-- Name: taggit_taggeditem_content_type_id_object_id_196cc965_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_content_type_id_object_id_196cc965_idx ON public.taggit_taggeditem USING btree (content_type_id, object_id);


--
-- Name: taggit_taggeditem_object_id_e2d7d1df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_object_id_e2d7d1df ON public.taggit_taggeditem USING btree (object_id);


--
-- Name: taggit_taggeditem_tag_id_f4f5b767; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_tag_id_f4f5b767 ON public.taggit_taggeditem USING btree (tag_id);


--
-- Name: unique_in_progress_workflow; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_in_progress_workflow ON public.wagtailcore_workflowstate USING btree (page_id) WHERE ((status)::text = ANY ((ARRAY['in_progress'::character varying, 'needs_changes'::character varying])::text[]));


--
-- Name: wagtail_localize_localesynchronization_sync_from_id_1bc4c2eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_localesynchronization_sync_from_id_1bc4c2eb ON public.wagtail_localize_localesynchronization USING btree (sync_from_id);


--
-- Name: wagtail_localize_overridablesegment_context_id_eed8c388; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_overridablesegment_context_id_eed8c388 ON public.wagtail_localize_overridablesegment USING btree (context_id);


--
-- Name: wagtail_localize_overridablesegment_source_id_5720ac20; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_overridablesegment_source_id_5720ac20 ON public.wagtail_localize_overridablesegment USING btree (source_id);


--
-- Name: wagtail_localize_relatedobjectsegment_context_id_c0a725be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_relatedobjectsegment_context_id_c0a725be ON public.wagtail_localize_relatedobjectsegment USING btree (context_id);


--
-- Name: wagtail_localize_relatedobjectsegment_object_id_6c7b58df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_relatedobjectsegment_object_id_6c7b58df ON public.wagtail_localize_relatedobjectsegment USING btree (object_id);


--
-- Name: wagtail_localize_relatedobjectsegment_source_id_9a3b1373; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_relatedobjectsegment_source_id_9a3b1373 ON public.wagtail_localize_relatedobjectsegment USING btree (source_id);


--
-- Name: wagtail_localize_segmentoverride_context_id_f827efe0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_segmentoverride_context_id_f827efe0 ON public.wagtail_localize_segmentoverride USING btree (context_id);


--
-- Name: wagtail_localize_segmentoverride_last_translated_by_id_a86448fc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_segmentoverride_last_translated_by_id_a86448fc ON public.wagtail_localize_segmentoverride USING btree (last_translated_by_id);


--
-- Name: wagtail_localize_segmentoverride_locale_id_b1673d41; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_segmentoverride_locale_id_b1673d41 ON public.wagtail_localize_segmentoverride USING btree (locale_id);


--
-- Name: wagtail_localize_string_locale_id_2cded126; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_string_locale_id_2cded126 ON public.wagtail_localize_string USING btree (locale_id);


--
-- Name: wagtail_localize_stringsegment_context_id_63235539; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringsegment_context_id_63235539 ON public.wagtail_localize_stringsegment USING btree (context_id);


--
-- Name: wagtail_localize_stringsegment_source_id_5804129f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringsegment_source_id_5804129f ON public.wagtail_localize_stringsegment USING btree (source_id);


--
-- Name: wagtail_localize_stringsegment_string_id_6356d5ec; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringsegment_string_id_6356d5ec ON public.wagtail_localize_stringsegment USING btree (string_id);


--
-- Name: wagtail_localize_stringtra_last_translated_by_id_764c7a3f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringtra_last_translated_by_id_764c7a3f ON public.wagtail_localize_stringtranslation USING btree (last_translated_by_id);


--
-- Name: wagtail_localize_stringtranslation_context_id_3cc13049; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringtranslation_context_id_3cc13049 ON public.wagtail_localize_stringtranslation USING btree (context_id);


--
-- Name: wagtail_localize_stringtranslation_locale_id_b9e3730d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringtranslation_locale_id_b9e3730d ON public.wagtail_localize_stringtranslation USING btree (locale_id);


--
-- Name: wagtail_localize_stringtranslation_translation_of_id_8eef01d0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_stringtranslation_translation_of_id_8eef01d0 ON public.wagtail_localize_stringtranslation USING btree (translation_of_id);


--
-- Name: wagtail_localize_templatesegment_context_id_81456b25; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_templatesegment_context_id_81456b25 ON public.wagtail_localize_templatesegment USING btree (context_id);


--
-- Name: wagtail_localize_templatesegment_source_id_26b89dc6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_templatesegment_source_id_26b89dc6 ON public.wagtail_localize_templatesegment USING btree (source_id);


--
-- Name: wagtail_localize_templatesegment_template_id_c370b3c8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_templatesegment_template_id_c370b3c8 ON public.wagtail_localize_templatesegment USING btree (template_id);


--
-- Name: wagtail_localize_translatableobject_content_type_id_0d6c5139; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translatableobject_content_type_id_0d6c5139 ON public.wagtail_localize_translatableobject USING btree (content_type_id);


--
-- Name: wagtail_localize_translati_specific_content_type_id_3095eb76; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translati_specific_content_type_id_3095eb76 ON public.wagtail_localize_translationsource USING btree (specific_content_type_id);


--
-- Name: wagtail_localize_translation_source_id_e326e1e3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translation_source_id_e326e1e3 ON public.wagtail_localize_translation USING btree (source_id);


--
-- Name: wagtail_localize_translation_target_locale_id_8c524f73; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translation_target_locale_id_8c524f73 ON public.wagtail_localize_translation USING btree (target_locale_id);


--
-- Name: wagtail_localize_translationcontext_object_id_86263ce7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationcontext_object_id_86263ce7 ON public.wagtail_localize_translationcontext USING btree (object_id);


--
-- Name: wagtail_localize_translationlog_locale_id_37619cb4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationlog_locale_id_37619cb4 ON public.wagtail_localize_translationlog USING btree (locale_id);


--
-- Name: wagtail_localize_translationlog_page_revision_id_b036eea6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationlog_page_revision_id_b036eea6 ON public.wagtail_localize_translationlog USING btree (page_revision_id);


--
-- Name: wagtail_localize_translationlog_source_id_7228923e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationlog_source_id_7228923e ON public.wagtail_localize_translationlog USING btree (source_id);


--
-- Name: wagtail_localize_translationsource_locale_id_0ac264c0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationsource_locale_id_0ac264c0 ON public.wagtail_localize_translationsource USING btree (locale_id);


--
-- Name: wagtail_localize_translationsource_object_id_6a078fd2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtail_localize_translationsource_object_id_6a078fd2 ON public.wagtail_localize_translationsource USING btree (object_id);


--
-- Name: wagtailcore_collection_path_d848dc19_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collection_path_d848dc19_like ON public.wagtailcore_collection USING btree (path varchar_pattern_ops);


--
-- Name: wagtailcore_collectionview_collectionviewrestriction__47320efd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionview_collectionviewrestriction__47320efd ON public.wagtailcore_collectionviewrestriction_groups USING btree (collectionviewrestriction_id);


--
-- Name: wagtailcore_collectionviewrestriction_collection_id_761908ec; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionviewrestriction_collection_id_761908ec ON public.wagtailcore_collectionviewrestriction USING btree (collection_id);


--
-- Name: wagtailcore_collectionviewrestriction_groups_group_id_1823f2a3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionviewrestriction_groups_group_id_1823f2a3 ON public.wagtailcore_collectionviewrestriction_groups USING btree (group_id);


--
-- Name: wagtailcore_comment_page_id_108444b5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_page_id_108444b5 ON public.wagtailcore_comment USING btree (page_id);


--
-- Name: wagtailcore_comment_resolved_by_id_a282aa0e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_resolved_by_id_a282aa0e ON public.wagtailcore_comment USING btree (resolved_by_id);


--
-- Name: wagtailcore_comment_revision_created_id_1d058279; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_revision_created_id_1d058279 ON public.wagtailcore_comment USING btree (revision_created_id);


--
-- Name: wagtailcore_comment_user_id_0c577ca6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_user_id_0c577ca6 ON public.wagtailcore_comment USING btree (user_id);


--
-- Name: wagtailcore_commentreply_comment_id_afc7e027; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_commentreply_comment_id_afc7e027 ON public.wagtailcore_commentreply USING btree (comment_id);


--
-- Name: wagtailcore_commentreply_user_id_d0b3b9c3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_commentreply_user_id_d0b3b9c3 ON public.wagtailcore_commentreply USING btree (user_id);


--
-- Name: wagtailcore_groupapprovalt_groupapprovaltask_id_9a9255ea; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupapprovalt_groupapprovaltask_id_9a9255ea ON public.wagtailcore_groupapprovaltask_groups USING btree (groupapprovaltask_id);


--
-- Name: wagtailcore_groupapprovaltask_groups_group_id_2e64b61f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupapprovaltask_groups_group_id_2e64b61f ON public.wagtailcore_groupapprovaltask_groups USING btree (group_id);


--
-- Name: wagtailcore_groupcollectionpermission_collection_id_5423575a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_collection_id_5423575a ON public.wagtailcore_groupcollectionpermission USING btree (collection_id);


--
-- Name: wagtailcore_groupcollectionpermission_group_id_05d61460; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_group_id_05d61460 ON public.wagtailcore_groupcollectionpermission USING btree (group_id);


--
-- Name: wagtailcore_groupcollectionpermission_permission_id_1b626275; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_permission_id_1b626275 ON public.wagtailcore_groupcollectionpermission USING btree (permission_id);


--
-- Name: wagtailcore_grouppagepermission_group_id_fc07e671; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_grouppagepermission_group_id_fc07e671 ON public.wagtailcore_grouppagepermission USING btree (group_id);


--
-- Name: wagtailcore_grouppagepermission_page_id_710b114a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_grouppagepermission_page_id_710b114a ON public.wagtailcore_grouppagepermission USING btree (page_id);


--
-- Name: wagtailcore_locale_language_code_03149338_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_locale_language_code_03149338_like ON public.wagtailcore_locale USING btree (language_code varchar_pattern_ops);


--
-- Name: wagtailcore_page_alias_of_id_12945502; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_alias_of_id_12945502 ON public.wagtailcore_page USING btree (alias_of_id);


--
-- Name: wagtailcore_page_content_type_id_c28424df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_content_type_id_c28424df ON public.wagtailcore_page USING btree (content_type_id);


--
-- Name: wagtailcore_page_first_published_at_2b5dd637; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_first_published_at_2b5dd637 ON public.wagtailcore_page USING btree (first_published_at);


--
-- Name: wagtailcore_page_live_revision_id_930bd822; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_live_revision_id_930bd822 ON public.wagtailcore_page USING btree (live_revision_id);


--
-- Name: wagtailcore_page_locale_id_3c7e30a6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_locale_id_3c7e30a6 ON public.wagtailcore_page USING btree (locale_id);


--
-- Name: wagtailcore_page_locked_by_id_bcb86245; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_locked_by_id_bcb86245 ON public.wagtailcore_page USING btree (locked_by_id);


--
-- Name: wagtailcore_page_owner_id_fbf7c332; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_owner_id_fbf7c332 ON public.wagtailcore_page USING btree (owner_id);


--
-- Name: wagtailcore_page_path_98eba2c8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_path_98eba2c8_like ON public.wagtailcore_page USING btree (path varchar_pattern_ops);


--
-- Name: wagtailcore_page_slug_e7c11b8f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_slug_e7c11b8f ON public.wagtailcore_page USING btree (slug);


--
-- Name: wagtailcore_page_slug_e7c11b8f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_slug_e7c11b8f_like ON public.wagtailcore_page USING btree (slug varchar_pattern_ops);


--
-- Name: wagtailcore_pagelogentry_action_c2408198; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_action_c2408198 ON public.wagtailcore_pagelogentry USING btree (action);


--
-- Name: wagtailcore_pagelogentry_action_c2408198_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_action_c2408198_like ON public.wagtailcore_pagelogentry USING btree (action varchar_pattern_ops);


--
-- Name: wagtailcore_pagelogentry_content_changed_99f27ade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_content_changed_99f27ade ON public.wagtailcore_pagelogentry USING btree (content_changed);


--
-- Name: wagtailcore_pagelogentry_content_type_id_74e7708a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_content_type_id_74e7708a ON public.wagtailcore_pagelogentry USING btree (content_type_id);


--
-- Name: wagtailcore_pagelogentry_page_id_8464e327; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_page_id_8464e327 ON public.wagtailcore_pagelogentry USING btree (page_id);


--
-- Name: wagtailcore_pagelogentry_revision_id_8043d103; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_revision_id_8043d103 ON public.wagtailcore_pagelogentry USING btree (revision_id);


--
-- Name: wagtailcore_pagelogentry_user_id_604ccfd8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_user_id_604ccfd8 ON public.wagtailcore_pagelogentry USING btree (user_id);


--
-- Name: wagtailcore_pagerevision_approved_go_live_at_e56afc67; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_approved_go_live_at_e56afc67 ON public.wagtailcore_pagerevision USING btree (approved_go_live_at);


--
-- Name: wagtailcore_pagerevision_created_at_66954e3b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_created_at_66954e3b ON public.wagtailcore_pagerevision USING btree (created_at);


--
-- Name: wagtailcore_pagerevision_page_id_d421cc1d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_page_id_d421cc1d ON public.wagtailcore_pagerevision USING btree (page_id);


--
-- Name: wagtailcore_pagerevision_submitted_for_moderation_c682e44c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_submitted_for_moderation_c682e44c ON public.wagtailcore_pagerevision USING btree (submitted_for_moderation);


--
-- Name: wagtailcore_pagerevision_user_id_2409d2f4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_user_id_2409d2f4 ON public.wagtailcore_pagerevision USING btree (user_id);


--
-- Name: wagtailcore_pagesubscription_page_id_a085e7a6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagesubscription_page_id_a085e7a6 ON public.wagtailcore_pagesubscription USING btree (page_id);


--
-- Name: wagtailcore_pagesubscription_user_id_89d7def9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagesubscription_user_id_89d7def9 ON public.wagtailcore_pagesubscription USING btree (user_id);


--
-- Name: wagtailcore_pageviewrestri_pageviewrestriction_id_f147a99a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestri_pageviewrestriction_id_f147a99a ON public.wagtailcore_pageviewrestriction_groups USING btree (pageviewrestriction_id);


--
-- Name: wagtailcore_pageviewrestriction_groups_group_id_6460f223; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestriction_groups_group_id_6460f223 ON public.wagtailcore_pageviewrestriction_groups USING btree (group_id);


--
-- Name: wagtailcore_pageviewrestriction_page_id_15a8bea6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestriction_page_id_15a8bea6 ON public.wagtailcore_pageviewrestriction USING btree (page_id);


--
-- Name: wagtailcore_site_hostname_96b20b46; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_hostname_96b20b46 ON public.wagtailcore_site USING btree (hostname);


--
-- Name: wagtailcore_site_hostname_96b20b46_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_hostname_96b20b46_like ON public.wagtailcore_site USING btree (hostname varchar_pattern_ops);


--
-- Name: wagtailcore_site_root_page_id_e02fb95c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_root_page_id_e02fb95c ON public.wagtailcore_site USING btree (root_page_id);


--
-- Name: wagtailcore_task_content_type_id_249ab8ba; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_task_content_type_id_249ab8ba ON public.wagtailcore_task USING btree (content_type_id);


--
-- Name: wagtailcore_taskstate_content_type_id_0a758fdc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_content_type_id_0a758fdc ON public.wagtailcore_taskstate USING btree (content_type_id);


--
-- Name: wagtailcore_taskstate_finished_by_id_13f98229; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_finished_by_id_13f98229 ON public.wagtailcore_taskstate USING btree (finished_by_id);


--
-- Name: wagtailcore_taskstate_page_revision_id_9f52c88e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_page_revision_id_9f52c88e ON public.wagtailcore_taskstate USING btree (page_revision_id);


--
-- Name: wagtailcore_taskstate_task_id_c3677c34; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_task_id_c3677c34 ON public.wagtailcore_taskstate USING btree (task_id);


--
-- Name: wagtailcore_taskstate_workflow_state_id_9239a775; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_workflow_state_id_9239a775 ON public.wagtailcore_taskstate USING btree (workflow_state_id);


--
-- Name: wagtailcore_workflowpage_workflow_id_56f56ff6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowpage_workflow_id_56f56ff6 ON public.wagtailcore_workflowpage USING btree (workflow_id);


--
-- Name: wagtailcore_workflowstate_page_id_6c962862; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_page_id_6c962862 ON public.wagtailcore_workflowstate USING btree (page_id);


--
-- Name: wagtailcore_workflowstate_requested_by_id_4090bca3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_requested_by_id_4090bca3 ON public.wagtailcore_workflowstate USING btree (requested_by_id);


--
-- Name: wagtailcore_workflowstate_workflow_id_1f18378f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_workflow_id_1f18378f ON public.wagtailcore_workflowstate USING btree (workflow_id);


--
-- Name: wagtailcore_workflowtask_task_id_ce7716fe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowtask_task_id_ce7716fe ON public.wagtailcore_workflowtask USING btree (task_id);


--
-- Name: wagtailcore_workflowtask_workflow_id_b9717175; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowtask_workflow_id_b9717175 ON public.wagtailcore_workflowtask USING btree (workflow_id);


--
-- Name: wagtaildocs_document_collection_id_23881625; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_document_collection_id_23881625 ON public.wagtaildocs_document USING btree (collection_id);


--
-- Name: wagtaildocs_document_uploaded_by_user_id_17258b41; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_document_uploaded_by_user_id_17258b41 ON public.wagtaildocs_document USING btree (uploaded_by_user_id);


--
-- Name: wagtaildocs_uploadeddocument_uploaded_by_user_id_8dd61a73; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_uploadeddocument_uploaded_by_user_id_8dd61a73 ON public.wagtaildocs_uploadeddocument USING btree (uploaded_by_user_id);


--
-- Name: wagtailembeds_embed_cache_until_26c94bb0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailembeds_embed_cache_until_26c94bb0 ON public.wagtailembeds_embed USING btree (cache_until);


--
-- Name: wagtailembeds_embed_hash_c9bd8c9a_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailembeds_embed_hash_c9bd8c9a_like ON public.wagtailembeds_embed USING btree (hash varchar_pattern_ops);


--
-- Name: wagtailforms_formsubmission_page_id_e48e93e7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailforms_formsubmission_page_id_e48e93e7 ON public.wagtailforms_formsubmission USING btree (page_id);


--
-- Name: wagtailimages_image_collection_id_c2f8af7e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_collection_id_c2f8af7e ON public.wagtailimages_image USING btree (collection_id);


--
-- Name: wagtailimages_image_created_at_86fa6cd4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_created_at_86fa6cd4 ON public.wagtailimages_image USING btree (created_at);


--
-- Name: wagtailimages_image_uploaded_by_user_id_5d73dc75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_uploaded_by_user_id_5d73dc75 ON public.wagtailimages_image USING btree (uploaded_by_user_id);


--
-- Name: wagtailimages_rendition_filter_spec_1cba3201; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_filter_spec_1cba3201 ON public.wagtailimages_rendition USING btree (filter_spec);


--
-- Name: wagtailimages_rendition_filter_spec_1cba3201_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_filter_spec_1cba3201_like ON public.wagtailimages_rendition USING btree (filter_spec varchar_pattern_ops);


--
-- Name: wagtailimages_rendition_image_id_3e1fd774; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_image_id_3e1fd774 ON public.wagtailimages_rendition USING btree (image_id);


--
-- Name: wagtailimages_uploadedimage_uploaded_by_user_id_85921689; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_uploadedimage_uploaded_by_user_id_85921689 ON public.wagtailimages_uploadedimage USING btree (uploaded_by_user_id);


--
-- Name: wagtailredirects_redirect_old_path_bb35247b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_old_path_bb35247b ON public.wagtailredirects_redirect USING btree (old_path);


--
-- Name: wagtailredirects_redirect_old_path_bb35247b_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_old_path_bb35247b_like ON public.wagtailredirects_redirect USING btree (old_path varchar_pattern_ops);


--
-- Name: wagtailredirects_redirect_redirect_page_id_b5728a8f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_redirect_page_id_b5728a8f ON public.wagtailredirects_redirect USING btree (redirect_page_id);


--
-- Name: wagtailredirects_redirect_site_id_780a0e1e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_site_id_780a0e1e ON public.wagtailredirects_redirect USING btree (site_id);


--
-- Name: wagtailsearch_editorspick_page_id_28cbc274; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_editorspick_page_id_28cbc274 ON public.wagtailsearch_editorspick USING btree (page_id);


--
-- Name: wagtailsearch_editorspick_query_id_c6eee4a0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_editorspick_query_id_c6eee4a0 ON public.wagtailsearch_editorspick USING btree (query_id);


--
-- Name: wagtailsearch_query_query_string_e785ea07_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_query_query_string_e785ea07_like ON public.wagtailsearch_query USING btree (query_string varchar_pattern_ops);


--
-- Name: wagtailsearch_querydailyhits_query_id_2185994b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_querydailyhits_query_id_2185994b ON public.wagtailsearch_querydailyhits USING btree (query_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_blog_detail_image_id_4fc0bf64_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_blog_detail_image_id_4fc0bf64_fk_wagtailim FOREIGN KEY (blog_detail_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_blog_list_image_id_1657541b_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_blog_list_image_id_1657541b_fk_wagtailim FOREIGN KEY (blog_list_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_page_ptr_id_fb1104b9_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_page_ptr_id_fb1104b9_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_bloglistingpage blog_bloglistingpage_page_ptr_id_7666e38e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_bloglistingpage
    ADD CONSTRAINT blog_bloglistingpage_page_ptr_id_7666e38e_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_cookiepolicy blog_cookiepolicy_page_ptr_id_d02d076c_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_cookiepolicy
    ADD CONSTRAINT blog_cookiepolicy_page_ptr_id_d02d076c_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_gdpr blog_gdpr_page_ptr_id_62f3bfd5_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_gdpr
    ADD CONSTRAINT blog_gdpr_page_ptr_id_62f3bfd5_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_privacy blog_privacy_page_ptr_id_a2da6703_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_privacy
    ADD CONSTRAINT blog_privacy_page_ptr_id_a2da6703_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_resources blog_resources_page_ptr_id_bbe3d049_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_resources
    ADD CONSTRAINT blog_resources_page_ptr_id_bbe3d049_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_termsofservices blog_termsofservices_page_ptr_id_64fd7663_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_termsofservices
    ADD CONSTRAINT blog_termsofservices_page_ptr_id_64fd7663_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_homepage home_homepage_page_ptr_id_e5b77cf7_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_homepage
    ADD CONSTRAINT home_homepage_page_ptr_id_e5b77cf7_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_9957a03c_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_9957a03c_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id FOREIGN KEY (tag_id) REFERENCES public.taggit_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_localesynchronization wagtail_localize_loc_locale_id_036a3a20_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_localesynchronization
    ADD CONSTRAINT wagtail_localize_loc_locale_id_036a3a20_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_localesynchronization wagtail_localize_loc_sync_from_id_1bc4c2eb_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_localesynchronization
    ADD CONSTRAINT wagtail_localize_loc_sync_from_id_1bc4c2eb_fk_wagtailco FOREIGN KEY (sync_from_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_overridablesegment wagtail_localize_ove_context_id_eed8c388_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_overridablesegment
    ADD CONSTRAINT wagtail_localize_ove_context_id_eed8c388_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_overridablesegment wagtail_localize_ove_source_id_5720ac20_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_overridablesegment
    ADD CONSTRAINT wagtail_localize_ove_source_id_5720ac20_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_relatedobjectsegment wagtail_localize_rel_context_id_c0a725be_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_relatedobjectsegment
    ADD CONSTRAINT wagtail_localize_rel_context_id_c0a725be_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_relatedobjectsegment wagtail_localize_rel_object_id_6c7b58df_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_relatedobjectsegment
    ADD CONSTRAINT wagtail_localize_rel_object_id_6c7b58df_fk_wagtail_l FOREIGN KEY (object_id) REFERENCES public.wagtail_localize_translatableobject(translation_key) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_relatedobjectsegment wagtail_localize_rel_source_id_9a3b1373_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_relatedobjectsegment
    ADD CONSTRAINT wagtail_localize_rel_source_id_9a3b1373_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_segmentoverride wagtail_localize_seg_context_id_f827efe0_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_segmentoverride
    ADD CONSTRAINT wagtail_localize_seg_context_id_f827efe0_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_segmentoverride wagtail_localize_seg_last_translated_by_i_a86448fc_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_segmentoverride
    ADD CONSTRAINT wagtail_localize_seg_last_translated_by_i_a86448fc_fk_auth_user FOREIGN KEY (last_translated_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_segmentoverride wagtail_localize_seg_locale_id_b1673d41_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_segmentoverride
    ADD CONSTRAINT wagtail_localize_seg_locale_id_b1673d41_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_str_context_id_3cc13049_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_str_context_id_3cc13049_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringsegment wagtail_localize_str_context_id_63235539_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringsegment
    ADD CONSTRAINT wagtail_localize_str_context_id_63235539_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_str_last_translated_by_i_764c7a3f_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_str_last_translated_by_i_764c7a3f_fk_auth_user FOREIGN KEY (last_translated_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_string wagtail_localize_str_locale_id_2cded126_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_string
    ADD CONSTRAINT wagtail_localize_str_locale_id_2cded126_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_str_locale_id_b9e3730d_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_str_locale_id_b9e3730d_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringsegment wagtail_localize_str_source_id_5804129f_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringsegment
    ADD CONSTRAINT wagtail_localize_str_source_id_5804129f_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringsegment wagtail_localize_str_string_id_6356d5ec_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringsegment
    ADD CONSTRAINT wagtail_localize_str_string_id_6356d5ec_fk_wagtail_l FOREIGN KEY (string_id) REFERENCES public.wagtail_localize_string(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_stringtranslation wagtail_localize_str_translation_of_id_8eef01d0_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_stringtranslation
    ADD CONSTRAINT wagtail_localize_str_translation_of_id_8eef01d0_fk_wagtail_l FOREIGN KEY (translation_of_id) REFERENCES public.wagtail_localize_string(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_templatesegment wagtail_localize_tem_context_id_81456b25_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_templatesegment
    ADD CONSTRAINT wagtail_localize_tem_context_id_81456b25_fk_wagtail_l FOREIGN KEY (context_id) REFERENCES public.wagtail_localize_translationcontext(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_templatesegment wagtail_localize_tem_source_id_26b89dc6_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_templatesegment
    ADD CONSTRAINT wagtail_localize_tem_source_id_26b89dc6_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_templatesegment wagtail_localize_tem_template_id_c370b3c8_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_templatesegment
    ADD CONSTRAINT wagtail_localize_tem_template_id_c370b3c8_fk_wagtail_l FOREIGN KEY (template_id) REFERENCES public.wagtail_localize_template(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translatableobject wagtail_localize_tra_content_type_id_0d6c5139_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translatableobject
    ADD CONSTRAINT wagtail_localize_tra_content_type_id_0d6c5139_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationsource wagtail_localize_tra_locale_id_0ac264c0_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource
    ADD CONSTRAINT wagtail_localize_tra_locale_id_0ac264c0_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationlog wagtail_localize_tra_locale_id_37619cb4_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationlog
    ADD CONSTRAINT wagtail_localize_tra_locale_id_37619cb4_fk_wagtailco FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationsource wagtail_localize_tra_object_id_6a078fd2_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource
    ADD CONSTRAINT wagtail_localize_tra_object_id_6a078fd2_fk_wagtail_l FOREIGN KEY (object_id) REFERENCES public.wagtail_localize_translatableobject(translation_key) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationcontext wagtail_localize_tra_object_id_86263ce7_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationcontext
    ADD CONSTRAINT wagtail_localize_tra_object_id_86263ce7_fk_wagtail_l FOREIGN KEY (object_id) REFERENCES public.wagtail_localize_translatableobject(translation_key) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationlog wagtail_localize_tra_page_revision_id_b036eea6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationlog
    ADD CONSTRAINT wagtail_localize_tra_page_revision_id_b036eea6_fk_wagtailco FOREIGN KEY (page_revision_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationlog wagtail_localize_tra_source_id_7228923e_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationlog
    ADD CONSTRAINT wagtail_localize_tra_source_id_7228923e_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translation wagtail_localize_tra_source_id_e326e1e3_fk_wagtail_l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation
    ADD CONSTRAINT wagtail_localize_tra_source_id_e326e1e3_fk_wagtail_l FOREIGN KEY (source_id) REFERENCES public.wagtail_localize_translationsource(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translationsource wagtail_localize_tra_specific_content_typ_3095eb76_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translationsource
    ADD CONSTRAINT wagtail_localize_tra_specific_content_typ_3095eb76_fk_django_co FOREIGN KEY (specific_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtail_localize_translation wagtail_localize_tra_target_locale_id_8c524f73_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtail_localize_translation
    ADD CONSTRAINT wagtail_localize_tra_target_locale_id_8c524f73_fk_wagtailco FOREIGN KEY (target_locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction wagtailcore_collecti_collection_id_761908ec_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction
    ADD CONSTRAINT wagtailcore_collecti_collection_id_761908ec_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collecti_collectionviewrestri_47320efd_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collecti_collectionviewrestri_47320efd_fk_wagtailco FOREIGN KEY (collectionviewrestriction_id) REFERENCES public.wagtailcore_collectionviewrestriction(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collecti_group_id_1823f2a3_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collecti_group_id_1823f2a3_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_page_id_108444b5_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_page_id_108444b5_fk_wagtailcore_page_id FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_resolved_by_id_a282aa0e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_resolved_by_id_a282aa0e_fk_auth_user_id FOREIGN KEY (resolved_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_revision_created_id_1d058279_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_revision_created_id_1d058279_fk_wagtailco FOREIGN KEY (revision_created_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_user_id_0c577ca6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_user_id_0c577ca6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_commentreply wagtailcore_commentr_comment_id_afc7e027_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentr_comment_id_afc7e027_fk_wagtailco FOREIGN KEY (comment_id) REFERENCES public.wagtailcore_comment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_commentreply wagtailcore_commentreply_user_id_d0b3b9c3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentreply_user_id_d0b3b9c3_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapp_group_id_2e64b61f_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapp_group_id_2e64b61f_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapp_groupapprovaltask_id_9a9255ea_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapp_groupapprovaltask_id_9a9255ea_fk_wagtailco FOREIGN KEY (groupapprovaltask_id) REFERENCES public.wagtailcore_groupapprovaltask(task_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask wagtailcore_groupapp_task_ptr_id_cfe58781_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask
    ADD CONSTRAINT wagtailcore_groupapp_task_ptr_id_cfe58781_fk_wagtailco FOREIGN KEY (task_ptr_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_collection_id_5423575a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_collection_id_5423575a_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_group_id_05d61460_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_group_id_05d61460_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_permission_id_1b626275_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_permission_id_1b626275_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppag_group_id_fc07e671_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppag_group_id_fc07e671_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppag_page_id_710b114a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppag_page_id_710b114a_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_alias_of_id_12945502_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_alias_of_id_12945502_fk_wagtailcore_page_id FOREIGN KEY (alias_of_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_content_type_id_c28424df_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_content_type_id_c28424df_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_live_revision_id_930bd822_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_live_revision_id_930bd822_fk_wagtailco FOREIGN KEY (live_revision_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_locale_id_3c7e30a6_fk_wagtailcore_locale_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_locale_id_3c7e30a6_fk_wagtailcore_locale_id FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_locked_by_id_bcb86245_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_locked_by_id_bcb86245_fk_auth_user_id FOREIGN KEY (locked_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_owner_id_fbf7c332_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_owner_id_fbf7c332_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagelogentry wagtailcore_pageloge_content_type_id_74e7708a_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry
    ADD CONSTRAINT wagtailcore_pageloge_content_type_id_74e7708a_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevi_page_id_d421cc1d_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevi_page_id_d421cc1d_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevision_user_id_2409d2f4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevision_user_id_2409d2f4_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubs_page_id_a085e7a6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubs_page_id_a085e7a6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_user_id_89d7def9_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_user_id_89d7def9_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageview_group_id_6460f223_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageview_group_id_6460f223_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction wagtailcore_pageview_page_id_15a8bea6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction
    ADD CONSTRAINT wagtailcore_pageview_page_id_15a8bea6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageview_pageviewrestriction__f147a99a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageview_pageviewrestriction__f147a99a_fk_wagtailco FOREIGN KEY (pageviewrestriction_id) REFERENCES public.wagtailcore_pageviewrestriction(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_site wagtailcore_site_root_page_id_e02fb95c_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_root_page_id_e02fb95c_fk_wagtailcore_page_id FOREIGN KEY (root_page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_task wagtailcore_task_content_type_id_249ab8ba_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task
    ADD CONSTRAINT wagtailcore_task_content_type_id_249ab8ba_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_content_type_id_0a758fdc_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_content_type_id_0a758fdc_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_page_revision_id_9f52c88e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_page_revision_id_9f52c88e_fk_wagtailco FOREIGN KEY (page_revision_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_workflow_state_id_9239a775_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_workflow_state_id_9239a775_fk_wagtailco FOREIGN KEY (workflow_state_id) REFERENCES public.wagtailcore_workflowstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_finished_by_id_13f98229_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_finished_by_id_13f98229_fk_auth_user_id FOREIGN KEY (finished_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_task_id_c3677c34_fk_wagtailcore_task_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_task_id_c3677c34_fk_wagtailcore_task_id FOREIGN KEY (task_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_current_task_state_i_3a1a0632_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_current_task_state_i_3a1a0632_fk_wagtailco FOREIGN KEY (current_task_state_id) REFERENCES public.wagtailcore_taskstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_page_id_6c962862_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_page_id_6c962862_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowpage wagtailcore_workflow_page_id_81e7bab6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflow_page_id_81e7bab6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_requested_by_id_4090bca3_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_requested_by_id_4090bca3_fk_auth_user FOREIGN KEY (requested_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowtask wagtailcore_workflow_task_id_ce7716fe_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflow_task_id_ce7716fe_fk_wagtailco FOREIGN KEY (task_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_workflow_id_1f18378f_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_1f18378f_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowpage wagtailcore_workflow_workflow_id_56f56ff6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_56f56ff6_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowtask wagtailcore_workflow_workflow_id_b9717175_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_b9717175_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_document wagtaildocs_document_collection_id_23881625_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_collection_id_23881625_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_document wagtaildocs_document_uploaded_by_user_id_17258b41_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_uploaded_by_user_id_17258b41_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_uploadeddocument wagtaildocs_uploaded_uploaded_by_user_id_8dd61a73_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument
    ADD CONSTRAINT wagtaildocs_uploaded_uploaded_by_user_id_8dd61a73_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailforms_formsubmission wagtailforms_formsub_page_id_e48e93e7_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission
    ADD CONSTRAINT wagtailforms_formsub_page_id_e48e93e7_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_image wagtailimages_image_collection_id_c2f8af7e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_collection_id_c2f8af7e_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_image wagtailimages_image_uploaded_by_user_id_5d73dc75_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_uploaded_by_user_id_5d73dc75_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_rendition wagtailimages_rendit_image_id_3e1fd774_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendit_image_id_3e1fd774_fk_wagtailim FOREIGN KEY (image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_uploadedimage wagtailimages_upload_uploaded_by_user_id_85921689_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage
    ADD CONSTRAINT wagtailimages_upload_uploaded_by_user_id_85921689_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailredirects_redirect wagtailredirects_red_redirect_page_id_b5728a8f_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_red_redirect_page_id_b5728a8f_fk_wagtailco FOREIGN KEY (redirect_page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailredirects_redirect wagtailredirects_red_site_id_780a0e1e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_red_site_id_780a0e1e_fk_wagtailco FOREIGN KEY (site_id) REFERENCES public.wagtailcore_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_editorspick wagtailsearch_editor_page_id_28cbc274_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editor_page_id_28cbc274_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_editorspick wagtailsearch_editor_query_id_c6eee4a0_fk_wagtailse; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editor_query_id_c6eee4a0_fk_wagtailse FOREIGN KEY (query_id) REFERENCES public.wagtailsearch_query(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_queryd_query_id_2185994b_fk_wagtailse; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_queryd_query_id_2185994b_fk_wagtailse FOREIGN KEY (query_id) REFERENCES public.wagtailsearch_query(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_user_id_59c92331_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_user_id_59c92331_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

