from django.db import models
from wagtail.admin.edit_handlers import PageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from streams import blocks
# Create your models here.

class ModulePage(Page):

    template = "module/lead-management.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True) 

    image1 = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    image2 = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    subtitle = models.CharField(max_length=100, blank=True, null=True)
    content = StreamField(
        [
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True,
    )

    
    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)

    content_panels = Page.content_panels + [
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        FieldPanel("subtitle"),
        ImageChooserPanel("image1"),
        ImageChooserPanel("image2"),
        StreamFieldPanel("content"),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
        
    ]    


