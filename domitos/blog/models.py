"""Blog listing and blog detail pages."""
from django.core.mail import EmailMessage
from datetime import date
from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField

from wagtail.core.models import Page, Locale
from wagtail.images.edit_handlers import ImageChooserPanel

from streams import blocks

from django.db import models

from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
    FieldPanel,
    FieldRowPanel,
    InlinePanel,
    MultiFieldPanel
)
from wagtail.core.fields import RichTextField
from wagtail.contrib.forms.models import (
    AbstractEmailForm,
    AbstractFormField
)

class Resources(Page):
    template = "resources/partners.html"


class Downloads(Page):
    template = "resources/downloads.html"

    
class PartnerFormField(AbstractFormField):
    page = ParentalKey(
        'PartnerPage',
        on_delete=models.CASCADE,
        related_name='form_fields',
    )


class PartnerPage(AbstractEmailForm):

    template = "resources/partner_form.html"
    # This is the default path.
    # If ignored, Wagtail adds _landing.html to your template name
    landing_page_template = "resources/partner_form_thankyou.html"

    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro'),
        InlinePanel('form_fields', label='Form Fields'),
        FieldPanel('thank_you_text'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel("subject"),
        ], heading="Email Settings"),
    ]  
  



class BlogListingPage(Page):
    """Listing page lists all the Blog Detail Pages."""

    template = "blog/blog_listing_page.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    category = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        help_text='Overwrites the default title',
    )
    
    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)

    content_panels = Page.content_panels + [
        FieldPanel("category"),
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
    ]

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)
        context["posts"] = BlogDetailPage.objects.live().filter(locale=Locale.get_active()).public().order_by('-date')
       
        return context


class BlogDetailPage(Page):
    """Blog detail page."""
    def posts(self):
        posts = BlogDetailPage.objects.all()
        posts = posts.filter(locale=Locale.get_active()).order_by('-date')[:3]
        return posts

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    category = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        help_text='Overwrites the default title',
    )
    blog_detail_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    blog_list_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    date = models.DateField(max_length=50,null=True,blank=True)
    read_time = models.CharField(max_length=100, blank=False, null=True)

    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)

    content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )
    content_panels = Page.content_panels + [
       
        FieldPanel("category"),
        ImageChooserPanel("blog_detail_image"),
        ImageChooserPanel("blog_list_image"),
        FieldPanel("date"),
        StreamFieldPanel("content"),
        FieldPanel("read_time"),
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
        

    ]
class Privacy(Page):
        template="resources/privacy_policy.html"
        privacy_title=models.CharField(max_length=200, blank=True, null=True)
        privacy_content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )

        
        content_panels = Page.content_panels + [
            FieldPanel("privacy_title"),
            StreamFieldPanel("privacy_content"),
    ]
class TermsOfServices(Page):
    template="resources/terms-of-conditions.html"
    terms_title=models.CharField(max_length=200, blank=True, null=True)
    terms_content = StreamField(
    [
        ("title_and_text", blocks.TitleAndTextBlock()),
        ("full_richtext", blocks.RichtextBlock()),
        ("simple_richtext", blocks.SimpleRichtextBlock()),
        ("cards", blocks.CardBlock()),
        ("cta", blocks.CTABlock()),
    ],
    null=True,
    blank=True,
)

        
    content_panels = Page.content_panels + [
        FieldPanel("terms_title"),
        StreamFieldPanel("terms_content"),
]

class CookiePolicy(Page):
    template="resources/cookie.html"
    cookie_title=models.CharField(max_length=200, blank=True, null=True)
    cookie_content = StreamField(
    [
        ("title_and_text", blocks.TitleAndTextBlock()),
        ("full_richtext", blocks.RichtextBlock()),
        ("simple_richtext", blocks.SimpleRichtextBlock()),
        ("cards", blocks.CardBlock()),
        ("cta", blocks.CTABlock()),
    ],
    null=True,
    blank=True,
)

    
    content_panels = Page.content_panels + [
        FieldPanel("cookie_title"),
        StreamFieldPanel("cookie_content"),
]

class GDPR(Page):
    template="resources/gdpr.html"
    gdpr_title=models.CharField(max_length=200, blank=True, null=True)
    gdpr_content = StreamField(
    [
        ("title_and_text", blocks.TitleAndTextBlock()),
        ("full_richtext", blocks.RichtextBlock()),
        ("simple_richtext", blocks.SimpleRichtextBlock()),
        ("cards", blocks.CardBlock()),
        ("cta", blocks.CTABlock()),
    ],
    null=True,
    blank=True,
)

    
    content_panels = Page.content_panels + [
        FieldPanel("gdpr_title"),
        StreamFieldPanel("gdpr_content"),
]

