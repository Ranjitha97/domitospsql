from django.apps import AppConfig


class CovidsolutionsConfig(AppConfig):
    name = 'covidsolutions'
